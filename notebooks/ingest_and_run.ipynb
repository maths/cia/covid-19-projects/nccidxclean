{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1aea95b0",
   "metadata": {},
   "source": [
    "# Ingest Data and Run the Cleaning Pipelines\n",
    "\n",
    "This notebook ingests the data and runs the NCCIDxClean step-by-step. The NHSx NCCID Cleaning pipeline is then run to allow for comparison."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3d30bcd",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "pd.set_option(\"display.max_columns\", None)\n",
    "import numpy as np\n",
    "import os\n",
    "from pathlib import Path\n",
    "import pydicom\n",
    "from pydicom import Dataset\n",
    "import json\n",
    "from typing import Dict, List, Tuple\n",
    "from tqdm.auto import tqdm\n",
    "from datetime import date\n",
    "import warnings\n",
    "\n",
    "warnings.filterwarnings('ignore')\n",
    "\n",
    "import nccid_cleaning.cleaning as nhsx\n",
    "import nccid_cleaning.etl as etl\n",
    "from nccid_cleaning import clean_data_df, patient_df_pipeline\n",
    "\n",
    "from nccidxclean import xclean_nccid\n",
    "import nccidxclean.clean as xclean"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d923296e",
   "metadata": {},
   "source": [
    "## Data Ingestion\n",
    "\n",
    "Code taken from: https://github.com/nhsx/nccid-cleaning/blob/master/notebooks/ingest.ipynb and courtesy of NHSx.\n",
    "\n",
    "Data ingestion tools that generate tabular patient clinical data and imaging metadata files (.csv) are available in the nhsx pipeline submodule etl.py.\n",
    "\n",
    "To use this you need to provide a `BASE_PATH` that points to the location of the data that has been pulled from the NCCID S3 bucket, where your local directory structure should match the original S3 structure. If you have split the data into training/test/validation sets, each subdirectory should have the same structure as the original S3 bucket and the below pipeline should be run separately for each of the dataset splits.\n",
    "\n",
    "You can set the local path to your NCCID data below by changing the `DEFAULT_PATH` variable or alternatively set as an environment variable, `NCCID_DATA_DIR` in e.g., .bashrc.\n",
    "\n",
    "To use the ingestion tools, the directory tree for your NCCID_DATA_DIR should have the same structure as the original NCCID S3 bucket."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd8ca599",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Edit this to update your local NCCID data path\n",
    "DEFAULT_PATH = \"/project/data/training\"\n",
    "BASE_PATH = Path(os.getenv(\"NCCID_DATA_DIR\", DEFAULT_PATH))\n",
    "print(BASE_PATH)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74eb149f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create directory for outputs if it doesn't exist\n",
    "Path('./data').mkdir(exist_ok=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4806917",
   "metadata": {},
   "source": [
    "### Imaging Metadata\n",
    "For the imaging metadata, a separate CSV is generated for each imaging modality: X-ray, CT, MRI. Three steps are performed:\n",
    "\n",
    "`select_image_files` - traverses the directory tree finding all files of the imaging modality. For X-ray is it recommended to set `select_all = True` to process all available X-ray files. Whereas, for 3D modalities, CT, and MRI, `select_first = True` is recommened to select only the first file of each imaging volume, to speed up run time and reduce redundancy of information.\n",
    "`ingest_dicom_jsons` - reads the DICOM header information for each file.\n",
    "`pydicom_to_df` - converts the DICOM metadata into a pandas DataFrame where the rows are images and columns are the DICOM attributes.\n",
    "\n",
    "The resulting DataFrames are saved as CSV files in `data/`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d308a7f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# subdirectories\n",
    "XRAY_SUBDIR = \"xray-metadata\"\n",
    "CT_SUBDIR = \"ct-metadata\"\n",
    "MRI_SUBDIR = \"mri-metadata\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7e0f5b1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# 1. finding image file lists within the subdirs\n",
    "xray_files = etl.select_image_files(BASE_PATH / XRAY_SUBDIR, select_all=True)\n",
    "ct_files = etl.select_image_files(BASE_PATH / CT_SUBDIR, select_first=True)\n",
    "mri_files = etl.select_image_files(BASE_PATH / MRI_SUBDIR, select_first=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5be5107c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# 2. process image metadata\n",
    "xray_datasets = etl.ingest_dicom_jsons(xray_files)\n",
    "ct_datasets = etl.ingest_dicom_jsons(ct_files)\n",
    "mri_datasets = etl.ingest_dicom_jsons(mri_files)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a78183db",
   "metadata": {},
   "outputs": [],
   "source": [
    "# 3. converting to DataFrame\n",
    "xrays = etl.pydicom_to_df(xray_datasets)\n",
    "cts = etl.pydicom_to_df(ct_datasets)\n",
    "mris = etl.pydicom_to_df(mri_datasets)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5717cfdd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save as csv\n",
    "xrays.to_csv(\"data/xrays.csv\")\n",
    "cts.to_csv(\"data/cts.csv\")\n",
    "mris.to_csv(\"data/mris.csv\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14e19a9d",
   "metadata": {},
   "source": [
    "### Patient Clinical Data\n",
    "For patient clinical data, the most recent data file (for COVID-positive) or status file (for COVID-negative) is parsed for each patient in the directory tree. The resulting DataFrame is generated using patient_jsons_to_df, where rows are patients and columns are data fields.\n",
    "\n",
    "Three fields that are not in the original jsons files are included in the DataFrame:\n",
    "\n",
    "`filename_earliest_date` - earlist data/status file present for the patient.\n",
    "`filename_latest_date` - latest data/status file present for the patient. This is the file from which the rest of the patient's data has been pulled.\n",
    "`filename_covid_status` - indicates it the patient is in the COVID-postive or COVID-negative cohort, based on whether they have every been submitted with a data file (which are only present for positive patients."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7fb327d7",
   "metadata": {},
   "outputs": [],
   "source": [
    "PATIENT_SUBDIR = \"data\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4b3603c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# process patient clinical data\n",
    "patient_files = list(os.walk(BASE_PATH / PATIENT_SUBDIR))\n",
    "patients = etl.patient_jsons_to_df(patient_files)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0638655d",
   "metadata": {},
   "outputs": [],
   "source": [
    "patients.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64c8304a",
   "metadata": {},
   "source": [
    "## Clean Data with NCCIDxClean"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db23590a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Uncomment and modify for bespoke pipeline\n",
    "\n",
    "# pl = (\n",
    "#     xclean.check_new_centres,\n",
    "#     xclean.column_shift,\n",
    "#     xclean.remap_hospitals,\n",
    "#     xclean.remap_sex,\n",
    "#     xclean.remap_ethnicity,\n",
    "#     xclean.parse_date_columns,\n",
    "#     xclean.parse_binary_and_cat,\n",
    "#     xclean.binarise_lung_csv,\n",
    "#     xclean.rescale_fio2,\n",
    "#     nhsx._coerce_numeric_columns,\n",
    "#     nhsx._remap_test_result_columns,\n",
    "#     xclean.clean_numeric,\n",
    "#     xclean.clip_numeric,\n",
    "#     xclean.fix_headers,\n",
    "#     xclean.sense_check,\n",
    "#     xclean.inferences,\n",
    "# )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "ff7527dd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "06/03/2023 10:41:31 - INFO - =============== BEGINNING CLEANING ===============\n",
      "06/03/2023 10:41:31 - INFO - Checking for new centres not in development data...\n",
      "06/03/2023 10:41:31 - INFO - Checking for and correcting column shift...\n",
      "06/03/2023 10:41:33 - INFO - Remapping hospital names...\n",
      "06/03/2023 10:41:33 - INFO - Cleaning sex...\n",
      "06/03/2023 10:41:33 - INFO - Cleaning ethnicity...\n",
      "06/03/2023 10:41:33 - INFO - Cleaning binary and categorical columns...\n",
      "06/03/2023 10:41:59 - INFO - Converting pmh lung and cvs disease to binary + unknown...\n",
      "06/03/2023 10:42:00 - INFO - Cleaning date columns...\n",
      "06/03/2023 10:42:17 - INFO - Cleaning fio2...\n",
      "06/03/2023 10:42:18 - INFO - Cleaning numeric columns...\n",
      "06/03/2023 10:42:51 - WARNING - PaO2 < 7 kPa (52 mmHg) may be venous -> these have been excluded\n",
      "06/03/2023 10:42:51 - WARNING - SpO2/PaO2 should be considered in conjunction with FiO2\n",
      "06/03/2023 10:42:53 - WARNING - Leeds Teaching Hospitals NHS Trust not in the development data...\n",
      "      -> D-dimer Units have been inferred as ng/mL FEU using the mean (437.5).\n",
      "06/03/2023 10:42:55 - INFO - Clipping numeric columns...\n",
      "06/03/2023 10:42:55 - INFO - Fixing column headers...\n",
      "06/03/2023 10:42:55 - INFO - Sense checking dates, news2 and wcc...\n",
      "06/03/2023 10:43:01 - WARNING - 212 patients have a 'news2_score_on_arrival' less than calculated.\n",
      "       -> Please decide whether to use this value given many components of the score are in the data.\n",
      "06/03/2023 10:43:21 - INFO - Performing inferences and further sense checks...\n",
      "06/03/2023 10:43:36 - INFO - =============== FINISHED CLEANING ================\n",
      "06/03/2023 10:43:36 - INFO - Rearranging columns...\n",
      "06/03/2023 10:43:39 - INFO - Creating warning/error report...\n",
      "06/03/2023 10:43:40 - INFO - Selecting output columns...\n",
      "06/03/2023 10:43:40 - INFO - Rearranging columns...\n",
      "06/03/2023 10:43:40 - INFO - ===================== DONE! ======================\n"
     ]
    }
   ],
   "source": [
    "cleaned = xclean_nccid(patients,\n",
    "                       return_cols = 'all_with_original',\n",
    "#                       xpipeline = pl,\n",
    "                      )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "798f1cef",
   "metadata": {},
   "outputs": [],
   "source": [
    "cleaned = xclean.order_columns(cleaned)\n",
    "cleaned.to_csv('./data/cleaned.csv', index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23e0aa62",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "cleaned = pd.read_csv('./data/cleaned.csv', dtype=xclean.read_in_types_dict, parse_dates=xclean.date_cols, dayfirst=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc3bfdbe",
   "metadata": {
    "scrolled": false
   },
   "source": [
    "***Now manually check the numeric and date error sheets saved in `./for_review`***"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8698444a",
   "metadata": {
    "scrolled": false
   },
   "source": [
    "### Read in amended error sheets - numeric and dates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "87b96a08",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "patients = xclean.merge_checks_with_cleaned_df(\n",
    "        cleaned,\n",
    "        numeric_errs_path='./for_review/numeric_warnings.csv',\n",
    "        date_errs_path='./for_review/date_warnings.csv'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d6d3f16",
   "metadata": {
    "scrolled": false
   },
   "source": [
    "### DICOM Enrichment"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a2c0b4f0",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "imaging = {\n",
    "    'xray': xrays,\n",
    "    'ct': cts,\n",
    "    'mri': mris,\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "054e6d83",
   "metadata": {},
   "outputs": [],
   "source": [
    "# DICOM enrichment and sense check\n",
    "if imaging is not None:\n",
    "    patients = xclean.add_dicom_update(patients, list(imaging.values()))\n",
    "    for modality, data in imaging_subdirs.items():\n",
    "        xclean.sense_check_dicom_dates(patients, data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "134c4949",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the final clinical data\n",
    "patients.to_csv(\"./data/patients.csv\", index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "707e745d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Save the DICOM enrichment changes for future use\n",
    "xclean.save_dcm_updates(patients)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "756dd4c6",
   "metadata": {},
   "source": [
    "## Clean Data with Original NHSx NCCID Cleaning Pipeline\n",
    "\n",
    "https://github.com/nhsx/nccid-cleaning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12e045e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# cleaning\n",
    "nhsx_patients = nhsx.clean_data_df(patients, nhsx.patient_df_pipeline)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "857bfb9e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# enriching\n",
    "nhsx_patients = etl.patient_data_dicom_update(nhsx_patients, list(imaging.values()))\n",
    "nhsx_patients.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6cb63310",
   "metadata": {},
   "outputs": [],
   "source": [
    "# save to csv\n",
    "nhsx_patients.to_csv(\"data/nhsx_patients.csv\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "nccidxclean",
   "language": "python",
   "name": "nccidxclean"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
