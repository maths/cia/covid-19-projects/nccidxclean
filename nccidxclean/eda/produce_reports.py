"""
This module contains functions to produce reports for exploratory data analysis (EDA).
"""

import logging
import dataprep.eda as dp
import pandas as pd
from typing import Sequence, Union, Tuple, Dict, NoReturn
from dataprep.eda.create_report import Report
from fpdf import FPDF
import fpdf
from markdown import markdown

from nccidxclean.clean.dicts_and_maps import cleaned_base, all_o2_cols


console_msg = logging.getLogger('consoleLog')

feature_types = {
    'age': 'numeric',
    'ethnicity': 'categorical',
    'sex': 'categorical',
    'smoking_status': 'categorical',
    'pack_year_history': 'numeric',
    'pmh_hypertension': 'categorical',
    'pmh_cvs_disease': 'categorical',
    'pmh_diabetes_mellitus_type_2': 'categorical',
    'pmh_lung_disease': 'categorical',
    'pmh_lung_disease_info': 'categorical',
    'pmh_ckd': 'categorical',
    'if_ckd_stage': 'categorical',
    'current_acei_use': 'categorical',
    'current_angiotension_receptor_blocker_use': 'categorical',
    'current_nsaid_used': 'categorical',
    'date_of_admission': 'date',
    'duration_of_symptoms': 'numeric',
    'respiratory_rate_on_admission': 'numeric',
    'heart_rate_on_admission': 'numeric',
    'pao2_gas': 'numeric',
    'pao2_saturation': 'numeric',
    'fio2': 'numeric',
    'pf_ratio': 'numeric',
    'systolic_bp': 'numeric',
    'diastolic_bp': 'numeric',
    'temperature_on_admission': 'numeric',
    'news2_score_on_arrival': 'numeric',
    'calculated_minimum_news2': 'numeric',
    'wcc_on_admission': 'numeric',
    'lymphocyte_count_on_admission': 'numeric',
    'platelet_count_on_admission': 'numeric',
    'crp_on_admission': 'numeric',
    'd-dimer_on_admission': 'numeric',
    'fibrinogen__if_d-dimer_not_performed': 'numeric',
    'ferritin': 'numeric',
    'urea_on_admission': 'numeric',
    'creatinine_on_admission': 'numeric',
    'troponin_i': 'numeric',
    'troponin_t': 'numeric',
    'date_of_acquisition_of_1st_rt-pcr': 'date',
    'date_of_result_of_1st_rt-pcr': 'date',
    '1st_rt-pcr_result': 'categorical',
    'date_of_acquisition_of_2nd_rt-pcr': 'date',
    'date_of_result_of_2nd_rt-pcr': 'date',
    '2nd_rt-pcr_result': 'categorical',
    'latest_swab_date': 'date',
    'final_covid_status': 'categorical',
    'date_of_positive_covid_swab': 'date',
    'date_of_1st_cxr': 'date',
    'covid_code': 'categorical',
    'cxr_severity': 'categorical',
    'date_of_2nd_cxr': 'date',
    'covid_code_2': 'categorical',
    'cxr_severity_2': 'categorical',
    'itu_admission': 'categorical',
    'date_of_itu_admission': 'date',
    'apache_score_on_itu_arrival': 'numeric',
    'intubation': 'categorical',
    'date_of_intubation': 'date',
    'death': 'categorical',
    'date_of_death': 'date',
    'date_last_known_alive': 'date',
}


def produce_dp_report(
        report_df: pd.DataFrame,
        report_title: str,
) -> Report:
    """
    Utilises the DataPrep package to create a report for
    exploratory data analysis.

    :param report_df: The dataframe of clinical data to be analysed
    :type report_df: pd.DataFrame
    :param report_title: The title of the output report
    :type report_title: str
    :return: The DataPrep report
    :rtype: Report
    """

    # generate report and handle error from dataprep if column has no values
    console_msg.info("Generating:  " + report_title + "...")
    try:
        dp_report = dp.create_report(report_df, title=report_title)
    except KeyError:
        console_msg.warning("Found columns with no values, these were removed for the report.")
        _report_df = report_df.dropna(axis=1, how='all')
        dp_report = dp.create_report(_report_df, title=report_title)

    return dp_report


def feature_by_centre_report(
    pos_df: pd.DataFrame,
    cols_to_report: Union[Sequence[str], str] = "all",
) -> FPDF:
    """
    Examines features by centre, producing a report for each feature.

    :param pos_df: dataframe of cleaned clinical data for Covid-19 positive patients
    :type pos_df: pd.DataFrame
    :param cols_to_report: features / columns to include in the report
    :type cols_to_report: Union[Sequence[str], str], defaults to "all"
    :return: Report in the form of a pdf.
    :rtype: FPDF
    """

    def examine_by_centre(
            patients_df: pd.DataFrame,
            df_col: str,
    ) -> NoReturn:
        """
        For each column, it produces:
            1. A table of missing by centre
            2. The DataPrep plot for the column versus submitting centre
            3. For numeric fields it shows pd.describe table, and for categorical fields
            it produces a table of the breakdown of the possible values by submitting centre.

        :param patients_df: The dataframe of clinical data to be analysed
        :type patients_df: pd.DataFrame
        :param df_col: The column to be analysed
        :type df_col: str
        :return: None
        :rtype: NoReturn
        """

        nonlocal pdf

        pdf.write(markdown("<hr style='border:2px solid gray'>"))
        pdf.write(markdown(f"## {df_col.upper()}"))
        pdf.write(markdown("<hr style='border:1px solid gray'>"))

        df_col_type = feature_types[df_col]

        missing1 = patients_df.groupby('SubmittingCentre').agg({df_col: lambda x: x.isna().sum()})
        missing2 = patients_df.groupby('SubmittingCentre').agg(
            {df_col: lambda x: x.isna().sum() * 100 / (x.isna().sum() + x.notna().sum())})
        missing3 = missing1.join(missing2, lsuffix='___n_missing', rsuffix='___%_missing')
        missing3.columns = missing3.columns.map(lambda x: tuple(x.split('___')))
        missing3 = missing3.sort_index(ascending=[True, False], axis=1)
        missing4 = missing3.T.style.set_table_styles([
            dict(selector='th', props=[('text-align', 'center')]),
            dict(selector='td', props=[('text-align', 'center')]),
        ]).format(precision=0)

        pdf.write(markdown("<br><left><strong>Missing values by centre:<strong><left><br>"))
        pdf.write(missing4)
        pdf.write(markdown(f"<br><br><strong>Charts and table for {df_col} by centre:<strong><br>"))

        if df_col_type == 'categorical':

            chart = dp.plot(patients_df, "SubmittingCentre", df_col, display=['Stacked Bar Chart', 'Heat Map'],
                            progress=False, config={"height": 500, "width": 1500})

            missing_filled = patients_df[['SubmittingCentre', df_col]]
            missing_filled[df_col] = missing_filled[df_col].fillna("MISSING")

            tab = (
                pd.crosstab(
                    columns=missing_filled['SubmittingCentre'],
                    index=missing_filled[df_col],
                )
            )
            tab_percent = (
                    pd.crosstab(
                        columns=missing_filled['SubmittingCentre'],
                        index=missing_filled[df_col],
                        normalize='columns'
                    ) * 100
            )
            tab2 = tab.join(tab_percent, lsuffix='___n', rsuffix='___%')
            tab2.columns = tab2.columns.map(lambda x: tuple(x.split('___')))
            tab2 = (
                tab2.sort_index(ascending=[True, False], axis=1)
                .rename_axis(columns=['SubmittingCentre', ''], axis=1)
            )

            if "MISSING" not in tab2.index:
                tab2.loc['MISSING'] = 0

            tab2 = tab2.style.set_table_styles([
                dict(selector='th', props=[('text-align', 'center'), ('border-left', '1px solid #000066')]),
                dict(selector='td', props=[('text-align', 'center'), ('border-left', '1px solid #000066')]),
            ]).format(precision=1)

            pdf.write(chart.template_base.render(context=chart.context))
            # pdf.write(chart.show())
            pdf.write(tab2)

        elif df_col_type == 'numeric':

            chart = dp.plot(patients_df, "SubmittingCentre", df_col, progress=False,
                            config={"height": 500, "width": 1500, "ngroups": 20})

            tab_wide = patients_df[patients_df[df_col].notnull()].pivot(columns='SubmittingCentre', values=col)

            described = tab_wide.describe(include='all')

            described.loc['n_missing'] = \
            patients_df.groupby('SubmittingCentre').agg({df_col: lambda x: x.isna().sum()})[
                df_col]
            described.loc['%_missing'] = patients_df.groupby('SubmittingCentre').agg(
                {df_col: lambda x: x.isna().sum() * 100 / (x.isna().sum() + x.notna().sum())})[df_col]

            pdf.write(chart.template_base.render(context=chart.context))
            # pdf.write(chart.show())
            pdf.write(
                described.style.set_table_styles([
                    dict(selector='th', props=[('text-align', 'center'), ('border-left', '1px solid #000066')]),
                    dict(selector='td', props=[('text-align', 'center'), ('border-left', '1px solid #000066')]),
                ]).format(precision=2)
            )

        elif df_col_type == 'date':
            chart = dp.plot(patients_df, "SubmittingCentre", df_col, progress=False,
                            config={"height": 500, "width": 1500, "ngroups": 20})
            pdf.write(chart.template_base.render(context=chart.context))
            # pdf.write(chart.show())

    console_msg.info("Generating: Features by center report...")

    pdf = fpdf.FPDF(format='letter')

    pdf.write(markdown("<hr style='border:2px solid gray'>"))
    pdf.write(markdown("# NCCID EDA Report for Feature by Submitting Center"))
    pdf.write(markdown("<hr style='border:2px solid gray'>"))

    cols = feature_types.keys() if cols_to_report == "all" else cols_to_report
    for col in cols:
        examine_by_centre(pos_df, col)

    return pdf


def create_all_reports(
        cleaned_patients: pd.DataFrame,
        features_to_report: Union[str, Sequence[str]] = "all",
) -> Tuple[Report, Report, Report, Dict, fpdf.FPDF]:
    """
    Creates all reports for the EDA.

    :param cleaned_patients: cleaned dataframe of clinical data
    :type cleaned_patients: pd.DataFrame
    :param features_to_report: features to report on, defaults to "all"
    :type features_to_report: Union[str, Sequence[str]], optional
    :return: Tuple of 3 processed reports (all patients, Covid-positive patients only, and Covid-negative
        patients only), report dictionary by center and pdf of a report by feature.
    :rtype: Tuple[Report, Report, Report, Dict, fpdf.FPDF]
    """

    if features_to_report == 'all':
        features_to_report = [col for col in cleaned_base + all_o2_cols if col in cleaned_patients.columns]
    if 'SubmittingCentre' not in features_to_report:
        features_to_report.append('SubmittingCentre')

    clean_df = cleaned_patients[features_to_report]
    cohort_df = {cohort: clean_df[clean_df['filename_covid_status'] == cohort] for cohort in (True, False)}

    # produce report for all patients
    all_patients_report = produce_dp_report(clean_df, 'All Patients')

    # produce report for covid positive and negative cohorts
    cov_pos_report = produce_dp_report(cohort_df[True], f'NCCID EDA Report for Covid Positive Cohort')
    cov_neg_report = produce_dp_report(cohort_df[False], f'NCCID EDA Report for Covid Negative Cohort')

    # produce reports for each center
    reports_by_center = {center: produce_dp_report(
        cohort_df[True][cohort_df[True]['SubmittingCentre'] == center], f'NCCID EDA Report for {center}'
    ) for center in cohort_df[True]['SubmittingCentre'].unique().tolist()}

    # produce reports for each feature by center
    report_by_feature = feature_by_centre_report(cohort_df[True], features_to_report)

    return all_patients_report, cov_pos_report, cov_neg_report, reports_by_center, report_by_feature
