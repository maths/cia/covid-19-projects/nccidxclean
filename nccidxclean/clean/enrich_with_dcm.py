from typing import Collection, NoReturn, Union, Dict
import pandas as pd
import numpy as np
import os
from nccid_cleaning import etl
from nccidxclean.clean.utils import _encrypt_patient, _save_updates
from pathlib import Path
import pydicom
from pydicom import Dataset
from tqdm import tqdm


def _ingest_dicom(file: Path) -> Dataset:
    """
    Reads in metadata of a single of dicom or json file with pydicom
    """
    if Path(file).suffix == '.dcm':
        return pydicom.dcmread(file, stop_before_pixels=True)
    else:
        # return ingest_dicom_json(file)  # NHSx way (correct)
        return etl.ingest_dicom_json(file)


def _ingest_dicoms(files: Collection[Path]) -> Dict[Path, Dataset]:
    """
    Reads in metadata of a list of dicom or json files with pydicom
    """

    datasets = {}
    for file in tqdm(files, desc="Reading files"):
        if Path(file).suffix == '.dcm' or Path(file).suffix == '.json':
            datasets[file] = _ingest_dicom(file)

    return datasets


def extract_dcm_metadata(
        base_dir: os.PathLike,
        img_subdirs: Dict[str, Union[str, os.PathLike]]
) -> Dict[str, pd.DataFrame]:

    """
    Extracts metadata from dicom files

    :param base_dir: base directory - should have same structure as original S3 bucket
    :type base_dir: Union[str, os.PathLike]
    :param img_subdirs: dictionary of modalities and their subdirectory
    :type img_subdirs: Dict[str, Union[str, os.PathLike]]
    :return: dictionary of the modality and a dataframe of the extracted metadata
    :rtype: Dict[str, pd.DataFrame]
    """

    Path('./data/').mkdir(exist_ok=True)

    meta = {}
    for modality, subdir in img_subdirs.items():
        if subdir is not None:
            # finding image file lists within the subdirs
            files = etl.select_image_files(Path(base_dir) / subdir, select_all=True)
            datasets = _ingest_dicoms(files)
            meta[modality] = etl.pydicom_to_df(datasets)
            meta[modality].to_csv(f"./data/{modality}.csv")

    return meta


def add_dicom_update(
        patients: pd.DataFrame,
        images: Collection[pd.DataFrame]
) -> pd.DataFrame:

    """
    Fills in missing values for Sex and Age from imaging dicom headers. The updated data is saved in
    the columns 'sex_update' and 'age_update'.

    :param patients: The patient clinical DataFrame that needs filling in.
    :type: pd.DataFrame
    :param images: List/tuple of image metadata dataframes, e.g., [xrays, cts, mris].
    :type: Collection[pd.DataFrame]
    :return: Patient data with updated sex and age information completed using the image metadata.
    :rtype: pd.DataFrame
    """

    demogs = pd.concat(
        [
            modality[["Pseudonym", "PatientSex", "PatientAge"]]
            for modality in images
        ]
    )
    demogs["ParsedPatientAge"] = demogs["PatientAge"].map(etl.dicom_age_in_years)
    demogs_dedup = (
        demogs.sort_values("ParsedPatientAge", ascending=False)
            .drop_duplicates(subset=["Pseudonym"], keep="first")
            .sort_index()
    )

    def _fill_demog(x, df_dicom, dem, dcm_demog):
        demog = x[dem]
        if demog == "Unknown" or pd.isnull(demog):
            try:
                demog = df_dicom.loc[df_dicom["Pseudonym"] == x["Pseudonym"]][
                    dcm_demog
                ].values[0]
            except IndexError:
                pass
        return demog

    patients["age_b4dcm"] = patients["age"].copy()
    patients["age"] = patients.apply(
        lambda x: _fill_demog(x, demogs_dedup, 'age', 'ParsedPatientAge'), axis=1
    )

    demogs_dedup = (
        demogs.sort_values("PatientSex", ascending=False)
            .drop_duplicates(subset=["Pseudonym"], keep="first")
            .sort_index()
    )

    patients["sex_b4dcm"] = patients["sex"].copy()
    patients["sex"] = patients.apply(
        lambda x: _fill_demog(x, demogs_dedup, 'sex', 'PatientSex'), axis=1
    )

    return patients


def save_dcm_updates(
        patients: pd.DataFrame
) -> NoReturn:

    """
    Encrypts and saves the updates to the clinical data from the dicoms
    involving the age and sex features.

    :param patients: cleaned + dicom enriched dataframe
    :type patients: pd.DataFrame
    :return: None
    :rtype: NoReturn
    """

    # Select only patients with data updated by the dicom
    updated_patients = patients.loc[
        (patients.age != patients.age_b4dcm) | (patients.sex != patients.sex_b4dcm)
        ]
    # Keep only the data that was updated
    updated_patients.loc[updated_patients.age != updated_patients.age_b4dcm, 'age'] = np.nan
    updated_patients.loc[updated_patients.sex != updated_patients.sex_b4dcm, 'sex'] = np.nan
    # Encrypt and save these updates
    xclean_id_dict = updated_patients.set_index('Pseudonym')['xclean_id'].to_dict()
    updates = updated_patients[['Pseudonym', 'age', 'sex']]
    updates.loc[:, 'salt'] = os.urandom(16)
    updates_encrypted = updates.apply(_encrypt_patient, axis=1, args=(xclean_id_dict, updates.columns.tolist()))
    _save_updates(updates_encrypted, 'dcm')

    return
