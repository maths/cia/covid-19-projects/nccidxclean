"""
Parse and clean binary and categorical columns.
"""

import pandas as pd
import numpy as np
import re
from typing import Dict, List
import simple_icd_10 as icd
import logging

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def _xp_remap_lung_disease(
        x: pd.Series,
        lung_dict: Dict[str, str]
) -> pd.Series:

    """
    Remap lung diseases from codes to definition values.
    :param x: input patient series (row of dataframe representing one patient)
    :type x: pd.Series
    :param lung_dict: lung disease mappings
    :type lung_dict: Dict[str, str]
    :return: output patient series
    :rtype: pd.Series
    """

    x["pmh_lung_disease"] = np.nan
    if pd.notna(x["PMH Lung disease"]):
        match_ICD_code = re.search(r"(5: )([A-Z]\d+(?:\.[A-Z0-9])?)", str(x["PMH Lung disease"]))
        if match_ICD_code:
            x["pmh_lung_disease"] = 'Other'
        elif x["PMH Lung disease"] == "5- COPD and Asthma":
            x["pmh_lung_disease"] = "1+4"
        else:
            for key, value in lung_dict.items():
                if value.lower() in str(x["PMH Lung disease"]).lower():
                    if pd.isna(x["pmh_lung_disease"]):
                        x["pmh_lung_disease"] = key
                    else:
                        x["pmh_lung_disease"] = x["pmh_lung_disease"] + "+" + key
                if key in str(x["PMH Lung disease"]):
                    if pd.isna(x["pmh_lung_disease"]):
                        x["pmh_lung_disease"] = key
                    else:
                        x["pmh_lung_disease"] = x["pmh_lung_disease"] + "+" + key

    return x


def _xp_create_lung_disease_info(
        x: pd.Series,
        lung_dict: Dict[str, str],
        other_conditions: List[str]
) -> pd.Series:

    """
    Create new column containing the additional lung disease info provided for a select number of patients.
    :param x: input patient series (row of dataframe representing one patient)
    :type x: pd.Series
    :param lung_dict: lung disease mappings
    :type lung_dict: Dict[str, str]
    :param other_conditions: other conditions provided in development data
    :type other_conditions: List[str]
    :return: output patient series
    :rtype: pd.Series
    """

    if pd.notna(x["pmh_lung_disease"]):
        x["pmh_lung_disease_info"] = []
        match_ICD_code_info = re.search(r"(5: )([A-Z]\d+(?:\.[A-Z0-9])?)", str(x["PMH Lung disease"]))
        if match_ICD_code_info:
            icd_code = match_ICD_code_info.group(2)
            try:
                icd_desc = icd.get_description(icd_code)
                x["pmh_lung_disease_info"].append(icd_desc)
            except:
                match_ICD_parent = re.search(r"([A-Z]\d+)\.([A-Z0-9])", icd_code)
                icd_desc = icd.get_description(match_ICD_parent.group(1))
                x["pmh_lung_disease_info"].append(icd_desc)
        elif x["PMH Lung disease"] == "5- COPD and Asthma":
            x["pmh_lung_disease_info"].append("COPD+Asthma")
        else:
            for key, value in lung_dict.items():
                if key in x["pmh_lung_disease"]:
                    x["pmh_lung_disease_info"].append(value)
            if 'Other' in x["pmh_lung_disease_info"]:
                for condition in other_conditions:
                    if condition.lower() in str(x["PMH Lung disease"]).lower():
                        if 'Other-' in '\t'.join(x["pmh_lung_disease_info"]):
                            x["pmh_lung_disease_info"].append(
                                ("Other-" + condition).replace(" ", "")
                            )
                        else:
                            x["pmh_lung_disease_info"] = list(map(
                                lambda x: x.replace('Other', ("Other-" + condition).replace(" ", "")
                                                    ), x["pmh_lung_disease_info"]))
    else:
        x["pmh_lung_disease_info"] = np.nan

    return x


def _xp_create_cvs_disease_info(
        x: pd.Series,
        cvs_dict: Dict[str, str],
) -> pd.Series:

    """
    Create new column containing the additional cvs disease info provided for a select number of patients.
    :param x: input patient series (row of dataframe representing one patient)
    :type x: pd.Series
    :param cvs_disease_dict: cvs disease mappings
    :type cvs_disease_dict: Dict[str, str]
    :return: output patient series
    :rtype: pd.Series
    """

    #         patients_df['pmh_cvs_disease_info'] = patients_df['pmh_cvs_disease'].map(cvs_disease_dict)

    if pd.notna(x["pmh_cvs_disease"]):
        x["pmh_cvs_disease_info"] = []
        for key, value in cvs_dict.items():
            if value.lower() in str(x["PMH CVS disease"]).lower():
                x["pmh_cvs_disease_info"].append(value)
            elif key in str(x["PMH CVS disease"]):
                x["pmh_cvs_disease_info"].append(value)
    else:
        x["pmh_cvs_disease_info"] = np.nan

    return x


def parse_binary_and_cat(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Parses the binary and categorical columns.  The original binary and categorical functions have
    been merged and the following added/changed:

    - code to allow for unknown values to be kept, e.g. binary field with values of ["0", "1"]
      now has possible values of ["0", "1", "2"] where '2' is unknown.
    - code to handle multiple Lung Diseases added (e.g. 2,4 = COPD and asthma) and save any additional lung disease
      info in a new field.
    - code to turn multiple cvs entries (e.g. '1,2') to 4 (i.e. 'multiple'), rather than extracting the first one.
    - handles 'Yes' in diabetes field
    - handles discrepancies for Sandwell & Birmingham in pmh_hypertension, pmh_cvs_disease, and pmh_lung_disease.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Cleaning binary and categorical columns...")

    if "Pack year history" in patients_df:
        # Remove known errors such as ' '
        patients_df["pack_year_history"] = patients_df[
            "Pack year history"
        ].astype(str).str.extract(r"(\d+)")

    # strip digits from strings and exclude values outside of schema
    # "Unknown" categories mapped to nan if they exist
    schema_values = {
        # Previously "Binary Columns"  ('0': 'No', '1': 'Yes', '2': 'Unknown')
        "PMH hypertension": ["0", "1", "2"],
        "PMH h1pertension": ["0", "1", "2"],
        "PMH CKD": ["0", "1", "2"],
        "Current ACEi use": ["0", "1", "2"],
        "Current Angiotension receptor blocker use": ["0", "1", "2"],
        "Current NSAID used": ["0", "1", "2"],
        "ITU admission": ["0", "1"],
        "Intubation": ["0", "1"],
        "Death": ["0", "1"],
        # Previously "Categorical Columns"
        "Smoking status": ["0", "1", "2", "3"],  # '0': 'Never', '1': 'Ex-smoker', '2': 'Current', '3': 'Unknown'
        "If CKD, stage": ["2", "3", "4", "5"],
        "CXR severity": ["1", "2", "3"],  # '1': 'Mild', '2': 'Moderate', '3': 'Severe'
        "CXR severity 3": ["1", "2", "3"],
        "COVID CODE": ["0", "1", "2", "3"],
        # '0': 'Normal', '1': 'Classic/Probable', '2': 'Indeterminate', '3': 'Non-Covid'
        "COVID CODE 2": ["0", "1", "2", "3"],
    }

    for col in [col for col in schema_values.keys() if col in patients_df]:
        new_col = col.lower().replace(" ", "_").replace(",", "")
        patients_df[new_col] = (
            patients_df[col].astype(str).str.extract(r"(\d+)")
        )
        patients_df[new_col] = patients_df[new_col].loc[
            patients_df[new_col].isin(schema_values[col])
        ]

    # merge 'PMH h1pertension column'
    if "PMH h1pertension" in patients_df:
        patients_df["pmh_hypertension"] = patients_df[
            "pmh_hypertension"
        ].fillna(patients_df["pmh_h1pertension"])

    # not merging 'PMH diabetes mellitus TYPE I'
    if "PMH diabetes mellitus type II" in patients_df:
        patients_df["pmh_diabetes_mellitus_type_2"] = patients_df[
            "PMH diabetes mellitus type II"
        ].copy()
        if "PMH diabetes mellitus TYPE II" in patients_df:
            # Fill in from capitalised original field, "TYPE" instead of "type"
            patients_df["pmh_diabetes_mellitus_type_2"] = patients_df[
                "pmh_diabetes_mellitus_type_2"
            ].fillna(patients_df["PMH diabetes mellitus TYPE II"])
        # finish up remapping
        patients_df["pmh_diabetes_mellitus_type_2"] = patients_df[
            "pmh_diabetes_mellitus_type_2"
        ].map(
            {
                0: "0",
                "0": "0",
                "0.0": "0",
                1: "1",
                "1": "1",
                "1.0": "1",
                "Yes": "1",  # added 'Yes' as found in diabetes field
                2: "2",
                "2": "2",
                "2.0": "2",
            }
        )

    # Lung Disease:
    # Allows for multiple lung diseases and extracts the diagnosis for other if provided.
    # '0': 'No Lung Dx', '1': 'COPD', '2': 'Fibrosis', '3': 'Cystic Fibrosis',
    # '4': 'Asthma', '5': 'Other', '6' : 'Unknown'}

    lung_disease_dict = {
        '0': 'No Lung Dx', '1': 'COPD', '2': 'Fibrosis', '3': 'Cystic Fibrosis',
        '4': 'Asthma', '5': 'Other', '6': 'Unknown'
    }

    other_lung_conditions = [
        'Lobectomy',
        'Lung Ca',
        'Aspergillosis'
    ]

    patients_df = patients_df.apply(_xp_remap_lung_disease, axis=1, args=(lung_disease_dict,))
    patients_df = patients_df.apply(_xp_create_lung_disease_info, axis=1,
                                    args=(lung_disease_dict, other_lung_conditions,))

    # CVS Disease:
    # Remap "x,y" and "x, y" to 4 (multiple) as per the schema.
    # '0': 'No CVS Dx', '1': 'MI', '2': 'Angina', '3': 'Stroke', '4': 'Multiple'

    cvs_disease_dict = {
        '0': 'No_CVS_Dx',
        '1': 'MI',
        '2': 'Angina',
        '3': 'Stroke',
        '4': 'Multiple',
        '5': 'Unknown'
    }

    patients_df["pmh_cvs_disease"] = patients_df["PMH CVS disease"].copy()
    patients_df.loc[patients_df["pmh_cvs_disease"].str.contains(r"\d,\d", na=False), "pmh_cvs_disease"] = "4"
    patients_df.loc[patients_df["pmh_cvs_disease"].str.contains(r"\d,\s\d", na=False), "pmh_cvs_disease"] = "4"

    patients_df["pmh_cvs_disease"] = (
        patients_df["pmh_cvs_disease"].astype(str).str.extract(r"(\d+)")
    )
    patients_df["pmh_cvs_disease"] = patients_df["pmh_cvs_disease"].loc[
        patients_df["pmh_cvs_disease"].isin(cvs_disease_dict.keys())
    ]

    # All sandwell patients have PMH CVS disease of 1! Remove...
    patients_df.loc[patients_df.SubmittingCentre.str.contains("Sandwell"), "pmh_cvs_disease"] = np.nan

    patients_df = patients_df.apply(_xp_create_cvs_disease_info, axis=1, args=(cvs_disease_dict,))

    # console_msg.info('Done cleaning categorical and binary columns!')

    return patients_df


def _binarise_lung_disease(x: pd.Series) -> pd.Series:

    """
    Convert lung disease column to 0 (No), 1 (Yes), 2 (Unknown).
    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    lung_binarise_map = {
        '0': '0',
        '1': '1',
        '2': '1',
        '3': '1',
        '4': '1',
        '5': '1',
        '6': '2',
    }

    for key, value in lung_binarise_map.items():
        if key in str(x.pmh_lung_disease):
            x.pmh_lung_disease = value

    return x


def binarise_lung_csv(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Converts lung disease to binary + unknown as noted some categories had very few values.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info('Converting pmh lung and cvs disease to binary + unknown...')

    cvs_binarise_map = {
        '0': '0',
        '1': '1',
        '2': '1',
        '3': '1',
        '4': '1',
        '5': '2',
    }

    patients_df = patients_df.apply(_binarise_lung_disease, axis=1)

    patients_df['pmh_cvs_disease'] = patients_df['pmh_cvs_disease'].map(cvs_binarise_map)

    # console_msg.info('Done converting pmh lung and cvs disease!')
    return patients_df
