"""
Performs logical sense checks on the data after cleaning to warn of potential errors and remove
data where necessary.
"""

import pandas as pd
import numpy as np
import logging
from nccidxclean.clean.dicts_and_maps import date_cols

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def _sense_check_news2(x: pd.Series) -> pd.Series:

    """
    Checks whether the NEWS2 is larger than the score you can calulate from the data provided.
    The calculated score is saved in 'calculated_minimum_news2' and is a minimum
    approximation as not all the necessary fields are provided for the calculation.

    N.B. carefully consider whether to use NEWS in you model as it is derived
    from other values.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    min_news2 = 0

    if 38.1 <= x["temperature_on_admission"] <= 39.0 or 35.1 <= x["temperature_on_admission"] <= 36.0:
        min_news2 += 1
    elif 39.1 <= x["temperature_on_admission"]:
        min_news2 += 2
    elif x["temperature_on_admission"] <= 35:
        min_news2 += 3

    if 9 <= x["respiratory_rate_on_admission"] <= 11:
        min_news2 += 1
    elif 21 <= x["respiratory_rate_on_admission"] <= 24:
        min_news2 += 2
    elif 8 >= x["respiratory_rate_on_admission"] >= 25:
        min_news2 += 3

    if pd.notnull(x["fio2"]) and x["fio2"] != 21:
        min_news2 += 2

    if x["pmh_lung_disease"] != 1:  # i.e. don't have copd
        if 94 <= x["spo2_saturation"] <= 95:
            min_news2 += 1
        elif 92 <= x["spo2_saturation"] <= 93:
            min_news2 += 2
        elif x["spo2_saturation"] <= 91:
            min_news2 += 3
    else:  # can use these values for all copd (regardless of their co2), as minimum value
        if 86 <= x["spo2_saturation"] <= 87:
            min_news2 += 1
        elif 84 <= x["spo2_saturation"] <= 85:
            min_news2 += 2
        elif x["spo2_saturation"] <= 83:
            min_news2 += 3

    if 101 <= x["systolic_bp"] <= 110:
        min_news2 += 1
    elif 91 <= x["systolic_bp"] <= 100:
        min_news2 += 2
    elif 90 >= x["systolic_bp"] >= 220:
        min_news2 += 3

    if 41 <= x["heart_rate_on_admission"] <= 50 or 91 <= x["heart_rate_on_admission"] <= 110:
        min_news2 += 1
    elif 111 <= x["heart_rate_on_admission"] <= 130:
        min_news2 += 2
    elif 40 <= x["heart_rate_on_admission"] >= 131:
        min_news2 += 3

    if pd.notnull(x["temperature_on_admission"]) or pd.notnull(x["respiratory_rate_on_admission"]) or pd.notnull(
            x["fio2"]) or pd.notnull(x["spo2_saturation"]) or pd.notnull(x["systolic_bp"]) or pd.notnull(
        x["heart_rate_on_admission"]):
        x['calculated_minimum_news2'] = min_news2

    return x


def _sense_check_dates(x: pd.Series) -> pd.Series:

    """
    Checks that the dates flow in a logical order and produces a warning if not. Dates are removed if they are
    after 2022, before 2019, or after the date of the latest included data set.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    def _basic_date_check(x: pd.Series) -> pd.Series:

        for col in date_cols:
            if pd.notna(x[col]) and x[col] != 'error':
                if x[col] > pd.to_datetime('01/01/2023', format="%d/%m/%Y"):
                    logger.error(f"DATE: {x['Pseudonym']}: {col} after 2022")
                    # x[col] = np.nan
                elif x[col] < pd.to_datetime('01/01/2019', format="%d/%m/%Y"):
                    logger.error(f"DATE: {x['Pseudonym']}: {col} before 2019")
                    # x[col] = np.nan
        return x

    def _check_swab_results(x: pd.Series) -> pd.Series:

        swab_checks = [
            ('date_of_acquisition_of_1st_rt-pcr', 'date_of_result_of_1st_rt-pcr'),
            ('date_of_acquisition_of_2nd_rt-pcr', 'date_of_result_of_2nd_rt-pcr'),
            ('date_of_acquisition_of_1st_rt-pcr', 'date_of_acquisition_of_2nd_rt-pcr'),
            ('date_of_result_of_1st_rt-pcr', 'date_of_result_of_2nd_rt-pcr')
        ]

        for col1, col2 in swab_checks:
            if pd.notna(x[col1]) and pd.notna(x[col2]) and x[col1] != 'error' and x[col2] != 'error':
                if x[col1] > x[col2]:
                    logger.warning(f"DATE: {x['Pseudonym']}: {col1} after {col2}")

        return x

    def _check_date_of_admission(x: pd.Series) -> pd.Series:

        if pd.notna(x['date_of_admission']):

            for field in [
                'date_of_itu_admission',
                'date_of_intubation',
                'date_last_known_alive',
                'date_of_death',
            ]:
                if pd.notna(x[field]) and x[field] != 'error':
                    if x[field] < x['date_of_admission']:
                        logger.warning(f"DATE: {x['Pseudonym']}: date_of_admission after {field}")

        return x

    def _check_date_of_death(x: pd.Series) -> pd.Series:

        if pd.notna(x['date_of_death']):

            for field in [
                'date_of_acquisition_of_1st_rt-pcr',
                'date_of_acquisition_of_2nd_rt-pcr',
                'date_of_positive_covid_swab',
                'date_of_1st_cxr',
                'date_of_2nd_cxr',
                'date_of_itu_admission',
                'date_of_intubation',
                'date_last_known_alive',
            ]:
                if pd.notna(x[field]) and x[field] != 'error':
                    if x[field] > x['date_of_death']:
                        logger.warning(f"DATE: {x['Pseudonym']}: {field} after date_of_death")

        return x

    def _check_date_of_latest_data(x: pd.Series) -> pd.Series:

        if pd.notna(x['filename_latest_date']):

            for field in [
                'date_of_positive_covid_swab',
                'date_of_acquisition_of_1st_rt-pcr',
                'date_of_acquisition_of_2nd_rt-pcr',
                'date_of_result_of_1st_rt-pcr',
                'date_of_result_of_2nd_rt-pcr',
                'date_of_admission',
                'date_of_itu_admission',
                'date_of_intubation',
                'date_of_1st_cxr',
                'date_of_2nd_cxr',
                'date_last_known_alive',
                'date_of_death',
            ]:
                if pd.notna(x[field]) and x[field] != 'error':
                    if x[field] > x['filename_latest_date']:
                        # x[field] = np.nan
                        logger.error(f"DATE: {x['Pseudonym']}: {field} after filename_latest_date")

        return x

    sense_check_pipeline = [
        _basic_date_check,
        _check_swab_results,
        _check_date_of_admission,
        _check_date_of_death,
        _check_date_of_latest_data
    ]

    for check_function in sense_check_pipeline:
        x = x.pipe(check_function)

    return x


def _sense_check_wcc(x: pd.Series) -> pd.Series:

    """
    Provides a warning if lymphocyte count is greater than the wcc.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series

    """

    if pd.notna(x['lymphocyte_count_on_admission']) and pd.notna(x['wcc_on_admission']) and x[
        'lymphocyte_count_on_admission'] > x['wcc_on_admission']:
        logger.warning(f"NUM: {x['Pseudonym']}: Lymphocyte count > WCC")

    return x


def sense_check(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Sense checks data to identify potential errors.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Sense checking dates, news2 and wcc...")

    # Sense check NEWS2 score
    patients_df = patients_df.apply(_sense_check_news2, axis=1)
    if len(patients_df[patients_df["calculated_minimum_news2"] > patients_df['news2_score_on_arrival']]) > 0:
        console_msg.warning(f"{len(patients_df[patients_df['calculated_minimum_news2'] > patients_df['news2_score_on_arrival']])}"
                            " patients have a 'news2_score_on_arrival' less than calculated.\n"
                            "       -> Please decide whether to use this value given many components of the score are in the data.")

    # Sense check dates
    patients_df = patients_df.apply(_sense_check_dates, axis=1)
    patients_df = patients_df.apply(_sense_check_wcc, axis=1)

    # console_msg.info("Done sense checking!")

    return patients_df


def sense_check_dicom_dates(
        patients_df: pd.DataFrame,
        imaging_df: pd.DataFrame,
) -> (pd.DataFrame, pd.DataFrame):

    """
    1. Checks that no patients have a scan dated after they supposedly died in the clinical data.
    2. Updates date_last_known_alive in the clinical data, if patient had a scan.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.Dataframe
    :param imaging_df: dataframe containing metadata from the DICOM imaging files
    :type imaging_df: pd.Dataframe
    :return: the clinical data and image metadata dataframes
    :rtype: pd.Dataframe, pd.Dataframe
    """

    console_msg.info("Sense checking clinical data against DICOM...")

    def _sense_check_versus_date_of_death(x: pd.Series) -> pd.Series:

        """
        Checks that no patients have a scan dated after they supposedly died.

        :param x: input patient series
        :type x: pd.Series
        :return: output patient series
        :rtype: pd.Series
        """

        dicom_fields = ['StudyDate', 'AcquisitionDate']

        for dicom_field in dicom_fields:
            if pd.notna(x[dicom_field]) and pd.notna(x['date_of_death']) and x['date_of_death'] != 'error':
                dcm_dt_int = str(int(float(x[dicom_field])))
                if pd.to_datetime(dcm_dt_int, format="%Y%m%d", errors='coerce') > x['date_of_death']:
                    console_msg.warning(
                        f"{x['Pseudonym']}: scan {dicom_field} - {pd.to_datetime(x[dicom_field]).strftime('%d/%m/%Y')} "
                        f"after their date_of_death ({x['date_of_death'].strftime('%d/%m/%Y')}).\n"
                        f"      'StudyInstanceUID =', {x['StudyInstanceUID']}\n"
                        f"      -> May be post-mortem or error in the clinical data -> please review"
                    )
        return x

    def _check_date_last_known_alive(x: pd.Series) -> pd.Series:

        dicom_fields = ['StudyDate', 'AcquisitionDate']

        for dicom_field in dicom_fields:
            if pd.notna(x[dicom_field]) and pd.notna(x['date_last_known_alive']) and x['date_last_known_alive'] != 'error':
                dcm_dt_int = str(int(float(x[dicom_field])))
                dicom_datetime = pd.to_datetime(dcm_dt_int, format="%Y%m%d", errors='coerce')
                if dicom_datetime > x['date_last_known_alive']:
                    x['date_last_known_alive'] = dicom_datetime

        return x

    merged_df = imaging_df.merge(patients_df[[
        'Pseudonym', 'date_of_death', 'date_last_known_alive'
    ]], on="Pseudonym", how='outer')

    # Check dates of death vs. dicom
    merged_df = merged_df.apply(_sense_check_versus_date_of_death, axis=1)

    # Update date last known alive:
    merged_df = merged_df.apply(_check_date_last_known_alive, axis=1)

    date_last_known_alive_df = merged_df[['Pseudonym', 'date_last_known_alive']].sort_values(
        by="date_last_known_alive", ignore_index=True, ascending=False, na_position='last'
    ).drop_duplicates(
        subset='Pseudonym', keep='first'
    )

    patients_df = patients_df.drop(['date_last_known_alive'], axis=1)
    patients_df = patients_df.merge(date_last_known_alive_df, on='Pseudonym', how='left')

    console_msg.info("Done sense checking vs. dicom dates!")
    return patients_df, imaging_df
