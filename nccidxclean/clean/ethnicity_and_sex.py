"""
Remaps sex and ethnicity.
"""

import pandas as pd
import logging
from nccidxclean.clean.dicts_and_maps import _ETHNICITY_MAPPING, _SEX_MAPPING

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


# Ethnicity Mapping - unchanged from NHSx pipeline, except "np.nan: 'Unknown'" commented out
_ETHNICITY_MAPPING = {k.lower(): v for k, v in _ETHNICITY_MAPPING.items()}
_ETHNICITY_MAPPING.update(
    {v.lower(): v for v in set(_ETHNICITY_MAPPING.values())}
)

# _ETHNICITY_MAPPING.update({np.nan: "Unknown"})


# NCCID Sex Mapping - unchanged except "np.nan: 'Unknown'" commented out
_SEX_MAPPING = {k.lower(): v for k, v in _SEX_MAPPING.items()}
_SEX_MAPPING.update({v.lower(): v for v in set(_SEX_MAPPING.values())})


# _SEX_MAPPING.update({np.nan: "Unknown"})


def remap_ethnicity(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Remap ethnicities to standardised groupings.
    :param patients_df:  dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data with ethnicities remapped
    :rtype: pd.DataFrame
    """

    console_msg.info("Cleaning ethnicity...")

    _ETHNICITY_MAPPING.update(
        {
            e.lower(): "Multiple"
            for e in patients_df["Ethnicity"].unique()
            if "mixed" in str(e).lower() or "multiple" in str(e).lower()
        }
    )
    _ETHNICITY_MAPPING.update(
        {
            e.lower(): "White"
            for e in patients_df["Ethnicity"].unique()
            if "white" in str(e).lower()
        }
    )
    _ETHNICITY_MAPPING.update(
        {
            str(e).lower(): "Unknown"
            for e in patients_df["Ethnicity"].unique()
            if str(e).lower() not in _ETHNICITY_MAPPING
        }
    )

    try:
        patients_df["ethnicity"] = (
            patients_df["Ethnicity"].str.lower().replace(_ETHNICITY_MAPPING)
        )
    except AttributeError:
        # If the column is all empty, the .str call would
        # break as non-string is inferred
        pass

    # console_msg.info("Done cleaning ethnicity!")

    return patients_df


def remap_sex(patients_df: pd.DataFrame) -> pd.DataFrame:
    
    """
    NCCID code:
    Remaps sex to F/M/Unknown.
    Converts any missing values to Unknown.

    This function:
    - Missing values are left missing to be consistent with the other fields.  Developer can
    choose whether to treat unknown and np.nan as equivalent.
    - Converts the Sandwell sex codes to match the schema.  For female patients values 0,1 and 2 are reported
    for Sandwell and West Birmingham (2 = Female, 1 = Male). To match the other centres (0 = Female, 1 = Male)
    values were corrected based on the information from the json files.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data with sex remapped
    :rtype: pd.DataFrame
    """

    console_msg.info("Cleaning sex...")

    try:
        patients_df["sex"] = (
            patients_df["Sex"]
                .str.lower()
                .map(_SEX_MAPPING)
            #             .replace({np.nan: "Unknown"})
        )

        patients_df.loc[(patients_df.SubmittingCentre == 'Sandwell and West Birmingham Hospitals NHS Trust') &
                        (patients_df.Sex == "2"), "sex"] = "F"

    except AttributeError:
        # If the column is all empty, the .str call would
        # break as non-string is inferred
        pass

    # console_msg.info("Done cleaning sex!")

    return patients_df
