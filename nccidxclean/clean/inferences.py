"""
Performs logical inferences on the data to reduce missing data.
"""

import pandas as pd
import numpy as np
from typing import Collection, Callable
import logging

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def _update_final_covid_status(x: pd.Series) -> pd.Series:

    """
    There are patients with two positive swabs with a Negative final covid status.
    This function changes the final covid status to 'Positive' if they have had a positive test.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    if x['final_covid_status'] == 'Negative' and (
            x['1st_rt-pcr_result'] == "Positive" or x['2nd_rt-pcr_result'] == "Positive" or pd.notna(
        x['date_of_positive_covid_swab'])
    ):
        x['final_covid_status'] = "Positive"

    if pd.notna(x['negative_swab_date']):
        x['final_covid_status'] = "Negative"

    return x


def _death_inferences(x: pd.Series) -> pd.Series:

    """
    Infers binary value of death parameter if missing using date of death and date last known alive.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    if pd.notna(x['date_of_death']) and pd.isna(x['death']):
        x['death'] = '1'

    if pd.notna(x['date_last_known_alive']) and pd.isna(x['death']) and pd.isna(x['date_of_death']):
        x['death'] = '0'

    return x


def _last_known_alive_inferences(x: pd.Series) -> pd.Series:

    """
    Infers date last known alive using date of death.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    if pd.notna(x['date_of_death']) and pd.isna(x['date_last_known_alive']):
        x['date_last_known_alive'] = x['date_of_death']

    return x


def _itu_and_intubation_inferences(x: pd.Series) -> pd.Series:
    """
    Infers value of the itu admission feature if date of itu admission is included.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    for param in ['itu_admission', 'intubation']:
        if pd.notna(x['date_of_' + param]) and pd.isna(x[param]):
            x[param] = '1'

    if pd.notna(x['date_of_' + param]) and x[param] == '0':
        logger.warning(f"NUM: {x['Pseudonym']}: date_of_{param} but {param}=0")

    return x


def _ckd_inferences(x: pd.Series) -> pd.Series:

    """
    Infers value of the ckd feature if date of a stage of ckd is included.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    if pd.notna(x['if_ckd_stage']):
        x['pmh_ckd'] = '1'

    return x


def _calculate_pf_ratio(x: pd.Series) -> pd.Series:

    """
    Imputes P\F ratio and returns result in a new column.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    x['pf_ratio'] = np.nan
    if pd.notna(x['fio2']) and pd.notna(x['pao2_gas']):
        x['pf_ratio'] = x['pao2_gas'] / (x['fio2'] / 100)
    elif pd.notna(x['fio2']) and pd.notna(x['pao2_imputed']):
        x['pf_ratio'] = x['pao2_imputed'] / (x['fio2'] / 100)

    return x


def inferences(
        patients_df: pd.DataFrame,
        inference_pipeline: Collection[Callable] = (
                'update_final_covid_status',
                'death_inferences',
                'last_known_alive_inferences',
                'itu_and_intubation_inferences',
                'ckd_inferences',
                'calculate_pf_ratio',
        )
) -> pd.DataFrame:

    """
    Applies functions which perform inferences. Pipeline of inference functions may be modified as desired.
    All functions are applied by default.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :param inference_pipeline: inference functions to apply
    :type inference_pipeline: Collection
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Performing inferences and further sense checks...")


    inference_function_dict = {
        'update_final_covid_status': _update_final_covid_status,
        'death_inferences': _death_inferences,
        'last_known_alive_inferences': _last_known_alive_inferences,
        'itu_and_intubation_inferences': _itu_and_intubation_inferences,
        'ckd_inferences': _ckd_inferences,
        'calculate_pf_ratio': _calculate_pf_ratio,
    }

    for inference_func in inference_pipeline:
        patients_df = patients_df.apply(inference_function_dict[inference_func], axis=1)

    # console_msg.info("Done performing inferences!")
    return patients_df
