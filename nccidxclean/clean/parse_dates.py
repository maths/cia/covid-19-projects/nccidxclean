"""

Parses and cleans date columns.

"""

import pandas as pd
import numpy as np
import re
import nccid_cleaning.cleaning as nhsx
import logging

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def _clean_us_dates(x: (str, int, float)) -> pd.datetime:
    """
    Converts fields expected in US date format MM/DD/YY
    into pd.datetime dates. Dates are pulled out from entries with
    known errors of the form '[Text] - YYYY-MM-DD'.
    Other known errors e.g., '.', ' ', and unknown errors are parsed as pd.NaT.

    :param x: input date entry
    :type x: str, int, float
    :return: date entry in datetime format
    :rtype: pd.datetime
    """

    _x = pd.NaT

    # -----------------------------
    # Added to manage excel formatted dates in US columns
    if str(x).isdigit():
        _x = pd.Timestamp(1899, 12, 30) + pd.Timedelta(days=int(x))
    # -----------------------------

    if isinstance(x, str):

        # Issue is that 'day first' isnt strict
        # Set format using "%m/%d/%Y" or "%m/%d/%y"
        # Use try and if format doesn't match, then put ERROR in column so can check it

        # Look for expected date patterns MM/DD/YY or MM/DD/YYYY
        match_standard = re.search(
            r"\d{1,2}/\d{1,2}/\d{2,4}",
            x,
        )
        if match_standard:
            # Coerce into US date format
            try:
                _x = pd.to_datetime(
                    match_standard.group(), format="%m/%d/%Y", errors="raise"
                )
            except:
                try:
                    _x = pd.to_datetime(
                        match_standard.group(), format="%m/%d/%y", errors="raise"
                    )
                except:
                    logger.error(f"DATE FORMAT: UK formatted date found in US date column ({x})")
                    _x = 'error_expected_us'

        else:
            # Look for known error date patterns
            match_error = re.search(r"\d{4}-\d{2}-\d{2}", x)
            if match_error:
                _x = pd.to_datetime(match_error.group(), errors="coerce")

    return _x


def _clean_uk_dates_expected_in_us(x: (str, int, float)) -> pd.datetime:
    """
    Cleans dates in UK format (MM/DD/YYYY) which were expected in US format (DD/MM/YYYY)

    :param x: input date entry
    :type x: str, int, float
    :return: date entry in datetime format
    :rtype: pd.datetime
    """

    _x = pd.NaT

    # -----------------------------
    # Added to manage excel formatted dates in US columns
    if str(x).isdigit():
        _x = pd.Timestamp(1899, 12, 30) + pd.Timedelta(days=int(x))
    # -----------------------------

    elif isinstance(x, str):

        # Issue is that 'day first' isnt strict
        # Set format using "%m/%d/%Y" or "%m/%d/%y"
        # Use try and if format doesn't match, then put ERROR in column so can check

        # Look for expected date patterns MM/DD/YY or MM/DD/YYYY
        match_standard = re.search(
            r"\d{1,2}/\d{1,2}/\d{2,4}",
            x,
        )
        if match_standard:
            # Coerce into US date format
            try:
                _x = pd.to_datetime(
                    match_standard.group(), format="%d/%m/%Y", errors="raise"
                )
            except:
                try:
                    _x = pd.to_datetime(
                        match_standard.group(), format="%m/%d/%y", errors="raise"
                    )
                except:
                    # At time of development of this package, Leicester, St Georges and The Walton Centre swab
                    # dates were provided in UK format and are handled accordingly -> Need review in case this has
                    # been corrected.
                    logger.error(f"DATE FORMAT: PCR dates expected in UK format in US format ({x})")
                    _x = 'error_expected_uk'

    return _x


def _convert_uk_excel_date(x: pd.Series) -> pd.Series:
    """
    Added to manage excel formatted dates in US columns.

    :param x: input patient series (row of dataframe representing one patient)
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    if str(x["SwabDate"]).isdigit():
        x["swabdate"] = pd.Timestamp(1899, 12, 30) + pd.Timedelta(days=int(x["SwabDate"]))
    return x


def _swab_dates(g):
    """
    Handles multiple swab dates in a Swabdates column not in the original schema
    for the COVID-negative patients.

    :param g: patient's 'Swabdates' entry of dates of PCR swabs
    :type g: any
    :return: list of PCR swab dates provided
    :rtype: list
    """

    if isinstance(g, list):
        return [
            {
                "SwabDate": pd.to_datetime(v["SwabDate"], dayfirst=True, errors="coerce"),
                "SwabStatus": v["SwabStatus"]
            }
            for v in g
        ]
    else:
        return []


def _clean_positive_swab_date(x: pd.Series) -> pd.Series:
    """
    1. Noted that for a substantial number of patients, their 'positive' swab date is equal to their
    PCR result date rather than the aquisition date.
    2. Noted that for a substantial number of patients, their 'positive' swab date is equal to their
    2nd PCR rather than the first.

    This function changes the entry to the earliest positive swab date provided.

    :param x: input series (row of dataframe representing one patient)
    :type x: pd.Series
    :return: output series
    :rtype: pd.Series
    """

    # Change the positive swab date to match the earliest positive swab date
    if x["1st_rt-pcr_result"] == 'Positive' and x["2nd_rt-pcr_result"] == 'Positive':
        pat_swab_dates = x[[
            'date_of_positive_covid_swab',
            'date_of_acquisition_of_1st_rt-pcr',
            'date_of_result_of_1st_rt-pcr',
            'date_of_acquisition_of_2nd_rt-pcr',
            'date_of_result_of_2nd_rt-pcr',
        ]]
        x['date_of_positive_covid_swab'] = pat_swab_dates[pat_swab_dates.notna()].min()
    elif x["1st_rt-pcr_result"] == 'Positive':
        pat_swab_dates = x[[
            'date_of_positive_covid_swab',
            'date_of_acquisition_of_1st_rt-pcr',
            'date_of_result_of_1st_rt-pcr',
        ]]
    elif x["2nd_rt-pcr_result"] == 'Positive':
        pat_swab_dates = x[[
            'date_of_positive_covid_swab',
            'date_of_acquisition_of_2nd_rt-pcr',
            'date_of_result_of_2nd_rt-pcr',
        ]]
    else:
        pat_swab_dates = x[['date_of_positive_covid_swab', ]]

    try:
        x['date_of_positive_covid_swab'] = pat_swab_dates[pat_swab_dates.notna()].min()
    except:
        pass

    return x


def parse_date_columns(patients_df: pd.DataFrame) -> pd.DataFrame:
    """
    Additions to original cleaning pipeline:
        1. Convertion of dates stored as numbers (in the excel date format).
        2. Adjustment of Leicester swab dates that were in UK rather than US format.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Cleaning date columns...")

    uk_in_us_format_cols = [
        "Date of Positive Covid Swab",
        "Date of acquisition of 1st RT-PCR",
        "Date of acquisition of 2nd RT-PCR",
    ]

    uk_in_us_format_centres = [
        "University Hospitals of Leicester NHS Trust",
        "St Georges University Hospitals NHS Foundation Trust",
        "The Walton Centre NHS Foundation Trust",
    ]

    # Cleaning and preprocessing for columns expected in US style dates -
    # https://nhsx.github.io/covid-chest-imaging-database/resources
    for col in [col for col in nhsx._US_DATE_COLS if col in patients_df]:
        patients_df[col.lower().replace(" ", "_")] = np.nan

        if col not in uk_in_us_format_cols:
            patients_df[col.lower().replace(" ", "_")] = patients_df[col].map(
                lambda x: _clean_us_dates(x)
            )
        else:
            patients_df.loc[
                ~patients_df['SubmittingCentre'].isin(uk_in_us_format_centres), col.lower().replace(" ", "_")
            ] = patients_df[
                ~patients_df['SubmittingCentre'].isin(uk_in_us_format_centres)
            ][col].map(
                #                 lambda x: _xp_clean_us_dates_leicester(x)
                lambda x: _clean_us_dates(x)

            )

            for sub_centre in uk_in_us_format_centres:
                patients_df.loc[
                    patients_df['SubmittingCentre'] == sub_centre, col.lower().replace(" ", "_")
                ] = patients_df[
                    patients_df['SubmittingCentre'] == sub_centre
                    ][col].map(
                    lambda x: _clean_uk_dates_expected_in_us(x)
                )

    patients_df = patients_df.apply(_clean_positive_swab_date, axis=1)

    # Parsing of columns expected in UK style dates

    if "SwabDates" in patients_df:  # Negative patients only
        # -----------------------------
        # Application of new function
        patients_df["swabdates"] = patients_df["SwabDates"].apply(_swab_dates)
        # -----------------------------

    if "SwabDate" in patients_df:
        patients_df["swabdate"] = pd.to_datetime(
            patients_df["SwabDate"], dayfirst=True, errors="coerce"
        )

    # -----------------------------
    # Application of new function
    patients_df = patients_df.apply(_convert_uk_excel_date, axis=1)
    # -----------------------------

    # Calculate the latest swab date
    if "date_of_positive_covid_swab" in patients_df:
        patients_df["latest_swab_date"] = pd.concat(
            [
                patients_df["date_of_positive_covid_swab"],
                patients_df["swabdate"],
            ],
            axis=1,
        ).max(axis=1)

    # Converts 'filename_latest_date' to datetime for later sense checks
    if 'filename_latest_date' in patients_df:
        patients_df["filename_latest_date"] = pd.to_datetime(
            patients_df["filename_latest_date"], format="%Y-%m-%d", errors="coerce"
        )

    # console_msg.info("Done cleaning dates!")
    return patients_df
