"""
Fixes known mistakes in column headers, places into logical order of submission spreadsheet, and
selects columns for output.
"""

import pandas as pd
import logging
from nccidxclean.clean.dicts_and_maps import original_cols, cleaned_base, all_o2_cols, other_cols, final_col_dict

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def fix_headers(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Fixes known mistakes in column headers.
    Is always run last as it acts on the cleaned columns.

    Orders columns in same order as submission script.

    Originally, the date of the swab for the 'negative' patients was saved in the 'swabdate' field, however,
    this does not make it immediately clear that this is only for negative patients.
    Here 'swabdate' is renamed to 'negative_swab_date' for that reason.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Fixing column headers...")

    if "cxr_severity_3" in patients_df:
        patients_df.rename(
            columns={"cxr_severity_3": "cxr_severity_2"},
            inplace=True,
        )

    # The below has been added to the original NCCID code
    patients_df.drop(["pmh_h1pertension", "o2_saturation"], axis=1, inplace=True)

    # Rename 'swabdate' to 'negative_swab_date'
    patients_df = patients_df.rename({'swabdate': 'negative_swab_date'}, axis=1)

    # console_msg.info("Done fixing column headers!")

    return patients_df


def order_columns(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Put columns in an order consistent with the submission spreadsheet with original columns followed by cleaned
    columns. This assists in spotting systematic errors.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Rearranging columns...")

    new_columns = []
    for col in patients_df.columns.to_list():
        if col not in final_col_dict.keys():
            new_columns.append(col)
            console_msg.warning(f"New column in data: '{col}'")
            logger.warning(f"COL: New column in data: '{col}'")
            final_col_dict[col] = len(final_col_dict.keys())

    patients_df = patients_df[sorted(patients_df.columns.to_list(), key=final_col_dict.get) + new_columns]

    # console_msg.info("Done rearranging columns!")

    return patients_df


def select_output_cols(
        patients_df: pd.DataFrame,
        cols: str
) -> pd.DataFrame:

    """
    Selects columns to be output.

    :param patients_df: dataframe of all clinical data, both original and cleaned
    :type patients_df: pd.DataFrame
    :param cols: columns to return, options are:
        'spo2_imputed_cleaned_only': returns the cleaned columns only, with spo2 imputed from pao2.
        'spo2_imputed_with_original': returns the original and cleaned columns, with spo2 and pao2.
        'o2_split_cleaned_only': returns the cleaned columns only, with spo2 and pao2.
        'o2_split_with_original': returns the original and cleaned columns, with spo2 and pao2.
        'all_cleaned_only': returns all possible cleaned columns.
        'all_with_original': returns all possible original and cleaned columns.
    :type cols: str
    :return: dataframe containing only columns requested
    :rtype: pd.DataFrame
    """
    
    console_msg.info("Selecting output columns...")

    if cols == 'spo2_imputed_cleaned_only':
        cols_to_include = cleaned_base + ['spo2_imputed']
    elif cols == 'spo2_imputed_with_original':
        cols_to_include = original_cols + cleaned_base + ['spo2_imputed']
    elif cols == 'o2_split_cleaned_only':
        cols_to_include = cleaned_base + ['pao2_gas', 'spo2_saturation']
    elif cols == 'o2_split_with_original':
        cols_to_include = original_cols + cleaned_base + ['pao2_gas', 'spo2_saturation']
    elif cols == 'all_cleaned_only':
        cols_to_include = cleaned_base + all_o2_cols + other_cols
    elif cols == 'all_with_original':
        cols_to_include = original_cols + cleaned_base + all_o2_cols + other_cols
    else:
        raise ValueError(cols, 'is not a valid entry for return_cols argument!')

    patients_df = patients_df[cols_to_include]
    patients_df = order_columns(patients_df)

    # console_msg.info("Done selecting output columns!")
    return patients_df
