"""
Parses and cleans numeric columns.
"""

import pandas as pd
import numpy as np
from typing import Optional, Union, Dict
import re
import logging
from nccidxclean.clean.dicts_and_maps import values_to_percentage, ddimer_units, clipping_dict, dd_conversion_factors, clinical_columns

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def _fiO2_mapping(x: str, conv_ltrs_to_percent: bool = True) -> Optional[Union[int, float, str]]:

    _x = x
    if "." in x and x != "0.5":
        # decimal entries are converted to percentages
        # put inside 'try' to catch edge cases such as "."
        try:
            _x = str(round(float(x) * 100))
        except ValueError:
            _x = None
    elif "l" in x.lower():
        try:
            if conv_ltrs_to_percent:
                _x = values_to_percentage[x.lower().rstrip("l")]
            else:
                _x = x.lower().rstrip("l")
        except KeyError:
            _x = None
    elif x in values_to_percentage.keys() and conv_ltrs_to_percent:
        _x = values_to_percentage[str(x)]

    if conv_ltrs_to_percent:
        # makes sure new value is in % value range
        if _x.isdigit() and 21 <= int(_x) <= 100:
            return int(_x)
        else:
            return None
    else:
        if _x.isdigit() and 0 <= int(_x) <= 100:
            # want to return float due to 0.5
            return float(_x)
        elif isinstance(_x, str):
            # still want to return unmapped venturi mask values
            return _x
        else:
            return None


def _raise_date_in_numeric(
        x: pd.Series,
        name: str,
        new_name: str
):

    """
    During development one patient (Covid7741) was found that has a date in the lymphocyte field (1/3/20) and
    another (Covid3469) had a date in Troponin T. It is valuable to manually check the data for additional
    systematic errors for these patients. For these two examples, the other data appeared reasonable.
    :param x: input patient series
    :type x: pd.Series
    :param name: original column name
    :type name: str
    :param new_name: cleaned column name
    :type new_name: str
    :return: None
    """

    if pd.isnull(x[new_name]):
        date_entered = re.search(r"^\d+(/|-)\d+(/|-)\d+", str(x[name]))
        if date_entered:
            logger.error(f"NUM: {x['Pseudonym']}: date entered in {name}")

    return None


def _merge_o2_sat_into_pao2(
        x: pd.Series
) -> pd.Series:

    """
    Only one value in o2 saturation -> merge column into pao2 prior to processing.
    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    if pd.isnull(x["pao2"]) and pd.notnull(x["o2_saturation"]):
        x["pao2"] = x["o2_saturation"]
    return x


def _split_and_clean_pao2(
        x: pd.Series,
        exclude_venous_gas: bool = True
) -> pd.Series:

    """
    Mislablled - PaO2 is usually referred to as the Partial Pressure of Oxygen on a Blood Gas.
    Oxygen saturation is an approximation of this using different units for PaO2 (mmHg rather than kPa).

    Some centres have provided a PaO2 in kPa (easily seen on a histogram):
    - 3 centres (Charing Cross, St. Mary's Hospital, Hammersmith Hospital) have given exlusively PaO2 in kPa.
    None of their results are >50.
    - Other corrections:
        - Royal United Hospitals Bath has: 1. Put some FiO2's in (e.g. 0.21, 21, 26, 38, 50); and 2. Put some blood gases in.
        - Ealing Hospital and Ashford and St Peters have put fio2 in by mistake.
        - Oxford University Hospitals and Liverpool Heart and Chest Hospital have put 0.XX where XX is the oxygen saturation.

    Solutions:
        - Integer values between and including 21-50 are taken as FiO2 if FiO2 is blank. If FiO2 appears blank a warning is raised.
        - Any common oxygen fractions <=0.5, e.g. 0.21 are assumed to be FiO2 if FiO2 is blank. If FiO2 is not blank a warning is raised.
        - Any values for that 0.5<=pao2<=1 holds are assumed to be o2 sats and are multiplied by 100.
        - Column split into pao2_gas and spo2_saturation

    N.B. pao2 < 7 is likely venous - these are removed by default.

    :param x: input patient series
    :type x: pd.Series
    :param exclude_venous_gas: Use pao2 < 7 to exlude likely venous gases
    :type exclude_venous_gas: bool
    :return: output patient series
    :rtype: pd.Series
    """

    # Assume decimals between 0.21 and 0.5 inclusive are FiO2s if FiO2 is blank.
    if pd.isnull(x['fio2']) and x['pao2'] <= 0.21 and x['pao2'] <= 0.5 and re.match(r'0.[0-9]{2}|0.[0-9]{1}',
                                                                                    str(x['pao2'])):
        x['fio2'] = x['pao2'] * 100

    # Take integer values between and including 21-50 are taken as FiO2 if FiO2 is blank.
    if pd.isnull(x['fio2']) and x['pao2'] >= 21 and x['pao2'] <= 50 and x['pao2'].is_integer():
        x['fio2'] = x['pao2']

    # Any values 0.5<=pao2<=1 are assumed to be o2 sats and are multiplied by 100.
    if x['pao2'] > 0.5 and x['pao2'] <= 1.0 and re.match(r'0.[0-9]{2}|0.[0-9]{1}|1.0', str(x['pao2'])):
        x['spo2_saturation'] = x['pao2'] * 100

    # Split the column into likely blood gas PaO2's and likely oxygen saturations
    if x['pao2'] >= 50 and x['pao2'].is_integer():
        x['spo2_saturation'] = x['pao2']
    elif x['pao2'] > 1 and x['pao2'] < 21:
        x['pao2_gas'] = x['pao2']
    elif x['pao2'] >= 21 and x['pao2'] < 50 and pd.notna(x['fio2']) and x['pao2'] != x['fio2']:
        x['pao2_gas'] = x['pao2']

    if exclude_venous_gas == True and x['pao2_gas'] <= 7:
        x['pao2_gas'] = np.nan

    # To make blood gas comparible to oxygen saturation, convert to mmHg from kPa
    if pd.notna(x['pao2_gas']):
        x['pao2_gas'] = round(x['pao2_gas'] / 0.133322)

    return x


def _impute_pao2_spo2(
        x: pd.Series,
) -> pd.Series:

    """
    Uses non-linear equation from Gadrey et al. 2019 to infer SpO2 and PaO2 from the corresponding values.

    Gadrey, Shrirang & Lau, Chelsea & Clay, Russ & Rhodes, Garret & Lake, Douglas & Moore, Christopher & Voss,
    John & Moorman, Joseph. (2019). Imputation of partial pressures of arterial oxygen using oximetry and
    its impact on sepsis diagnosis. Physiological Measurement. 40. 10.1088/1361-6579/ab5154.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    x.pao2_imputed = x.pao2_gas
    if pd.notna(x.spo2_saturation):
        s = x.spo2_saturation / 100
        x.pao2_imputed = ((28.6025 ** 3) / ((1 / s) - 0.99)) ** (1 / 3)
        x.pao2_imputed = round(x.pao2_imputed)

    x.spo2_imputed = x.spo2_saturation
    if pd.notna(x.pao2_gas):
        g = x.pao2_gas
        x.spo2_imputed = 100 * (((28.6025 ** 3) / (g ** 3)) + 0.99) ** (-1)
        x.spo2_imputed = round(x.spo2_imputed)

    return x


def _clean_creatinine(
        x: Union[int, float],
) -> Union[int, float]:

    """
    Cleans the creatinine data.  Issues:
        - 16 values are <20.
        - Some are less than 0.5 and appear to be in mmol/L rather than µmol/L.
        - Others between 0.5 and 20.0 are decimals, but too large for mmol/L.  These could be errors
          (e.g. decimal placement) or in mg/dL. Tried converting mg/dL (dividing by 0.0113) but figures not
          consistent with pmh_ckd and urea. These values are clipped and removed along with non-integer values
          later (also may be mg/dL).

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    # If <0.5, likely a creatinine in mmol/L (e.g. from POC machine) -> converted to micromols/L.
    if x < 0.4:
        return x * 1000
    else:
        return x


def _truncate_numerical_field(
        x: Union[int, float],
        trunc_min: Optional[Union[int, float]] = None,
        trunc_max: Optional[Union[int, float]] = None
) -> Union[int, float]:
    """
    Truncate numeric value.

    :param x: Original numeric value
    :type x: int, float
    :param trunc_min: Minimum value to truncate field
    :type trunc_min: int, float
    :param trunc_max: Maximum value to truncate field
    :type trunc_max: int, float
    :return: Truncated numeric value
    :rtype: int, float
    """
    if trunc_min != None and x < trunc_min:
        x = trunc_min
    if trunc_max != None and x > trunc_max:
        x = trunc_max
    return x


def rescale_fio2(patients_df: pd.DataFrame, ltrs_to_percent: bool = True) -> pd.DataFrame:
    """
    Remaps FiO2 entries to the % scale. Changes:
        1. 0.5 is more likely 0.5L (23%) than 50%;
        2. Minimum oxygen is 21% (room air), not 0% -> 0 should be 21% and minimum value should be 21%;
        3. Handle the 'Any supplemental oxygen: FiO2' data.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :param ltrs_to_percent: convert values suspected to be in L to % scale
    :type ltrs_to_percent: bool, default True
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Cleaning fio2...")

    # To handle the 'Any supplemental oxygen: FiO2' column:
    if "Any supplemental oxygen: FiO2" in patients_df:
        # if 'Any supplemental oxygen: FiO2' included instead of "FiO2" / "FiO2 is blank", use this data
        if "FiO2" not in patients_df:
            patients_df["FiO2"] = patients_df["Any supplemental oxygen: FiO2"].copy()
        else:
            patients_df["FiO2"] = patients_df["FiO2"].fillna(patients_df["Any supplemental oxygen: FiO2"])

    if "FiO2" in patients_df:
        patients_df["fio2"] = patients_df["FiO2"].map(
            lambda x: _fiO2_mapping(str(x), conv_ltrs_to_percent=ltrs_to_percent)
        )

    # console_msg.info("Done cleaning fio2!")
    return patients_df


def _additional_known_errors_check(
        x: pd.Series,
        name: str,
        new_name: str
) -> pd.Series:

    """
    Original code results in:
    1. Loss of data for CRP on admission, Ferritin and Troponin I:
        - nccid function fails to extract integer from known error in "[6] - 2020-05-19" format
        - nccid function results in NaN if entry includes "<" or ">" (e.g. CRP = <4 becomes NaN but is 'normal')
    2. Loss of data for Temperature on admission:
        - Previous function turns 37.4 (36 hours post admission, 1st available) becomes NaN for Covid 154

    :param x: input patient series
    :type x: pd.Series
    :param name: original column name
    :type name: str
    :param new_name: cleaned column name
    :type new_name: str
    :return: output patient series
    :rtype: pd.Series
    """

    if pd.isnull(x[new_name]):

        match_error_int = re.search(r"\[(\d+)\]", str(x[name]))
        #             match_error_lt_gt = re.search(r"\[(<|>)(\d+)\]", str(x[name]))
        #             lt_gt_dropped = re.search(r"(<|>)(\d+)", str(x[name]))
        match_error_lt_gt = re.search(r"\[(<|>)(\d+(?:\.\d+)?)\]", str(x[name]))
        lt_gt_dropped = re.search(r"(<|>)(\d+(?:\.\d+)?)", str(x[name]))
        lt_gt_space = re.search(r"(<|>) (\d+(?:\.\d+)?)", str(x[name]))
        float_with_comment = re.search(r"(\d+\.?\d+) \(.*?\)", str(x[name]))
        if match_error_int:
            x[new_name] = float(match_error_int.group(1))
        elif match_error_lt_gt:
            x[new_name] = float(match_error_lt_gt.group(2))
        elif lt_gt_dropped:
            x[new_name] = float(lt_gt_dropped.group(2))
        elif lt_gt_space:
            x[new_name] = float(lt_gt_space.group(2))
        elif float_with_comment:
            x[new_name] = float(float_with_comment.group(1))
        else:
            pass
    return x


def _infer_ddimer_units(
        dd_df: pd.DataFrame,
        ddimer_dict: Dict[str, str]
) -> Dict[str, str]:

    """
    Infers the D-dimer units used by a centre if they are not in the d-dimer unit dictionary.

    :param dd_df: input dataframe of clinical data
    :type dd_df: pd.DataFrame
    :param ddimer_dict: d-dimer units for each center
    :type ddimer_dict: Dict[str, str]
    :return: output dataframe of clinical data
    :rtype: pd.DataFrame
    """

    # First find the centres not seen before
    centres_unk_units = dd_df[
        (dd_df['d-dimer_on_admission'].notnull()) &
        (~dd_df['SubmittingCentre'].isin(ddimer_dict.keys()))
        ]['SubmittingCentre'].unique().tolist()

    # Secondly, if there is more than 5, use the median value to infer the units
    if len(centres_unk_units) > 0:
        for centre_unk in centres_unk_units:
            if len(dd_df[dd_df['SubmittingCentre'] == centre_unk]) >= 5:

                centre_median = dd_df[
                    (dd_df['d-dimer_on_admission'].notnull()) &
                    (dd_df['SubmittingCentre'] == centre_unk)
                    ]['d-dimer_on_admission'].median()

                if centre_median < 50:
                    ddimer_dict[centre_unk] = "μg/mL FEU"
                else:
                    ddimer_dict[centre_unk] = "ng/mL FEU"

                console_msg.warning(
                    f"{centre_unk} not in the development data...\n"
                    f"      -> D-dimer Units have been inferred as {ddimer_dict[centre_unk]} using the mean ({centre_median})."
                )
                logger.warning(f"CENTRE: d-dimer units inferred for: {centre_unk}")

            else:
                console_msg.warning(
                    f"{centre_unk} not in the development data\n"
                    f"      -> Unable to infer d-dimer units as less than 5 entries."
                )

    return ddimer_dict


def _clean_d_dimer(
        x: pd.Series,
        units: Dict[str, str],
        dd_conv_factors: Dict[str, int]
) -> pd.Series:

    """
    Clean d-dimer column to put all values into the same units.

    :param x: input patient series
    :type x: pd.Series
    :param ddimer_units: d-dimer units for each center
    :type ddimer_units: Dict[str, str]
    :param dd_conv_factors: required conversion factor for d-dimer for each center
    :type dd_conv_factors: Dict[str, int]
    :return: output patient series
    :rtype: pd.Series
    """

    if pd.notnull(x['d-dimer_on_admission']):
        try:
            x['d-dimer_on_admission'] = x['d-dimer_on_admission'] * dd_conv_factors[
                units[x['SubmittingCentre']]]
        except KeyError:
            pass

    return x


def _merge_Ferritin_2(x: pd.Series) -> pd.Series:

    """
    Merge the Ferritin 2 column which is outside of the schema into the Ferritin column.

    :param x: input patient series
    :type x: pd.Series
    :return: output patient series
    :rtype: pd.Series
    """

    if x['hospital'] == "Norfolk and Norwich University Hospital" and pd.isnull(x['ferritin']) and str(
            x['Ferritin 2']).replace(".", "", 1).isnumeric():
        x['ferritin'] = float(x['Ferritin 2'])
    return x


def _check_sheffield_trops(data: pd.DataFrame):

    """
    Check if Troponin's from Sheffield are included and provide a warning. Sheffield stated it uses ng/mL rather
    than ng/L for Troponin I.  However, no Sheffield Troponin I's in development data to confirm.

    :param data: dataframe of clinical data
    :type data: pd.DataFrame
    :return: None
    :rtype: NoneType
    """

    if 'Sheffield' in data[data['troponin_i'].notnull()]['hospital'].unique():
        console_msg.warning("Sheffield stated ng/mL used rather than ng/L for Troponin I but\n"
                            "       no Troponin I's from Sheffield included in the development data.\n"
                            "       Please check and divide by 1000 if appropriate.")
        logger.warning("CENTRE: Sheffield troponins included")

    return


def clean_numeric(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Cleans the numerical columns.  May only be performed after the NHSx _coerce_numeric_columns function.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Cleaning numeric columns...")

    for col in [col for col in clinical_columns if col in patients_df]:
        new_col = col.lower().replace(" ", "_").replace(",", "")
        patients_df = patients_df.apply(_additional_known_errors_check, args=(col, new_col,), axis=1)
        patients_df.apply(_raise_date_in_numeric, args=(col, new_col,), axis=1)

    # PaO2
    patients_df = patients_df.apply(_merge_o2_sat_into_pao2, axis=1)
    patients_df['pao2_gas'] = np.nan
    patients_df['spo2_saturation'] = np.nan
    patients_df = patients_df.apply(_split_and_clean_pao2, axis=1, args=(True,))
    console_msg.warning("PaO2 < 7 kPa (52 mmHg) may be venous -> these have been excluded")
    console_msg.warning("SpO2/PaO2 should be considered in conjunction with FiO2")
    patients_df['pao2_imputed'] = np.nan
    patients_df['spo2_imputed'] = np.nan
    patients_df = patients_df.apply(_impute_pao2_spo2, axis=1)
    # Truncate pa02 to estimated values
    patients_df['pao2_imputed'] = patients_df['pao2_imputed'].apply(_truncate_numerical_field, args=(None, 105))
    patients_df['spo2_imputed'] = patients_df['spo2_imputed'].apply(_truncate_numerical_field, args=(None, 100))

    # Creatinine
    patients_df['creatinine_on_admission'] = patients_df['creatinine_on_admission'].apply(_clean_creatinine)

    # CRP
    patients_df['crp_on_admission'] = patients_df['crp_on_admission'].apply(_truncate_numerical_field,
                                                                            args=(4, None))

    # D-dimer
    dd_units = _infer_ddimer_units(patients_df, ddimer_units)
    patients_df = patients_df.apply(_clean_d_dimer, axis=1, args=(dd_units, dd_conversion_factors,))
    patients_df['d-dimer_on_admission'] = patients_df['d-dimer_on_admission'].apply(_truncate_numerical_field,
                                                                                    args=(150, 10000))

    # Ferritin
    patients_df = patients_df.apply(_merge_Ferritin_2, axis=1)
    patients_df['ferritin'] = patients_df['ferritin'].apply(_truncate_numerical_field, args=(0, 15000))

    # Truncate troponins
    patients_df['troponin_i'] = patients_df['troponin_i'].apply(_truncate_numerical_field, args=(10, None))
    _check_sheffield_trops(patients_df[['hospital', 'troponin_i']])
    patients_df['troponin_t'] = patients_df['troponin_t'].apply(_truncate_numerical_field, args=(5, 25000))

    # Clean Fibrinogen
    patients_df.loc[
        patients_df[
            'fibrinogen__if_d-dimer_not_performed'
        ] > 20, 'fibrinogen__if_d-dimer_not_performed'
    ] = patients_df['fibrinogen__if_d-dimer_not_performed'] / 100

    patients_df[
        'fibrinogen__if_d-dimer_not_performed'
    ] = patients_df[
        'fibrinogen__if_d-dimer_not_performed'
    ].apply(_truncate_numerical_field, args=(None, 4.5))

    # console_msg.info("Done cleaning numeric columns!")
    return patients_df


def clip_numeric(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Removed values outside of expected limits.
    Is called after other numeric functions.

    New:
        - Additional clipping of numerical fields
        - Removal of non-integer numbers from integer fields

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """
    
    console_msg.info("Clipping numeric columns...")

    for col, limits in clipping_dict.items():

        # Clip numerical fields
        if col in patients_df and limits[0] != None and limits[1] != None:
            patients_df[col] = patients_df[
                col
            ].apply(lambda x: x if limits[0] <= x <= limits[1] else np.nan)
        elif col in patients_df and limits[0] == None and limits[1] != None:
            patients_df[col] = patients_df[
                col
            ].apply(lambda x: x if limits[1] >= x else np.nan)
        elif col in patients_df and limits[1] == None and limits[0] != None:
            patients_df[col] = patients_df[
                col
            ].apply(lambda x: x if limits[0] <= x else np.nan)

        # Remove values that are not integer when would expect them to be.
        if col in patients_df and limits[2] == 'integer':
            patients_df[col] = patients_df[
                col
            ].apply(lambda x: x if x.is_integer else np.nan)

    # console_msg.info("Done clipping!")
    return patients_df
