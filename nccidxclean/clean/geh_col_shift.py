"""
Corrects known error for George Eliot Hospital where data is shifted one-column-to-the-left.
"""

import pandas as pd
from typing import Dict
import logging
from nccidxclean.clean.dicts_and_maps import geh_submission_columns

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def _correct_geh(x: pd.Series, geh_error_map: Dict) -> pd.Series:
    _x = x.copy()
    if x['SubmittingCentre'] == 'George Eliot Hospital NHS Trust' and "/" in str(x['Troponin T']) and "/" in str(
            x['Current NSAID used']):
        for key, value in geh_error_map.items():
            _x[key] = x[value]
    return _x


def column_shift(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Originally, the majority of columns for George Eliot Hospital (GEH) had their data entered in the column
    one-to-the-left when placed in the order of the submission spreadsheet.
    Most have now been corrected but a small number remain, which are fixed by this function.

    All of these errors have a date entered in the 'Current NSAID used', 'Troponin T' and
    'Final COVID Status' columns allowing them to be identified and corrected.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    console_msg.info("Checking for and correcting column shift...")

    # column order specified in the excel file

    not_affected_cols = ['SubmittingCentre', 'Hospital', 'Age']
    affected_cols = [elem for elem in geh_submission_columns if elem not in not_affected_cols]
    geh_error_mapping = {}
    for i in range(1, len(affected_cols)):
        geh_error_mapping[affected_cols[i]] = affected_cols[i - 1]

    patients_df = patients_df.apply(_correct_geh, args=(geh_error_mapping, ), axis=1)

    # console_msg.info("Done checking and correcting column shift!")

    return patients_df
