"""
Utility functions to:
    1. extract data from old jsons to compare to current data
    2. produce outputs for checking warnings/errors from the log file
    3. merge checked data with the cleaned dataframe
"""
import re

import pandas as pd
import numpy as np
import os
from pathlib import Path
import json
from jsondiff import diff
from typing import Union, Tuple, Collection, Optional, NoReturn, Dict
import logging
from datetime import date
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.backends import default_backend
import base64
from nccid_cleaning import etl

from nccidxclean.clean.dicts_and_maps import read_in_types_dict, date_cols, cleaned_base

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def read_in_clinical_data(
        base_dir: Union[str, os.PathLike],
        clin_subdir: Union[str, os.PathLike],
) -> pd.DataFrame:

    """
    Reads in the clinical data from the json files.

    :param base_dir: base directory - should have same structure as original S3 bucket
    :type base_dir: Union[str, os.PathLike]
    :param clin_subdir: subdirectory containing the clinical data json files
    :type clin_subdir: Union[str, os.PathLike]
    :return: dataframe of clinical data with a row for each patient
    :rtype: pd.DataFrame
    """

    patient_files = list(os.walk(Path(base_dir) / clin_subdir))
    patients_df = etl.patient_jsons_to_df(patient_files)
    return patients_df


def extract_old_data(
        x: pd.Series,
        base_path: os.PathLike,
        patient_subdir: Union[str, os.PathLike],
) -> pd.Series:

    """
    Finds columns / data fields deleted from the most recent jsons compared to earlier jsons.

    :param x: input patient series from clinical data dataframe.
    :type x: pd.Series
    :param base_path: The base path to the JSON directory
    :type base_path: Path
    :param patient_subdir: The subdirectory for the patient
    :type patient_subdir: Path
    :return: output patient series including new column containing deleted patient data
    :rtype: pd.Series
    """
    
    console_msg.info("Extracting data from old jsons...")

    if x['filename_earliest_date'] != x['filename_latest_date']:
        first_json = base_path / patient_subdir / x["Pseudonym"] / ("data_" + x['filename_earliest_date'] + ".json")
        latest_json = base_path / patient_subdir / x["Pseudonym"] / ("data_" + x['filename_latest_date'] + ".json")

        if not os.path.exists(first_json):
            first_json = Path(str(first_json).replace("data_", "status_"))

        if not os.path.exists(latest_json):
            latest_json = Path(str(latest_json).replace("data_", "status_"))

        with open(first_json, "r") as f:
            first_record = json.load(
                f,
                object_hook=lambda d: dict(
                    d,
                    **d.get("OtherDataSources", {}).get(
                        "SegmentationData", {}
                    )
                ),
            )

        with open(latest_json, "r") as f:
            latest_record = json.load(
                f,
                object_hook=lambda d: dict(
                    d,
                    **d.get("OtherDataSources", {}).get(
                        "SegmentationData", {}
                    )
                ),
            )

        #         diff_dict[x["Pseudonym"]] = diff(first_record, latest_record, marshal=True)
        try:
            deleted = diff(first_record, latest_record, marshal=True)['$delete']
            deleted_dict = {}
            for col in deleted:
                deleted_dict[col] = first_record[col]
            deleted_dict['ID'] = x["Pseudonym"]
            x['deleted'] = deleted_dict
        except:
            x['deleted'] = np.nan

    console_msg.info("Done extracting from old jsons!")

    return x


def create_xclean_id(
        patient: pd.Series
) -> str:

    """
    Creates a unique ID for each patient using their original data. This may be used as a password to
    encrypt the data, only allowing access to data users with the original data.

    :param patient: row of dataframe representing a patient
    :type patient: pd.Series
    :return: unique ID for the patient generated from their original data
    :rtype: str
    """

    patient_copy = patient.copy().fillna("")
    xclean_id = str(patient_copy.Pseudonym.replace("Covid", ""))
    xclean_id += str(patient_copy.Age)
    xclean_id += str(patient_copy.Sex)
    xclean_id += str(patient_copy.Ethnicity)
    xclean_id += str(patient_copy['Date of Positive Covid Swab'])
    xclean_id += str(patient_copy.SwabDate)
    xclean_id += str(patient_copy['Date of admission'])
    xclean_id += str(patient_copy['Date of death'])
    xclean_id += str(patient_copy.SubmittingCentre)
    xclean_id += "0123456789123456"
    xclean_id = xclean_id.replace(" ", "")[0:16].strip()

    return xclean_id


def create_error_report(
        patients_df: pd.DataFrame,
        log_path: str
) -> pd.DataFrame:

    """
    Produces an Excel file for checking warnings/errors saved in the current
    working directory.

    :param patients_df: clinical data output by pipeline
    :type patients_df: pd.DataFrame
    :param log_path: path to the log file
    :type log_path: str
    :return: clinical data
    :rtype: pd.DataFrame
    """

    def _read_log(
            lpath: str
    ) -> pd.DataFrame:

        """
        Creates a dataframe from the log file

        :param lpath: path to the log file
        :type lpath: str
        :return: dataframe of logs
        :rtype: pd.DataFrame
        """

        with open(lpath) as f:
            logs = [line.strip("\n").split("-", maxsplit=3) for line in f.readlines()]
            log_df = pd.DataFrame(logs, columns=['dt', 'module', 'level', 'message'])
            log_df.module = log_df.module.str.split('.').str[1]
        return log_df

    def _agg_str_errors(df: pd.DataFrame) -> pd.DataFrame:
        return df.groupby('Pseudonym').to_review.agg(lambda x: x.tolist()).reset_index()

    def _numeric_errors(
            clin_df: pd.DataFrame,
            log_df: pd.DataFrame
    ) -> pd.DataFrame:

        """
        Creates dataframe of warnings/errors in the numeric columns for review

        :param clin_df: clinical data output by pipeline
        :type clin_df: pd.DataFrame
        :param log_df: dataframe of logs
        :type log_df: pd.DataFrame
        :return: warnings/errors in the numeric columns for review
        :rtype: pd.DataFrame
        """

        num_errs_log = log_df[log_df.message.str.contains(r"NUM:", regex=True)]
        num_errs_log = num_errs_log.message.str.split(": ", expand=True)
        num_errs_log = num_errs_log.drop(0, axis=1).rename({1: "Pseudonym", 2: 'to_review'}, axis=1)
        # Add column to enter notes
        num_errs = _agg_str_errors(num_errs_log).merge(clin_df, how='left', on='Pseudonym')
        num_errs.insert(2, "notes", "")

        return num_errs

    def _date_errors(
            clin_df: pd.DataFrame,
            log_df: pd.DataFrame
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:

        """
        Creates dataframe of warnings/errors in date columns for review

        :param clin_df: clinical data output by pipeline
        :type clin_df: pd.DataFrame
        :param log_df: dataframe of logs
        :type log_df: pd.DataFrame
        :return: warnings/errors in date columns for review
        :rtype: pd.DataFrame
        """

        # 1. Error placed in column where UK formatted date found in US date column or
        # PCR dates expected in UK format in US format
        uk_in_us = clin_df.loc[(clin_df == 'error_expected_us').any(axis=1)]
        if len(uk_in_us) != 0:
            uk_in_us['to_review'] = uk_in_us.apply(lambda row: row[row == 'error_expected_us'].index.tolist(), axis=1)
            uk_in_us['to_review'] = uk_in_us['to_review'].apply(lambda x: [y + ' expected in US format' for y in x])
            uk_in_us = uk_in_us[['Pseudonym', 'to_review']]
        else:
            uk_in_us = None

        us_in_uk = clin_df.loc[(clin_df == 'error_expected_UK').any(axis=1)]
        if len(us_in_uk) != 0:
            us_in_uk['to_review'] = us_in_uk.apply(lambda row: row[row == 'error_expected_UK'].index.tolist(), axis=1)
            us_in_uk['to_review'] = us_in_uk['to_review'].apply(lambda x: [y + ' expected in UK format' for y in x])
            us_in_uk = us_in_uk[['Pseudonym', 'to_review']]
        else:
            us_in_uk = None

        # 2. Dates do not follow logical order
        date_errs_log = log_df.loc[log_df.message.str.contains(r"DATE: ", regex=True)]
        if len(date_errs_log) != 0:
            date_errs_log = date_errs_log.message.str.split(": ", expand=True) \
                .drop(0, axis=1) \
                .rename({1: "Pseudonym", 2: 'to_review'}, axis=1)
        else:
            date_errs_log = None

        # Merge dfs
        dt_errs_all = pd.concat([uk_in_us, us_in_uk, date_errs_log])
        if len(dt_errs_all) != 0:
            dt_errs = _agg_str_errors(dt_errs_all).merge(clin_df, how='left', on='Pseudonym')
            # Reduce output columns
            date_cols = [col for col in dt_errs.columns.tolist() if "date" in col.lower()]
            # Add column to enter notes
            dt_errs['notes'] = ""
            dt_errs = dt_errs[
                ['Pseudonym', 'to_review', 'notes', 'SubmittingCentre', 'hospital'] + date_cols
                ]
        else:
            dt_errs = None

        # Clear error warnings in dataframe
        clin_df = clin_df.replace('error_expected_UK', np.nan)
        clin_df = clin_df.replace('error_expected_UK', np.nan)

        return clin_df, dt_errs

    def _create_summary(log_df: pd.DataFrame) -> pd.DataFrame:

        """
        creates a summary dataframe of warnings/errors for review.

        :param log_df: dataframe generated from log file
        :type log_df: pd.DataFrame
        :return: summary of potential errors
        :rtype: pd.DataFrame
        """

        # Error Type, Error Subtype, Value, Action

        # Column Errors
        def _col_err_summary(logging_df: pd.DataFrame) -> Optional[pd.DataFrame]:
            col_errs = logging_df[logging_df.message.str.contains(r"COL: ", regex=True)]
            if len(col_errs) == 0:
                return None
            col_errs = col_errs.message.str.split(": ", expand=True)
            col_errs = col_errs.rename({
                0: 'Error Type',
                1: "Error Subtype",
                2: 'Value'
            }, axis=1)
            col_errs['Action'] = "Review in dataframe / use EDA"
            return col_errs

        # Centre Errors
        def _centre_err_summary(logging_df: pd.DataFrame) -> Optional[pd.DataFrame]:
            cent_errs = logging_df[logging_df.message.str.contains(r"CENTRE: ", regex=True)]
            if len(cent_errs) == 0:
                return None
            cent_errs = cent_errs.message.str.split(": ", expand=True)
            cent_errs = cent_errs.rename({
                0: 'Error Type',
                1: "Error Subtype",
                2: 'Value'
            }, axis=1)
            cent_errs['Action'] = "Review in dataframe / use EDA"
            return cent_errs

        # Dates before 2019, after 2022, and after filename_lastest_date
        def _dt_removed_summary(logging_df: pd.DataFrame) -> pd.DataFrame:
            dt19_log = logging_df[logging_df.message.str.contains(r"DATE:.*2019", regex=True)]
            dt22_log = logging_df[logging_df.message.str.contains(r"DATE:.*2022", regex=True)]
            dtfilelatest_log = logging_df[
                logging_df.message.str.contains(r"DATE:.*after filename_latest_date", regex=True)]

            dt_removed = pd.DataFrame([
                ['Dates before 2019', len(dt19_log)],
                ['Dates after 2022', len(dt22_log)],
                ['Dates after filename_latest_date', len(dtfilelatest_log)]
            ], columns=['Error Subtype', 'Value'])
            dt_removed['Error Type'] = "DATE"
            dt_removed['Action'] = "Review dates check sheet"
            return dt_removed[dt_removed.Value != 0]

        # Date formatting errors
        def _dt_format_err_summary(logging_df: pd.DataFrame) -> pd.DataFrame:
            dt_uk_in_us = logging_df[logging_df.message.str.contains(r"DATE FORMAT: UK", regex=True)]
            dt_us_in_uk = logging_df[logging_df.message.str.contains(r"DATE FORMAT: PCR", regex=True)]
            dt_frmt = pd.DataFrame([
                ['UK dates found in US date column', len(dt_uk_in_us)],
                ['PCR dates expected in UK format in US', len(dt_us_in_uk)]
            ], columns=['Error Subtype', 'Value'])
            dt_frmt['Error Type'] = "DATE FORMAT"
            dt_frmt['Action'] = "Review dates check sheet"
            return dt_frmt[dt_frmt.Value != 0]

        # Illogical date errors
        def _illogical_dts_summary(logging_df: pd.DataFrame) -> pd.DataFrame:
            ill_dt_log = logging_df[logging_df.message.str.contains(r"DATE:", regex=True)]
            ill_dt = pd.DataFrame([
                ['DATE', 'Illogical date order', len(ill_dt_log), "Review dates check sheet"],
            ], columns=['Error Type', 'Error Subtype', 'Value', 'Action'])
            return ill_dt[ill_dt.Value != 0]

        # Numeric errors
        def _num_err_summary(logging_df: pd.DataFrame) -> Optional[pd.DataFrame]:
            num_log = logging_df[logging_df.message.str.contains(r"NUM:", regex=True)]
            if len(num_log) == 0:
                return None
            num_errs = num_log.message.str.split(": ", expand=True)
            num_errs = num_errs.rename({
                0: "Error Type",
                1: "Pseudonym",
                2: "Details"
            }, axis=1)
            num_errs['Error Subtype'] = np.nan
            num_errs.loc[num_errs.Details.str.contains(
                "Lymphocyte count > WCC", regex=True
            ), 'Error Subtype'] = "Lymphocyte count > WCC"
            num_errs.loc[num_errs.Details.str.contains(
                "but.*=0", regex=True
            ), 'Error Subtype'] = 'Inconsistent dates and categoricals'
            num_errs.loc[num_errs.Details.str.contains(
                "date entered in", regex=True
            ), 'Error Subtype'] = 'Dates in numeric columns'

            num_err_pivot = num_errs[['Error Type', 'Error Subtype']].pivot_table(
                index="Error Type", columns="Error Subtype", values="Error Subtype", aggfunc=len
            )
            num_err_stacked = num_err_pivot.stack().reset_index()
            num_err_stacked = num_err_stacked.rename({0: 'Value'}, axis=1)
            num_err_stacked["Action"] = "Review numeric check sheet"
            return num_err_stacked

        summary_df = pd.concat([
            _col_err_summary(log_df),
            _centre_err_summary(log_df),
            _dt_removed_summary(log_df),
            _dt_format_err_summary(log_df),
            _illogical_dts_summary(log_df),
            _num_err_summary(log_df),
        ])[['Error Type', 'Error Subtype', 'Value', 'Action']].reset_index(drop=True)

        # Add notes column
        summary_df['Notes'] = ""

        return summary_df

    # Create xclean id used to hash any data updates after reviewing errors
    patients_df['xclean_id'] = patients_df.apply(create_xclean_id, axis=1)

    console_msg.info('Creating warning/error report...')

    # Read in error log
    log = _read_log(log_path)

    # Create component error files
    numeric_errs = _numeric_errors(patients_df, log)
    patients_df, date_errs = _date_errors(patients_df, log)
    err_summary = _create_summary(log)

    # Remove patients who have been previously checked
    prev_check_id_path = Path(__file__).resolve().parents[2] / "data_updates" / 'check_history.txt'
    if prev_check_id_path.is_file():
        with open(prev_check_id_path, "r") as f:
            prev_checked_ids = [patient.strip("\n").strip() for patient in f.readlines()]
        numeric_errs = numeric_errs.loc[~numeric_errs.index.isin(prev_checked_ids)]
        date_errs = date_errs.loc[~date_errs.index.isin(prev_checked_ids)]

    # Save error files into csv files for review
    to_review_folder = Path("./for_review")
    to_review_folder.mkdir(exist_ok=True)
    numeric_errs.to_csv(to_review_folder / 'numeric_warnings.csv', index=False)
    date_errs.to_csv(to_review_folder / 'date_warnings.csv', index=False)
    err_summary.to_csv(to_review_folder / 'warning_summary.csv', index=False)

    # with pd.ExcelWriter('warnings_to_review.xlsx') as writer:
    #     err_summary.to_excel(writer, sheet_name='Summary', index=False)
    #     numeric_errs.to_excel(writer, sheet_name='Numeric_Warnings', index=False)
    #     date_errs.to_excel(writer, sheet_name='Date_Warnings', index=False)

    return patients_df


def _date_differences_as_timedelta(
        original_df: pd.DataFrame,
        new_df: pd.DataFrame,
        cols: Collection
) -> pd.DataFrame:

    """
    Converts manual changes in dates after review into a timedelta (i.e. amount of time before or after the original).

    :param original_df: original cleaned clinical data
    :type original_df: pd.DataFrame
    :param new_df: modifications to date fields
    :type new_df: pd.DataFrame
    :param cols: date columns on which to perform conversion
    :type cols: Collection
    :return: modifications to date fields in form of timedelta from the original
    :rtype: pd.DataFrame
    """

    # Select patients in update dataframe from the original cleaned data
    original_patients = original_df.loc[original_df.index.isin(new_df.index.tolist())]
    original_patients = original_patients.sort_values('Pseudonym')
    new_df = new_df.sort_values('Pseudonym')
    # Compare the two dateframes
    df = original_patients[cols].replace("NULL", pd.NaT).sort_values(by='Pseudonym').compare(
        new_df[cols].replace("NULL", pd.NaT).sort_values(by='Pseudonym'), align_axis=0, keep_equal=True
    )
    # Convert changes to timedelta
    return df.groupby(level='Pseudonym').diff()


def _encrypt_patient(
        row: pd.Series,
        id_dict: Dict,
        columns: Collection
) -> pd.Series:

    """
    Encrypt a row representing one patient using their xclean_id.

    :param row: patient's row from the updates
    :type row: pd.Series
    :param id_dict: dictionary with the patient's pseudonym as the key and the xclean_id the value
    :type id_dict: Dict
    :param columns: columns to decrypt
    :type columns: Collection
    :return: encrypted patient's row from the updates
    :rtype: pd.Series
    """

    kdf = PBKDF2HMAC(  # create an instance of the KDF
        algorithm=hashes.SHA256(),
        length=32,
        salt=row.salt,
        iterations=100000,
        backend=default_backend()
    )

    if row.Pseudonym in id_dict.keys():
        xclean_id = id_dict[row.Pseudonym]
        password = xclean_id.encode()
        key = base64.urlsafe_b64encode(kdf.derive(password))  # derive and encode the key
        f = Fernet(key)  # use the key for Fernet encryption and decryption
        for col in columns:  # iterate over columns
            if col != "salt":
                value = str(row[col]).encode()  # convert value to byte string
                row[col] = f.encrypt(value)  # replace original value with encrypted one
    else:
        for col in columns:
            row[col] = np.nan
    return row


def _decrypt_patient(
        row: pd.Series,
        encryption_id_dict: Dict,
        columns: Collection
) -> pd.Series:

    """
    Decrypt a row representing one patient using their xclean_id.

    :param row: encrypted patient's row from the updates
    :type row: pd.Series
    :param encryption_id_dict: dictionary with each patient's encrypted
    pseudonym as the key and the xclean_id the value
    :type encryption_id_dict: Dict
    :param columns: columns to encrypt
    :type columns: Collection
    :return: decrypted patient's row from the updates
    :rtype: pd.Series
    """

    kdf = PBKDF2HMAC(  # create an instance of the KDF
        algorithm=hashes.SHA256(),
        length=32,
        salt=row.salt.encode(),
        iterations=100000,
        backend=default_backend()
    )

    if row.Pseudonym in encryption_id_dict.keys():
        xclean_id = encryption_id_dict[row.Pseudonym]
        password = xclean_id.encode()
        key = base64.urlsafe_b64encode(kdf.derive(password))
        f = Fernet(key)
        for col in columns:  # iterate over columns
            if col != "salt":
                decrypted = f.decrypt(row[col])  # encrypt value
                row[col] = decrypted.decode()  # replace original value with encrypted one
    else:
        for col in columns:
            row[col] = np.nan
    return row


def _save_updates(
        update_df: pd.DataFrame,
        update_type: str
) -> NoReturn:

    """
    Saves the updated data as a csv file with the date in the filename

    :param update_df: updated data to be saved
    :type update_df: pd.DataFrame
    :return: None
    :rtype: NoReturn
    """

    # Remove xclean_id column
    if "xclean_id" in update_df.columns:
        update_df = update_df.drop("xclean_id", axis=1)

    # Prepare filename
    today = date.today().strftime("%d%m%Y")
    filename = update_type + "_updates_" + today + ".csv"
    # Get directory and ensure that it exists
    p = Path(__file__).resolve().parents[2] / "data_updates"
    p.mkdir(exist_ok=True)

    # Save
    update_df.to_csv(p / filename, index=False)

    return


def merge_checks_with_cleaned_df(
        patients_df: pd.DataFrame,
        numeric_errs_path: str,
        date_errs_path: str,
) -> pd.DataFrame:

    """
    Merges the error checked/amended data into the cleaned df.

    :param patients_df_path: cleaned nccid clinical data
    :type patients_df_path: pd.DataFrame
    :param numeric_errs_path: path to updated csv of numerical warnings/errors
    :type numeric_errs_path: str
    :param date_errs_path: path to updated csv of date warnings/errors
    :type date_errs_path: str
    :return: updated nccid clinical data
    :rtype: pd.DataFrame
    """

    numeric_errs = pd.read_csv(numeric_errs_path, index_col="Pseudonym", dtype=read_in_types_dict,
                               parse_dates=date_cols, dayfirst=True)
    date_errs = pd.read_csv(date_errs_path, index_col="Pseudonym", dtype=read_in_types_dict, parse_dates=date_cols,
                            dayfirst=True)
    patients_df = patients_df.set_index('Pseudonym')

    # Fill na with null -> pd.update will then use these values when evaluating changes
    patients_df = patients_df.fillna('NULL')
    numeric_errs = numeric_errs.fillna('NULL')
    date_errs = date_errs.fillna('NULL')

    # Create original to allow updates to be saved
    patients_df_old = patients_df.copy()

    # Update dataframe with new manual changes
    patients_df.update(numeric_errs)
    patients_df = patients_df.fillna("NULL")
    patients_df.update(date_errs)
    patients_df = patients_df.replace("NULL", np.nan).reset_index(drop=False)
    patients_df[date_cols] = patients_df[date_cols].apply(pd.to_datetime, errors='coerce')

    # Save patients previously checked...
    with open(Path(__file__).resolve().parents[2] / "data_updates" / 'check_history.txt', 'a') as f:
        f.writelines('\n'.join(np.unique(np.concatenate([numeric_errs.index.values, date_errs.index.values]))))

    # Save the changes for future updates...
    cols = cleaned_base + ['spo2_imputed']
    cols.remove('Pseudonym')
    # Identify differences between updated data and the original data
    numeric_patients = patients_df_old[patients_df_old.index.isin(numeric_errs.index)]
    numeric_patients = numeric_patients.sort_values('Pseudonym')
    numeric_errs = numeric_errs.sort_values('Pseudonym')
    numeric_comp = numeric_patients[cols].compare(
        numeric_errs[numeric_patients[cols].columns.tolist()],
        align_axis=0
    )
    # Convert any differences in the date cols to timedeltas
    num_dt_cols = [lambda x: x for x in date_cols if x in numeric_comp.columns.tolist()]
    if len(num_dt_cols) > 0:
        numeric_comp.loc[:, num_dt_cols] = _date_differences_as_timedelta(patients_df_old, numeric_errs, date_cols)
    # Keep only the updated data
    numeric_updates = numeric_comp.xs('other', level=1, drop_level=True)

    # Identify differences but convert date updates into days before or after original date for anonymisation
    timedelta_updates = _date_differences_as_timedelta(patients_df_old, date_errs, date_cols)
    # Select the timedelta data
    timedelta_updates = timedelta_updates.xs('other', level=1, drop_level=True)

    # combine the two sets of changes into one df...
    # first update the numeric df in case any patients in the dates df too
    numeric_updates.update(timedelta_updates)
    # concat patients not included in numeric sheet
    to_add = timedelta_updates[~timedelta_updates.index.isin(numeric_updates.index.tolist())]
    updates = pd.concat([numeric_updates, to_add])

    # encrypt the data so that only the original data will unencrypt the values
    updates = updates.reset_index(drop=False)
    if patients_df_old.index.name != "Pseudonym":
        xclean_id_dict = patients_df_old.set_index('Pseudonym')['xclean_id'].to_dict()
    else:
        xclean_id_dict = patients_df_old['xclean_id'].to_dict()
    updates.loc[:, 'salt'] = os.urandom(16)
    cols_to_encrypt = updates.columns.tolist()
    encrypted_updates = updates.apply(_encrypt_patient, args=(xclean_id_dict, cols_to_encrypt), axis=1)
    _save_updates(encrypted_updates, 'manual')

    return patients_df


def _load_encrypted_previous_changes(
        xclean_id_dict: Dict
) -> Tuple[Optional[pd.DataFrame], Optional[pd.DataFrame]]:

    """
    Load any previous changes from either manual data checks or dicom enrichment.

    :param id_dict: dictionary with the patient's pseudonym as the key and the xclean_id the value
    :type id_dict: Dict
    :return: unencrypted previous manual changes and dicom enrichment values
    :rtype: Tuple[pd.DataFrame, pd.DataFrame]
    """

    def _read_in_updates(p: Path, update_type: str) -> pd.DataFrame:
        updates_list = [pd.read_csv(p / f, dtype=bytes) for f in p.glob(update_type + "_updates_*.csv")]
        updates_df = pd.concat(updates_list)
        return updates_df

    def _read_in_latest_updates(p: Path, update_type: str) -> pd.DataFrame:
        update_files = [x.name for x in Path('.').glob(update_type + "_updates_*.csv")]
        dates = pd.Series([pd.to_datetime(re.search(r"\d+", x).group(), format="%d%m%Y") for x in update_files])
        updates_df = pd.read_csv(p / f"{update_type}_updates_{dates.max().strftime('%d%m%Y')}.csv", dtype=bytes)
        return updates_df

    update_path = Path(__file__).resolve().parents[2] / "data_updates"
    if len([x for x in update_path.glob("manual_updates_*.csv")]) > 0:
        # find and read in all manual updates
        man_updates = _read_in_updates(update_path, 'manual')

        # decrypt
        salt_used = man_updates.iloc[0].salt.encode()
        decrypt_id_dict = {}
        for key, value in xclean_id_dict.items():
            kdf = PBKDF2HMAC(  # create an instance of the KDF
                algorithm=hashes.SHA256(),
                length=32,
                salt=salt_used,
                iterations=100000,
                backend=default_backend()
            )
            key = base64.urlsafe_b64encode(kdf.derive(value.encode()))
            f = Fernet(key)
            decrypt_id_dict[f.encrypt(key)] = value
        print('got encrypted id dict')
        print(decrypt_id_dict)
        cols_to_decrypt = man_updates.columns.tolist()
        man_updates = man_updates.apply(_decrypt_patient, args=(decrypt_id_dict, cols_to_decrypt), axis=1)

        # convert data from strings
        man_updates = man_updates.replace({"<NA>": np.nan}).replace({"NaT": pd.NaT}).replace({"NaN": np.nan})
        man_dtypes = {"salt": bytes}
        for col in man_updates.columns.tolist():
            if "date" not in col and col != 'salt':
                man_dtypes[col] = read_in_types_dict[col]
        man_updates = man_updates.astype(man_dtypes)
        date_update_cols = [x for x in date_cols if x in man_updates.columns.tolist()]
        man_updates.loc[:, date_update_cols] = man_updates.loc[:, date_update_cols].apply(pd.to_timedelta)
    else:
        man_updates = None

    if len([x for x in update_path.glob("dcm_updates_*.csv")]) > 0:
        # find all dicom updates
        dcm_updates = _read_in_latest_updates(update_path, 'dcm')
        # decrypt
        salt_used = dcm_updates.iloc[0].salt.encode()
        decrypt_id_dict = {}
        for key, value in xclean_id_dict.items():
            kdf = PBKDF2HMAC(  # create an instance of the KDF
                algorithm=hashes.SHA256(),
                length=32,
                salt=salt_used,
                iterations=100000,
                backend=default_backend()
            )
            key = base64.urlsafe_b64encode(kdf.derive(value.encode()))
            f = Fernet(key)
            decrypt_id_dict[f.encrypt(key)] = value

        cols_to_decrypt = dcm_updates.columns.tolist()
        dcm_updates = dcm_updates.apply(_decrypt_patient, args=(decrypt_id_dict, cols_to_decrypt), axis=1)
        dcm_updates = dcm_updates.replace({"<NA>": np.nan}).replace({"NaT": pd.NaT}).replace({"NaN": np.nan})
        # Only Pseudonym, Age, Sex - already string
    else:
        dcm_updates = None

    return man_updates, dcm_updates


def _date_change_from_timedelta(
        original_df: pd.DataFrame,
        new_date_df: pd.DataFrame,
) -> pd.DataFrame:

    """
    Updates cleaned dates to those from manual changes using stored timedeltas.

    :param original_df: original data for the date columns with the index set to Pseudonym
    :type original_df: pd.DataFrame
    :param new_df: updated date data in the form of timedeltas with the index set to Pseudonym
    :type new_df: pd.DataFrame
    :return: updated clinical dataframe
    :rtype: pd.DataFrame
    """

    # ensure columns match for compare function
    o = original_df[new_date_df.columns.tolist()].copy()
    # ensure indices match for compare function
    o = o.loc[o.index.isin(new_date_df.index.tolist())]
    # do the comparison
    df = o.sort_values(by='Pseudonym').compare(new_date_df.sort_values(by='Pseudonym'), align_axis=0, keep_equal=True)
    # return the sum of the original date and the timedelta to get the changed date
    return df.groupby(level='Pseudonym').sum(min_count=2)


def update_with_prev_changes(
        patients_df: pd.DataFrame,
) -> pd.DataFrame:

    """
    Update the cleaned clinical data with previous saved manual changes and those
    made from previous DICOM enrichment (stored in encrypted form in the data_updates folder of the package).

    :param patients_df: cleaned dataframe
    :type patients_df: pd.DataFrame
    :return: cleaned dataframe with data updated from stored changes
    :rtype: pd.DataFrame
    """

    # Create dictionary of xclean ids to allow for decryption
    xclean_id_dict = patients_df.set_index('Pseudonym')['xclean_id'].to_dict()

    # Load all of the encrypted changes
    man_updt, dcm_updt = _load_encrypted_previous_changes(xclean_id_dict)

    # Set index of dataframe to patient pseudonym to allow for use of pd.update
    patients_df.set_index('Pseudonym')

    if man_updt is not None:
        man_updt = man_updt.set_index('Pseudonym')
        # Update the dataframe with the non-date, loaded manual changes

        patients_df.update(man_updt.loc[:, ~man_updt.columns.isin(date_cols)])
        # Make changes to date columns using the timedeltas
        date_updt = _date_change_from_timedelta(patients_df, man_updt[date_cols])
        patients_df.update(date_updt)
        patients_df = patients_df[date_cols].apply(pd.to_datetime, errors='coerce')


    if dcm_updt is not None:
        dcm_updt = dcm_updt.set_index('Pseudonym')
        # Update the dataframe with the loaded changes from dicom enrichment
        patients_df.update(dcm_updt)

    return patients_df.reset_index()
