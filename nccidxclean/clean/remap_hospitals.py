"""
Remaps hospital names from abbreviations.
"""

import pandas as pd
import logging
from nccidxclean.clean.dicts_and_maps import _NO_HOSP_MAP, _HOSPITAL_MAPPING, dev_submitting_centres

logger = logging.getLogger(__name__)
console_msg = logging.getLogger('consoleLog')


def remap_hospitals(patients_df: pd.DataFrame) -> pd.DataFrame:
    """
    Remap hospital names from abbreviations.

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """

    def _remap_hosp(x: pd.Series) -> pd.Series:

        if x in _HOSPITAL_MAPPING.keys():
            _x = _HOSPITAL_MAPPING[x]
        else:
            _x = x

        return _x

    patients_df['hospital'] = patients_df['Hospital'].str.strip()
    patients_df['hospital'] = patients_df['hospital'].str.replace("&", "and")

    patients_df.loc[
        patients_df['hospital'].astype(str).str.startswith('MPH'), 'hospital'] = 'Musgrove Park Hospital'

    patients_df['hospital'] = patients_df['hospital'].apply(_remap_hosp)

    patients_df.loc[
        (patients_df['hospital'] == 'RSCH') & (
                    patients_df['SubmittingCentre'] == 'Brighton and Sussex University Hospitals NHS Trust'), 'hospital'
    ] = "Royal Sussex County Hospital"
    patients_df.loc[
        (patients_df['hospital'] == 'RSCH') & (
                    patients_df['SubmittingCentre'] == 'Royal Surrey NHS Foundation Trust'), 'hospital'
    ] = "Royal Surrey County Hospital"

    # If hospital blank, can be filled for some trusts as only one acute hospital, but if not possible to infer,
    # unknown used for clarity
    patients_df.loc[patients_df['hospital'].isna(), 'hospital'] = patients_df['SubmittingCentre'].map(_NO_HOSP_MAP)

    return patients_df


def check_new_centres(patients_df: pd.DataFrame) -> pd.DataFrame:

    """
    Provides warning if a new center is included which was not used in the development data.
    Data from these centers should be checked

    :param patients_df: dataframe of clinical data
    :type patients_df: pd.DataFrame
    :return: dataframe of clinical data
    :rtype: pd.DataFrame
    """
    
    console_msg.info("Checking for new centres not in development data...")

    for centre in patients_df['SubmittingCentre'].unique().tolist():
        if centre not in dev_submitting_centres:
            console_msg.warning(f"Submitting Centre not in the development data: {centre}")
            logger.warning(f"CENTRE: Submitting Centre not in the development data: {centre}")

    # console_msg.info("Done checking for new centres!")

    return patients_df
