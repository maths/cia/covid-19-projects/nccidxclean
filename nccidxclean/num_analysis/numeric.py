"""

Generates analysis for the numerical fields of the nccid data cleaned using the nccidxclean pipeline.
Outputs are printed to stdout.

"""

import pandas as pd
from typing import NoReturn, Union
from nccid_cleaning.cleaning import _coerce_numeric_columns

from nccidxclean.clean.numeric import _truncate_numerical_field, _clean_creatinine, _infer_ddimer_units, _clean_d_dimer, rescale_fio2
from nccidxclean.clean.dicts_and_maps import ddimer_units, dd_conversion_factors, all_o2_cols


def round_sig(x: Union[str, int, float], sig_fig: int = 2) -> str:
    """
    Sets a number to a specified number of significant figures.

    :param x: input number
    :type x: str, int or float
    :param sig_fig: number of significant figures to use, default=2
    :type sig_fig: int
    :return: the number rounded using sig_fig significant figures
    :rtype: str
    """
    return '{:g}'.format(float('{:.{p}g}'.format(x, p=sig_fig)))


def value_counts_with_percentage(x: pd.Series) -> pd.DataFrame:
    """
    Generates a dataframe of value counts as a percentage for a given field.

    :param x: Input data field
    :type x: pd.Series
    :return: dataframe of value counts as a percentage
    :rtype: pd.DataFrame
    """
    counts = x.value_counts(dropna=False)
    percent100 = x.value_counts(normalize=True, dropna=False).mul(100).round(1).astype(str) + '%'
    return pd.DataFrame({'Count': counts, 'Percentage': percent100})


def creatinine_analysis(df: pd.DataFrame) -> NoReturn:
    """
    Generates creatinine analysis and prints to stdout.

    :param df: dataframe of numeric variables cleaned with _coerce_numeric_columns from the nhsx nccid-cleaning tool
    :type df: pd.DataFrame
    :return: None
    """
    print("CREATININE...")
    conv_count = 0
    clip_min = 30
    clip_max = 6000
    df['creatinine_on_admission'] = df['creatinine_on_admission'].apply(_clean_creatinine)
    print(f"No. of Creatinine results who's units were converted = {conv_count} ("
          f"{str(round_sig(100 * conv_count / len(df[df['creatinine_on_admission'].notna()])))}%)")
    clipped_no = len(df[(df['creatinine_on_admission'] < clip_min) | (
                df['creatinine_on_admission'] > clip_max) | isinstance(df['creatinine_on_admission'], int)])
    print(f"No. of Creatinine Results Clipped = {clipped_no} ("
          f"{str(round_sig(100 * clipped_no / len(df[df['creatinine_on_admission'].notna()])))}%)\n")


def ddimer_analysis(df: pd.DataFrame) -> NoReturn:
    """
    Generates d-dimer analysis and prints to stdout.

    :param df: dataframe of numeric variables cleaned with _coerce_numeric_columns from the nhsx nccid-cleaning tool
    :type df: pd.DataFrame
    :return: None
    """

    print("D-DIMER...")
    trunc_below = 0
    trunc_above = 0
    trunc_min = 150
    trunc_max = 10000
    dd_units = _infer_ddimer_units(df, ddimer_units)
    calc_df = df.apply(_clean_d_dimer, axis=1, args=(dd_units, dd_conversion_factors,))
    print(f"No. with D-dimer of exactly 10,000 by hospital (machine limit at these centers?): "
          f"{value_counts_with_percentage(calc_df[calc_df['d-dimer_on_admission'] == trunc_max].hospital)}\n")
    calc_df['d-dimer_on_admission'] = calc_df[
        'd-dimer_on_admission'].apply(_truncate_numerical_field, args=(trunc_min, trunc_max))
    print(f"No. of D-Dimer Results Truncated Above Max {trunc_max} = {trunc_above} "
          f"({str(round_sig(100 * trunc_above / len(calc_df[calc_df['d-dimer_on_admission'].notna()])))}%)")
    print(f"No. of D-Dimer Results Truncated Below Min {trunc_min} = {trunc_below} "
          f"({str(round_sig(100 * trunc_below / len(calc_df[calc_df['d-dimer_on_admission'].notna()])))}%)\n")


def troponin_analysis(df: pd.DataFrame) -> NoReturn:
    """
    Generates troponin i analysis and prints to stdout.

    :param df: dataframe of numeric variables cleaned with _coerce_numeric_columns from the nhsx nccid-cleaning tool
    :type df: pd.DataFrame
    :return: None
    """

    print("TROPONIN I...")
    trunc_below = 0
    trunc_min = 10
    trops = df[df['Troponin I'].notna()][['Troponin I', 'hospital']]
    trops['Troponin I'] = trops['Troponin I'].astype(str)
    print("Troponin I is entered as '<10' =", len(trops[trops['Troponin I'].str.contains('< 10')]), '(' + str(round_sig(
        100 * len(trops[trops['Troponin I'].str.contains('< 10')]) / len(trops[trops['Troponin I'].notna()]))) + '%)')
    print("Troponin I is entered as '<5' =", len(trops[trops['Troponin I'].str.contains('<5')]), '(' + str(round_sig(
        100 * len(trops[trops['Troponin I'].str.contains('<5')]) / len(trops[trops['Troponin I'].notna()]))) + '%)')
    print("Troponin I is entered as '<3' =", len(trops[trops['Troponin I'].str.contains('<3')]), '(' + str(round_sig(
        100 * len(trops[trops['Troponin I'].str.contains('<3')]) / len(trops[trops['Troponin I'].notna()]))) + '%)\n')
    print("Center/s with '<10' values:")
    print(f"{value_counts_with_percentage(trops[trops['Troponin I'].str.contains('< 10')].hospital)}\n")
    print("Center/s with '<' anywhere in the Troponin I value:")
    print(f"{value_counts_with_percentage(trops[trops['Troponin I'].str.contains('<')].hospital)}\n")
    df['troponin_i'] = df['troponin_i'].apply(_truncate_numerical_field, args=(10, None))
    print(f"No. of Troponin I Results Truncated Below Min of '{str(trunc_min)}' = {str(trunc_below)} ("
          f"{str(round_sig(100 * trunc_below / len(df[df['troponin_i'].notna()])))}%)\n")


def fio2_analysis(df: pd.DataFrame) -> NoReturn:
    """
    Generates fio2 analysis and prints to stdout.

    :param df: dataframe of nccid data cleaned using the nccidxclean pipeline
    :type df: pd.DataFrame
    :return: None
    """

    print("FiO2...")
    print(f"No. of FiO2's equal to 0.5 = {len(df[(df['FiO2'] == '0.5') | (df['FiO2'] == '0.5L')])} ("
          f"{str(round_sig(100 * len(df[(df['FiO2'] == '0.5') | (df['FiO2'] == '0.5L')]) / len(df[df['FiO2'].notna()])))}%)\n")
    fio2_df = df[df["FiO2"].str.replace(".", "", 1).str.isdigit() & df["FiO2"].notna()].copy()
    fio2_df.FiO2 = fio2_df.FiO2.str.replace("L", "").astype(float)
    print(f"No. of FiO2's between 0.5 and 1 = "
          f"{len(fio2_df[(fio2_df.FiO2 < 1) & (fio2_df.FiO2 > 0.5) & fio2_df.FiO2.notna()])} ("
          f"{str(round_sig(100 * len(fio2_df[(fio2_df.FiO2 < 1) & (fio2_df.FiO2 > 0.5) & fio2_df.FiO2.notna()]) / len(df[df['FiO2'].notna()])))}%)\n")


def pao2_analysis(df):
    """
    Generates pao2/spo2 analysis and prints to stdout.

    :param df: dataframe of nccid data cleaned using the nccidxclean pipeline
    :type df: pd.DataFrame
    :return: None
    """

    print("PaO2/SpO2...")

    if all(x in df.columns for x in all_o2_cols):

        # pao2 centres
        print('Total No. of PaO2s supplied in "PaO2" field:',
              len(df[df.pao2_gas.notna() & df.spo2_saturation.isna()]),
              '(' + str(round_sig(100 * len(df[df.pao2_gas.notna() & df.spo2_saturation.isna()]) / len(df[df.pao2_gas.notna() | df.spo2_saturation.isna()]))) + '%)',
              )
        print(value_counts_with_percentage(df[df.pao2_gas.notna()].hospital), "\n")

        # spo2 centres
        print('Total No. of SpO2s supplied in "PaO2" field:',
              len(df[df.pao2_gas.isna() & df.spo2_saturation.notna()]),
              '(' + str(round_sig(100 * len(df[df.pao2_gas.isna() & df.spo2_saturation.notna()]) / len(
                  df[df.pao2_gas.notna() | df.spo2_saturation.isna()]))) + '%)',
              )
        print(value_counts_with_percentage(df[df.spo2_saturation.notna()].hospital), "\n")

        # In both
        print('Provided a mixture of PaO2s and SpO2s in the PaO2 field:\n')
        for center in df[df.pao2_gas.notna()].hospital.unique():
            if center in df[df.spo2_saturation.notna()].hospital.unique():
                print(center)
        print('*** Note manual checking shows the two in Ealing and Oxford to be errors... ***\n')

        print("No. of SpO2's inferred from PaO2's =",
              len(df[df.pao2_gas.notna() & df.spo2_imputed.notna()]),
              '(' + str(round_sig(100 * len(df[df.pao2_gas.notna() & df.spo2_imputed.notna()]) / len(
                  df[df.spo2_imputed.notna()]))) + '%)')
        print("No. of PaO2's inferred from SpO2's =",
              len(df[df.spo2_saturation.notna() & df.pao2_imputed.notna()]),
              '(' + str(round_sig(100 * len(df[df.spo2_saturation.notna() & df.pao2_imputed.notna()]) / len(
                  df[df.pao2_imputed.notna()]))) + '%)\n')

        print("No. of SpO2's truncated =",
              len(df[df.pao2_gas.notna() & (df.spo2_imputed.astype(float) == 100)]),
              '(' + str(round_sig(100 * len(df[df.pao2_gas.notna() & (df.spo2_imputed.astype(float) == 100)]) / len(
                  df[df.spo2_imputed.notna()]))) + '%)\n')

    else:

        print(("*" * 100) + "\nUnable to analyse O2 as not all O2 columns in dataframe - try running "
                            "the nccidxclean pipeline with the 'return_cols' parameter = 'all_with_original' or "
                            "'all_cleaned_only'.\n" + ("*" * 100) + "\n")


def numeric_fields_analysis(x_df: pd.DataFrame) -> NoReturn:
    """
    Generates analysis for the numerical fields to understand how the nccidxclean pipeline
     affected the data.

    :param x_df: dataframe cleaned by the nccidxclean pipeline.
    :type x_df: pd.DataFrame
    :return: None
    """
    calc_df = x_df[[
        'Pseudonym',
        'SubmittingCentre',
        'hospital',
        'Creatinine on admission',
        'D-dimer on admission',
        'Troponin I',
        'Troponin T',
        'FiO2',
        'CRP on admission'
    ]].copy()

    # Produce dataframe used for calculating how many values affected by truncation, clipping, etc.
    calc_df = _coerce_numeric_columns(calc_df)

    # Perform analysis for each feature
    creatinine_analysis(calc_df)
    ddimer_analysis(calc_df)
    troponin_analysis(calc_df)
    fio2_analysis(x_df)
    pao2_analysis(x_df)
