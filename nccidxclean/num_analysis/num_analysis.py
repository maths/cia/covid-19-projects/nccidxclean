from typing import NoReturn
import pandas as pd

from nccidxclean.figures.overall_numbers import get_total_missing
from nccidxclean.num_analysis.tables import summary_table, info_by_center
from nccidxclean.num_analysis.numeric import numeric_fields_analysis
from nccidxclean.num_analysis.categorical import categorical_fields_analysis
from nccidxclean.num_analysis.dates import date_field_analysis


def count_missing(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame
) -> NoReturn:

    tot_missing, tot_dates_missing = get_total_missing(n_pos, x_pos)

    print("No. of missing values in each dataset:")
    print(tot_missing)
    print("\nNo. of missing dates in each dataset:")
    print(tot_missing)



def numeric_analysis(
        xtd_df: pd.DataFrame,
        xtd_pos: pd.DataFrame,
        nhs_pos: pd.DataFrame,
        xtd_dates,
        nhs_dates,
) -> NoReturn:
    """
    Performs numeric analysis of the data cleaned by the nccidxclean package.

    :param xtd_df: dataframe of nccid data cleaned using the nccidxclean pipeline
    :type xtd_df: pd.DataFrame
    :param xtd_pos: dataframe of nccid data for patients with a positive pcr cleaned using the nccidxclean pipeline
    :type xtd_pos: pd.DataFrame
    :param nhs_pos: dataframe of nccid data for patients with a positive pcr cleaned using the nccid-cleaning pipeline
    :type nhs_pos: pd.DataFrame
    :return: None
    """

    print('-' * 50 + '\nSUMMARY TABLES\n' + '-' * 50)
    summary_table(xtd_df, xtd_pos)
    info_by_center(xtd_df, xtd_pos)

    print('-' * 50 + '\nANALYSIS OF NUMERIC FIELDS\n' + '-' * 50)
    numeric_fields_analysis(xtd_df)

    print('-' * 50 + '\nANALYSIS OF CATEGORICAL FIELDS\n' + '-' * 50)
    categorical_fields_analysis(xtd_df, xtd_pos, nhs_pos)

    print('-' * 50 + '\nANALYSIS OF DATE FIELDS\n' + '-' * 50)
    date_field_analysis(nhs_pos, xtd_pos, nhs_dates, xtd_dates)

    print('-' * 50 + '\nMISSING VALUES\n' + '-' * 50)
    count_missing(xtd_pos, nhs_pos)
