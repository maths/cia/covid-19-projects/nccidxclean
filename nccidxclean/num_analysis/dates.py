import numpy as np
import pandas as pd
from typing import NoReturn
from nccid_cleaning.cleaning import _US_DATE_COLS
import re

from nccidxclean.num_analysis.numeric import round_sig
from nccidxclean.figures.dates import date_dict


def count_dates(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame
) -> pd.DataFrame:
    raw_pos = n_pos.replace("", np.nan).replace(" ", np.nan).replace("0", np.nan).copy()

    data_dataset = {'Raw Data': {}, 'NCCID Pipeline': {}, 'Add-on Pipeline': {}}
    for key, value in date_dict.items():
        data_dataset['Raw Data'][value] = len(raw_pos[raw_pos[value].notna()])
        data_dataset['NCCID Pipeline'][value] = len(n_pos[n_pos[key].notna()])
        data_dataset['Add-on Pipeline'][value] = len(x_pos[x_pos[key].notna()])

    date_entry_count = pd.DataFrame(data_dataset).sum().reset_index()
    date_entry_count = date_entry_count.rename({'index': 'Dataset', 0: 'Total No. of Date Entries'}, axis=1)

    try:
        date_entry_count = date_entry_count.set_index('Dataset')
    except:
        pass

    return date_entry_count


def dates_summary(date_entry_count: pd.DataFrame) -> NoReturn:

    print('Date entries after each pipeline:\n')
    print(f"{date_entry_count}\n")

    dates_lost = date_entry_count.loc['Raw Data']['Total No. of Date Entries'] - date_entry_count.loc['NCCID Pipeline'][
        'Total No. of Date Entries']

    print(f"Date values lost in NCCID pipeline = {dates_lost} ("
          f"{str(round_sig(100 * dates_lost / date_entry_count.loc['Raw Data']['Total No. of Date Entries']))}%)\n")

    dates_gained = date_entry_count.loc['Add-on Pipeline']['Total No. of Date Entries'] - \
                   date_entry_count.loc['NCCID Pipeline']['Total No. of Date Entries']

    print(f"Data values gained in extended nccidxclean pipeline versus NHSx nccid-cleaning = {dates_gained} ("
          f"{str(round_sig(100 * dates_gained / date_entry_count.loc['Add-on Pipeline']['Total No. of Date Entries']))}"
          f"%)\n")



def excel_dates(n_pos: pd.DataFrame, date_entry_count: pd.DataFrame) -> NoReturn:
    data_hosp = {'hospital': n_pos['hospital']}
    for field in date_dict.values():
        data_hosp[field] = n_pos[field].str.isdigit()

    date_entry_by_hosp = pd.DataFrame(data_hosp).groupby("hospital")
    date_entry_by_hosp = date_entry_by_hosp[list(date_dict.values())].apply(lambda x: x.sum()).reset_index()
    date_entry_by_hosp = date_entry_by_hosp.melt(id_vars=['hospital'])
    date_entry_by_hosp = date_entry_by_hosp.rename({'variable': 'Date Field'}, axis=1)[date_entry_by_hosp.value != 0]
    date_entry_by_hosp = date_entry_by_hosp.sort_values('hospital')

    print(f"No. of Excel format / decimal dates (all lost in NCCID pipeline) = {date_entry_by_hosp.value.sum()} ("
          f"{str(round_sig(100*date_entry_by_hosp.value.sum()/date_entry_count.loc['Raw Data']['Total No. of Date Entries']))}"
          f"%)\n")


def incorrect_date_format(n_pos: pd.DataFrame) -> NoReturn:

    def unflipped_dates(x: pd.Series) -> pd.Series:

        nonlocal same_day_month_count

        for field in affected_cols:
            if pd.isna(x[field]) or x[field] == "":
                x['flipped_' + field.lower().replace(' ', '_')] = 0
            else:
                match_standard = re.search(r'\d{1,2}/\d{1,2}/\d{2,4}', x[field], )
                try:
                    if match_standard:
                        # Coerce into US date format
                        temp = pd.to_datetime(match_standard.group(), format='%m/%d/%Y', errors="raise")
                        if temp.month == temp.day:
                            x['unflipped_' + field.lower().replace(' ', '_')] = 0
                            same_day_month_count += 1
                        else:
                            x['unflipped_' + field.lower().replace(' ', '_')] = 1
                except:
                    try:
                        _ = pd.to_datetime(match_standard.group(), format='%d/%m/%Y', errors="raise")
                        x['unflipped_' + field.lower().replace(' ', '_')] = 0
                    except:
                        x['unflipped_' + field.lower().replace(' ', '_')] = 0

        return x

    def flipped_dates(x: pd.Series) -> pd.Series:
        for field in _US_DATE_COLS:
            if pd.isna(x[field]) or x[field] == "":
                x['flipped_' + field.lower().replace(' ', '_')] = 0
            else:
                match_standard = re.search(r"\d{1,2}/\d{1,2}/\d{2,4}", x[field], )
                try:
                    if match_standard:
                        # Coerce into US date format
                        _ = pd.to_datetime(match_standard.group(), format='%m/%d/%Y', errors="raise")
                        x['flipped_' + field.lower().replace(' ', '_')] = 0
                except:
                    try:
                        _ = pd.to_datetime(match_standard.group(), format='%d/%m/%Y', errors="raise")
                        x['flipped_' + field.lower().replace(' ', '_')] = 1
                    except:
                        x['flipped_' + field.lower().replace(' ', '_')] = 0
        return x

    print('-' * 50, '\nDATES...\n')

    # Examine cases where PCR dates were sent in wrong format (expected in US format but provided in UK format)
    # and were corrected by pandas in original pipeline, e.g. 16/11/2023 is not a possible US format date, so
    # pandas automatically 'flipped' the day and month values

    switched_date_hosps = ['Leicester', 'St Georges', 'The Walton Centre']
    switched_date_hosps_df = n_pos[n_pos.hospital.isin(switched_date_hosps)].copy()
    switched_date_hosps_df = switched_date_hosps_df[
        ['date_of_acquisition_of_1st_rt-pcr', 'date_of_acquisition_of_2nd_rt-pcr',
         'date_of_positive_covid_swab']].melt()
    switched_date_hosps_df = switched_date_hosps_df.rename({'value': 'Date', 'variable': 'Date Field'}, axis=1)
    switched_date_hosps_df = switched_date_hosps_df[switched_date_hosps_df.Date.notna()]

    raw_pos = n_pos.replace("", np.nan).replace(" ", np.nan).replace("0", np.nan).copy()

    print(f"Total No. of dates expected in US format, which were in UK format = {len(switched_date_hosps_df)} ("
          f"{str(round_sig(100 * len(switched_date_hosps_df) / raw_pos[_US_DATE_COLS].count().sum()))}%)")
    print(f"(No. of date entries expected in US format in total = {str(raw_pos[_US_DATE_COLS].count().sum())} )\n")

    print(f'Note the below from the Pandas documentation: **dayfirst=True in Pandas is not strict**, but will *prefer* '
          f'to parse with day first:')
    print(f'If a delimited date string cannot be parsed in accordance with the given "dayfirst" option, e.g. '
          f'to_datetime(["31-12-2021"]), then a warning will be shown but the date will be processed.\n')

    dates_flipped = n_pos[['hospital'] + _US_DATE_COLS].copy()

    for field in _US_DATE_COLS:
        dates_flipped['flipped_' + field.lower().replace(' ', '_')] = 0

    dates_flipped_raw = dates_flipped.apply(flipped_dates, axis=1)
    dates_flipped = dates_flipped_raw[['hospital'] + ['flipped_' + x.lower().replace(' ', '_') for x in _US_DATE_COLS]]
    dates_flipped_counts = dates_flipped.groupby("hospital")[
        ['flipped_' + x.lower().replace(' ', '_') for x in _US_DATE_COLS]].apply(lambda x: x.sum())
    dates_flipped_counts = dates_flipped_counts[
        ['flipped_' + x.lower().replace(' ', '_') for x in _US_DATE_COLS]].reset_index().melt(id_vars='hospital')
    dates_flipped_counts = dates_flipped_counts[dates_flipped_counts.value.notna() & (dates_flipped_counts.value != 0)]

    # Examine cases where PCR dates were sent in wrong format (expected in US format but provided in UK format)
    # and were read in by the original pipeline but no error generated, so considered to be correct. e.g. 02/03/2021
    # read as 2nd March 2021 rather than 3rd February 2021.
    switched_date_hosps = ['Leicester', 'St Georges', 'The Walton Centre']

    affected_cols = [
        "Date of Positive Covid Swab",
        "Date of acquisition of 1st RT-PCR",
        "Date of acquisition of 2nd RT-PCR",
    ]

    dates_unflipped = n_pos[n_pos.hospital.isin(switched_date_hosps)][['hospital'] + affected_cols].copy()

    for field in affected_cols:
        dates_unflipped['unflipped_' + field.lower().replace(' ', '_')] = 0

    same_day_month_count = 0  # Initialise count variable
    dates_unflipped = dates_unflipped.apply(unflipped_dates, axis=1)
    dates_unflipped = dates_unflipped[
        ['hospital'] + ['unflipped_' + x.lower().replace(' ', '_') for x in affected_cols]]
    dates_unflipped = dates_unflipped.groupby("hospital")[
        ['unflipped_' + x.lower().replace(' ', '_') for x in affected_cols]].apply(lambda x: x.sum())
    dates_unflipped = dates_unflipped[
        ['unflipped_' + x.lower().replace(' ', '_') for x in affected_cols]].reset_index().melt(id_vars='hospital')
    dates_unflipped = dates_unflipped[dates_unflipped.value.notna() & (dates_unflipped.value != 0)]

    print(f"No. of dates expected in US format, which were in UK format, but were still read correctly =",
          f"{dates_flipped_counts.value.sum() + same_day_month_count} ("
          f"{str(round_sig(100*(dates_flipped_counts.value.sum() + same_day_month_count)/len(switched_date_hosps_df)))}"
          f"%)")
    print("i.e. date entered is impossible... however, 'dayfirst' format option is not strict in Pandas / NCCID code, "
          "so it switches the month and day values if it can't be read -> correct date obtained. This includes dates "
          "which have technically been read incorrectly, but the numeric day and month are the same.)\n")

    print("Affected hospitals are:")
    for hosp in dates_flipped_counts.hospital.unique():
        print("   -", hosp, '=', dates_flipped_counts[dates_flipped_counts.hospital == hosp].value.sum(),
              '(' + str(round_sig(100 * dates_flipped_counts[
                  dates_flipped_counts.hospital == hosp].value.sum() / dates_flipped_counts.value.sum())) + '%)\n')

    print("Affected columns:")
    for vari in dates_flipped_counts.variable.unique():
        print("   -", vari.replace('flipped_', ''), '=',
              dates_flipped_counts[dates_flipped_counts.variable == vari].value.sum(),
              '(' + str(round_sig(100 * dates_flipped_counts[
                  dates_flipped_counts.variable == vari].value.sum() / dates_flipped_counts.value.sum())) + '%)\n')

    print(f"No. of dates expected in US format, which were in UK format, but were read incorrectly ="
          f"{dates_unflipped.value.sum()} ("
          f"{str(round_sig(100 * dates_unflipped.value.sum() / len(switched_date_hosps_df)))}"
          f"%)")
    print(f"i.e. date entered is possible... "
          f"(error not spotted by NCCID pipeline -> Incorrect date obtained unless day = month -> "
          f"{str(same_day_month_count)} ({str(round_sig(100 * same_day_month_count / len(switched_date_hosps_df)))}%) "
          f"of such cases were removed from the above value)\n")

    print("Affected hospitals are:")
    for hosp in dates_unflipped.hospital.unique():
        print("   -", hosp, '=', dates_unflipped[dates_unflipped.hospital == hosp].value.sum())

    print("\nAffected columns:")
    for vari in dates_unflipped.variable.unique():
        print("   -", vari.replace('unflipped_', ''), '=',
              dates_unflipped[dates_unflipped.variable == vari].value.sum())


def pcr_imaging_gaps(n_dates: pd.DataFrame, x_dates: pd.DataFrame) -> NoReturn:
    hosp_means = []
    with_max_gap_dfs = []

    for i, df_dates in enumerate([n_dates, x_dates]):
        with_max_gap = df_dates[df_dates["Minimum Time between a PCR and Imaging (Days)"] < 352]
        #     with_max_gap = with_max_gap[with_max_gap.StudyDate.dt.year >= 2020]
        with_max_gap = with_max_gap.sort_values('hospital')
        with_max_gap_dfs.append(with_max_gap)
        hosp_means.append(with_max_gap[['hospital', 'Minimum Time between a PCR and Imaging (Days)']].groupby(
            'hospital').mean().reset_index())

    hosp_means[0] = hosp_means[0].rename({'Minimum Time between a PCR and Imaging (Days)': 'NCCID Pipeline'}, axis=1)
    hosp_means[1] = hosp_means[1].rename({'Minimum Time between a PCR and Imaging (Days)': 'Add-on Pipeline'}, axis=1)
    hosp_means_df = pd.merge(hosp_means[0], hosp_means[1], on='hospital', how='outer')

    print('Mean minimum time between imaging and a PCR test at each hospital:')
    print('NB. some hospitals included historical imaging - so this only includes imaging within 1 year of the test)')
    print("\n", hosp_means_df, "\n")

    print(f"Overall, the NCCID pipeline has a mean minimum time between a PCR test and imaging taking place (within 1 "
          f"year of that test) of: "
          f"{round(with_max_gap_dfs[0]['Minimum Time between a PCR and Imaging (Days)'].mean(), 2)} days")
    print(f"the add-on pipeline has a mean minimum time between a PCR test and imaging taking place (within 1 year of "
          f"that test) of "
          f"{round(with_max_gap_dfs[1]['Minimum Time between a PCR and Imaging (Days)'].mean(), 2)} days\n")

def date_field_analysis(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame,
        n_dates: pd.DataFrame,
        x_dates: pd.DataFrame,
) -> NoReturn:

    # Create dataframe with date counts...
    date_count_df = count_dates(n_pos, x_pos)

    # Summary of dates for each dataset and date entries lost overall
    dates_summary(date_count_df)

    # Examine cases where dates were lost in the original nccid-cleaning pipeline as the date was in
    # Microsoft Excel serial format.
    excel_dates(n_pos, date_count_df)

    # Incorrect day / month order submitted (US vs. UK format)
    incorrect_date_format(n_pos)

    # Calculate gaps between PCR and imaging
    pcr_imaging_gaps(n_dates, x_dates)
