import numpy as np
import pandas as pd
from typing import NoReturn

from nccidxclean.figures.prepare_dataframes import cleaned_cols


def summary_table(x_df: pd.DataFrame, x_pos: pd.DataFrame) -> NoReturn:
    """
    Creates summary table in markdown used on website.

    :param x_df: dataframe containing cleaned nccid data
    :type x_df: pd.DataFrame
    :param x_pos: dataframe containing cleaned nccid data for positive patients only
    :type x_pos: pd.DataFrame
    :return: None
    :rtype: NoReturn
    """

    x_neg = x_df[~x_df.Pseudonym.isin(x_pos.Pseudonym)]

    dataset_dict = {
        'COVID-Positive': [x_pos, 68, 66],
        'COVID-Negative': [x_neg, 7, 7],
        'Full Dataset': [x_df, 70, 67],
    }

    print("| | Patients | Submitting Centres | Hospitals | Raw Data Fields | Cleaned Data Fields | Earliest<br /> "
          "COVID Swab | Latest<br /> COVID Swab |")
    print("|--------|--------|--------|--------|--------|--------|--------|--------|")

    for dataset, [df, raw_no, cleaned_no] in dataset_dict.items():
        temp = df[['date_of_positive_covid_swab', 'negative_swab_date']].copy()
        temp['dates_calc'] = np.nan
        temp.loc[temp['date_of_positive_covid_swab'].notna(), 'dates_calc'] = temp['date_of_positive_covid_swab']
        temp.loc[temp['negative_swab_date'].notna(), 'dates_calc'] = temp['negative_swab_date']
        temp.dates_calc = pd.to_datetime(temp.dates_calc)

        if dataset == 'COVID-Negative':
            hosp_no = 'nan'
        else:
            hosp_no = df.hospital.nunique()

        print(
            "|",
            dataset, "|",
            f'{len(df):,}' + ' (' + str(round(100 * len(df) / len(x_df))) + '%)', "|",
            df.SubmittingCentre.nunique(), "|",
            hosp_no, "|",
            raw_no, "|",
            cleaned_no, "|",
            temp[temp['dates_calc'].notna()]['dates_calc'].min().strftime("%d/%m/%Y"), "|",
            temp[temp['dates_calc'].notna()]['dates_calc'].max().strftime("%d/%m/%Y"), "|",
        )

    print(("*" * 50) + "END OF TABLE\n" + ("*" * 50) + '\n\n')


def info_by_center(
        x_df: pd.DataFrame,
        x_pos: pd.DataFrame
) -> NoReturn:
    """
    Creates table of information for each center, including the number of hospitals, patients, missing values, and
    earliest / latest PCR dates. It is formatted in markdown for use on our website.

    :param x_df: dataframe containing cleaned nccid data
    :type x_df: pd.DataFrame
    :param x_pos: dataframe containing cleaned nccid data for positive patients only
    :type x_pos: pd.DataFrame
    :return: None
    :rtype: NoReturn
    """
    
    def round_sig(x, sig_fig=2):
        return '{:g}'.format(float('{:.{p}g}'.format(x, p=sig_fig)))

    print(
        "| | Total Patients | COVID-Positive /<br /> Negative Patients | Missing Values<br /> (COVID-Positives) | "
        "Earliest<br /> COVID Swab | Latest<br /> COVID Swab |",)
    print("|--------|--------|--------|--------|--------|--------|")

    temp = x_df.copy().rename({'spo2_imputed': 'pao2'}, axis=1)
    temp['dates_calc'] = np.nan
    temp.loc[temp['date_of_positive_covid_swab'].notna(), 'dates_calc'] = temp['date_of_positive_covid_swab']
    temp.loc[temp['negative_swab_date'].notna(), 'dates_calc'] = temp['negative_swab_date']
    temp.dates_calc = pd.to_datetime(temp.dates_calc)

    i = 0
    for center in temp.SubmittingCentre.unique():
        temp_center = temp[temp.SubmittingCentre == center]
        temp_center_pos = x_pos[x_pos.SubmittingCentre == center].rename({'spo2_imputed': 'pao2'}, axis=1)
        try:
            print(
                "|",
                center, "|",
                f'{len(temp_center):,}' + ' (' + str(round_sig(100 * len(temp_center) / len(x_df), 2)) + '%)', "|",
                f'{len(temp_center[temp_center.filename_covid_status == True]):,}' + ' (' + str(
                    round_sig(100 * len(temp_center[temp_center.filename_covid_status == True]) / len(temp_center),
                              2)) + '%)' + " /<br />" + f'{len(temp_center[temp_center.filename_covid_status == False]):,}' + ' (' + str(
                    round_sig(100 * len(temp_center[temp_center.filename_covid_status == False]) / len(temp_center),
                              2)) + '%)', "|",
                f'{temp_center_pos[cleaned_cols].isna().sum().sum():,}' + ' (' + str(round_sig(
                    100 * temp_center_pos[cleaned_cols].isna().sum().sum() / temp_center_pos[
                        cleaned_cols].size)) + '%)', "|",
                temp_center[temp_center.dates_calc.notna()].dates_calc.min().strftime("%d/%m/%Y"), "|",
                temp_center[temp_center.dates_calc.notna()].dates_calc.max().strftime("%d/%m/%Y"), "|",
            )
        except:
            print(
                "|",
                center, "|",
                f'{len(temp_center):,}' + ' (' + str(round_sig(100 * len(temp_center) / len(x_df), 2)) + '%)', "|",
                f'{len(temp_center[temp_center.filename_covid_status == True]):,}' + ' (' + str(
                    round_sig(100 * len(temp_center[temp_center.filename_covid_status == True]) / len(temp_center),
                              2)) + '%)' + " /<br />" + f'{len(temp_center[temp_center.filename_covid_status == False]):,}' + ' (' + str(
                    round_sig(100 * len(temp_center[temp_center.filename_covid_status == False]) / len(temp_center),
                              2)) + '%)', "|",
                f'{temp_center_pos[cleaned_cols].isna().sum().sum():,}' + ' (' + str(round_sig(
                    100 * temp_center_pos[cleaned_cols].isna().sum().sum() / temp_center_pos[
                        cleaned_cols].size)) + '%)', "|",
                'nan', "|",
                'nan', "|",
            )

    print(("*" * 50) + "END OF TABLE\n" + ("*" * 50) + '\n\n')

    print('')
