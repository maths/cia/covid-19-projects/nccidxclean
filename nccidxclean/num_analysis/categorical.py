"""

Generates analysis for the categorical fields of the nccid data cleaned using the nccidxclean pipeline.
Outputs are printed to stdout.

"""

from typing import NoReturn
import pandas as pd

from nccidxclean.num_analysis.numeric import value_counts_with_percentage, round_sig


def analyse_sex(
        x_df: pd.DataFrame,
        x_pos: pd.DataFrame,
        n_pos: pd.DataFrame,
) -> NoReturn:
    """
    Generates analysis of the sex feature and prints to stdout.

    :param x_df: dataframe of numeric variables cleaned with the nccidxclean tool
    :type x_df: pd.DataFrame
    :param x_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccidxclean tool
    :type x_pos: pd.DataFrame
    :param n_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccid-cleaning tool
    :type n_pos: pd.DataFrame
    :return: None
    """

    print('-' * 50, '\nSEX...\n')
    print(
        'NOTE: Sex not completed for COVID-negative patients, so all values for these negative cases are from DICOM '
        'enrichment.  This is where the largest boost is obtained from DICOM enrichment.\n'
    )

    print('-'*5, "\nSEX - FULL DATA...\n")
    print(f"Sex Values before merging with dicom: "
          f"{x_df['sex_b4dcm'].notnull().sum()} "
          f"({str(round_sig(100 * x_df['sex_b4dcm'].notnull().sum() / len(x_df)))}%)")
    print(f"Sex Values after merging with dicom: "
          f"{x_df['sex'].notnull().sum()} ({str(round_sig(100 * x_df['sex'].notnull().sum() / len(x_df)))}%)\n")
    print(f"Sex NaNs before merging with dicom: "
          f"{x_df['sex_b4dcm'].isnull().sum()} ({str(round_sig(100 * x_df['sex_b4dcm'].isnull().sum() / len(x_df)))}%)")
    print(f"Sex NaNs after merging with dicom: "
          f"{x_df['sex'].isnull().sum()} ({str(round_sig(100 * x_df['sex'].isnull().sum() / len(x_df)))}%)\n")
    print(f"Sex Unknowns before merging with dicom: "
          f"{(x_df['sex_b4dcm'] == 'Unknown').sum()} "
          f"({str(round_sig(100 * (x_df['sex_b4dcm'] == 'Unknown').sum() / len(x_df)))}%)")
    print(f"Sex Unknowns after merging with dicom: "
          f"{(x_df['sex'] == 'Unknown').sum()} "
          f"({str(round_sig(100 * (x_df['sex'] == 'Unknown').sum() / len(x_df)))}%)\n")

    print('-'*5, "\nCOVID POSITIVE PATIENTS ONLY (i.e. with full clinical data and sex entered...)\n")
    print('This field was not completed for COVID negative patients, '
          'so all values for negative patients are obtained from DICOM enrichment\n')

    print('Raw values entered by Sandwell and West Birmingham for Sex:')
    print(value_counts_with_percentage(x_pos[x_pos.SubmittingCentre.str.contains('Sandwell')].Sex), "\n")

    print('Values for sex after NCCID pipeline but prior to DICOM enrichment:')
    print(value_counts_with_percentage(n_pos.sex_b4dcm), "\n")

    print('Values for sex after NCCID pipeline and DICOM enrichment:')
    print(value_counts_with_percentage(n_pos.sex), "\n")

    print('Values for sex after new pipeline but prior to DICOM enrichment:')
    print(value_counts_with_percentage(x_pos.sex_b4dcm), "\n")

    print('Values for sex after new pipeline and DICOM enrichment:')
    print(value_counts_with_percentage(x_pos.sex), "\n")

    print(f"Sex Values in COV-positives before merging with dicom: "
          f"{(x_pos['sex_b4dcm'].notna()).sum()} "
          f"({str(round_sig(100 * x_pos['sex_b4dcm'].notna().sum() / len(x_pos)))}%)\n")
    print(f"Sex Values in COV-positives after merging with dicom: "
          f"{(x_pos['sex'].notna()).sum()} ({str(round_sig(100 * x_pos['sex'].notna().sum() / len(x_pos)))}%)\n")
    print(f"Sex NaNs in COV-positives before merging with dicom: "
          f"{x_pos['sex_b4dcm'].isnull().sum()} "
          f"({str(round_sig(100 * x_pos['sex_b4dcm'].isnull().sum() / len(x_pos)))}%)\n")
    print(f"Sex NaNs in COV-positives after merging with dicom: "
          f"{x_pos['sex'].isnull().sum()} ({str(round_sig(100 * x_pos['sex'].isnull().sum() / len(x_pos)))}%)\n")

    print('Sex values added for the COVID-positive patients from DICOM enrichment are for mainly three centres:')
    print(value_counts_with_percentage(
        x_pos[x_pos['sex_b4dcm'].isnull() & x_pos['sex'].notnull()].SubmittingCentre), "\n")
    print('None of St. Georges, Leicester or the Walton Centre provide any data other than PCR swab dates,')
    print('which is why these values were missing prior to DICOM enrichment.', "\n")


def analyse_age(
        x_df: pd.DataFrame,
        x_pos: pd.DataFrame
) -> NoReturn:
    """
    Generates analysis of age feature and prints to stdout.

    :param x_df: dataframe of numeric variables cleaned with the nccidxclean tool
    :type x_df: pd.DataFrame
    :param x_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccidxclean tool
    :type x_pos: pd.DataFrame
    :return: None
    """
    print('-' * 50, '\nAGE...\n')
    print('NOTE: Age not completed for COVID-negative patients, '
          'so all values for these negative cases are from DICOM enrichment.')
    print('This is where the largest boost is obtained from DICOM enrichment.\n')

    print('-'*5, "\nAGE - FULL DATA...\n")
    print(f"Age Values before merging with dicom: "
          f"{x_df['age_b4dcm'].notnull().sum()} "
          f"({str(round_sig(100 * x_df['age_b4dcm'].notnull().sum() / len(x_df)))}%)")
    print(f"Age Values after merging with dicom: "
          f"{x_df['age'].notnull().sum()} "
          f"({str(round_sig(100 * x_df['age'].notnull().sum() / len(x_df)))}%)\n")
    print(f"Age NaNs before merging with dicom: "
          f"{x_df['age_b4dcm'].isnull().sum()} ({str(round_sig(100 * x_df['age_b4dcm'].isnull().sum() / len(x_df)))}%)")
    print(f"Age NaNs after merging with dicom: "
          f"{x_df['age'].isnull().sum()} ({str(round_sig(100 * x_df['age'].isnull().sum() / len(x_df)))}%)")
    print(f"Age Unknowns before merging with dicom: "
          f"{(x_df['age_b4dcm'] == 'Unknown').sum()} "
          f"({str(round_sig(100 * (x_df['age_b4dcm'] == 'Unknown').sum() / len(x_df)))}%)")
    print(f"Age Unknowns after merging with dicom: "
          f"{(x_df['age'] == 'Unknown').sum()} "
          f"({str(round_sig(100 * (x_df['age'] == 'Unknown').sum() / len(x_df)))}%)\n")

    print('-'*5, "\nCOVID-POSITIVE CASES ONLY (FULL CLINICAL DATA):\n")
    print(f"Age Values in COV-positives before merging with dicom: "
          f"{(x_pos['age_b4dcm'].notna()).sum()} "
          f"({str(round_sig(100 * x_pos['age_b4dcm'].notna().sum() / len(x_pos)))}%)")
    print(f"Age Values in COV-positives after merging with dicom: "
          f"{(x_pos['age'].notna()).sum()} ({str(round_sig(100 * x_pos['age'].notna().sum() / len(x_pos)))}%)\n")
    print(f"Age NaNs in COV-positives before merging with dicom: "
          f"{x_pos['age_b4dcm'].isnull().sum()} "
          f"({str(round_sig(100 * x_pos['age_b4dcm'].isnull().sum() / len(x_pos)))}%)")
    print(f"Age NaNs in COV-positives after merging with dicom: "
          f"{x_pos['age'].isnull().sum()} ({str(round_sig(100 * x_pos['age'].isnull().sum() / len(x_pos)))}%)\n")
    print('Age values added for the COVID-positive patients from DICOM enrichment are for mainly three centres:\n')
    print(value_counts_with_percentage(
        x_pos[x_pos['age_b4dcm'].isnull() & x_pos['age'].notnull()].SubmittingCentre), "\n")
    print('None of St. Georges, Leicester or the Walton Centre provide any data other than PCR swab dates, '
          'which is why these values were missing prior to DICOM enrichment.\n')


def analyse_pmh_htn(
        x_pos: pd.DataFrame,
        n_pos: pd.DataFrame
) -> NoReturn:
    """
    Generates analysis of the pmh hypertension feature and prints to stdout.

    :param x_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccidxclean tool
    :type x_pos: pd.DataFrame
    :param n_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccid-cleaning tool
    :type n_pos: pd.DataFrame
    :return: None
    """

    print('-' * 50, '\nPMH HYPERTENSION...\n')
    print(f'Values with NCCID pipeline:\n {value_counts_with_percentage(n_pos.pmh_hypertension)}\n')
    print(f'Values for after new pipeline (NB. 2=Unknown):\n {value_counts_with_percentage(x_pos.pmh_hypertension)}\n')


def analyse_pmh_cvs(
        x_pos: pd.DataFrame,
        n_pos: pd.DataFrame
) -> NoReturn:
    """
    Generates analysis of the pmh cvs disease feature and prints to stdout.

    :param x_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccidxclean tool
    :type x_pos: pd.DataFrame
    :param n_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccid-cleaning tool
    :type n_pos: pd.DataFrame
    :return: None
    """

    print('-' * 50, '\nPMH CVS DISEASE...\n')
    print(f'Values entered by Sandwell and West Birmingham:\n'
          f'{value_counts_with_percentage(n_pos[n_pos.hospital.str.contains("Sand")].pmh_cvs_disease)}')
    calc_sand_perc = 100 * len(n_pos[n_pos.hospital.str.contains("Sand") & (n_pos.pmh_cvs_disease == 1)]) / \
                     len(n_pos[n_pos.hospital.str.contains("Sand")])
    print(f'This is {str(calc_sand_perc)}% of the values entered by Sandwell for the "COVID-positive" patients.\n'
          f'(i.e. they were all "Myocardial infarction")\n')
    print(f'Values for Sandwell and West Birmingham after new pipeline:\n'
          f'{value_counts_with_percentage(x_pos[x_pos.hospital.str.contains("Sand")].pmh_cvs_disease)}\n')
    print(f'Values in original data:\n{n_pos.pmh_cvs_disease.value_counts(dropna=False)}\n')


def categorical_fields_analysis(
        x_df: pd.DataFrame,
        x_pos: pd.DataFrame,
        n_pos: pd.DataFrame,
) -> NoReturn:
    """
    Generates analysis for the categorical fields of the nccid data cleaned using the nccidxclean pipeline.
    Outputs are printed to stdout.

    :param x_df: dataframe of numeric variables cleaned with the nccidxclean tool
    :type x_df: pd.DataFrame
    :param x_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccidxclean tool
    :type x_pos: pd.DataFrame
    :param n_pos: dataframe of numeric variables for patients with a positive PCR, cleaned with the nccid-cleaning tool
    :type n_pos: pd.DataFrame
    :return: None
    """

    analyse_sex(x_df, x_pos, n_pos)
    analyse_age(x_df, x_pos)
    analyse_pmh_htn(x_pos, n_pos)
    analyse_pmh_cvs(x_pos, n_pos)
