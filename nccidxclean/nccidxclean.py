"""
This module contains the main function for the nccidxclean package to clean NCCID clinical data.
"""

from typing import Collection, Union
import pandas as pd
from pathlib import Path
import argparse
import nccid_cleaning.cleaning as nhsx

import nccidxclean.clean as xclean


def _check_parsed_pipeline(pl: Collection[str], _preferred_function_order):
    """
    check not included overlapping nccid and cam functions in the pipeline
    e.g. 'cleaning._parse_date_columns' and '_xp_parse_date_columns'.

    :param pl: list or tuple of functions
    :type pl: Collection
    :return: None
    :rtype: NoneType
    """

    check_list = []
    for function in pl:
        if _preferred_function_order[function] not in check_list:
            check_list.append(_preferred_function_order[function])
        else:
            raise Exception(
                "ERROR! You are trying to use overlapping NHSx and Extended functions:\n",
                [key for key, value in _preferred_function_order.items() if
                 _preferred_function_order[function] in value]
            )
    return


def xclean_nccid(
        input_df: pd.DataFrame,
        xpipeline: Union[str, Collection] = 'xclean_default',
        return_cols: str = 'spo2_imputed_cleaned_only',
        collapse_pmh: bool = True,
        fio2_ltrs_to_percent: bool = True,
) -> pd.DataFrame:
    """
    Runs the full pipeline on a pandas dataframe (data_df).

    :param input_df: Input dataframe of clinical data from NCCID.
    :type input_df: pd.DataFrame
    :param xpipeline: List/tuple of cleaning functions to apply to the dataframe, which can be modified with the 
        'collapse_pmh' and 'spo2_ltrs_to_percent' parameters. The default pipeline is:
        'xclean_default' = [xclean.check_new_centres, xclean.column_shift, xclean.remap_hospitals, xclean.remap_sex,
        xclean.remap_ethnicity, xclean.parse_date_columns, xclean.parse_binary_and_cat, xclean.binarise_lung_csv,
        xclean.rescale_fio2, nhsx._coerce_numeric_columns, nhsx._remap_test_result_columns, xclean.clean_numeric,
        xclean.clip_numeric, xclean.fix_headers, xclean.sense_check, xclean.inferences]
    :type xpipeline: Union[str, Collection], optional, default = 'xclean_default'
    :param return_cols: Dataframe columns returned by the pipeline:
        'spo2_imputed_cleaned_only': returns the cleaned columns only, with spo2 imputed from pao2.
        'spo2_imputed_with_original': returns the original and cleaned columns, with spo2 and pao2.
        'o2_split_cleaned_only': returns the cleaned columns only, with spo2 and pao2.
        'o2_split_with_original': returns the original and cleaned columns, with spo2 and pao2.
        'all_cleaned_only': returns all possible cleaned columns.
        'all_with_original': returns all possible original and cleaned columns.
    :type return_cols: str, optional, default = spo2_imputed_cleaned_only
    :param collapse_pmh: Whether to collapse the pre-existing medical history values to binary + unknown in the default 
        pipeline.
    :type collapse_pmh: bool, optional, default = True
    :param fio2_ltrs_to_percent: Whether to convert FiO2 values in litres to percentages in the default pipeline.  This
        will prevent the P/F ratio being calculated in the inferences step.
    :type fio2_ltrs_to_percent: bool, optional, default = True
    :return: Cleaned dataframe.
    :rtype: pd.DataFrame
    """

    import logging.config
    logging_config_path = Path(__file__).resolve().parent / 'log.conf'
    logging.config.fileConfig(logging_config_path, disable_existing_loggers=False)
    console_msg = logging.getLogger('consoleLog')

    # dictionary to allow functions to be applied in the correct order
    preferred_function_order = {
        xclean.check_new_centres: 1,
        xclean.column_shift: 2,
        xclean.remap_hospitals: 3,
        nhsx._remap_sex: 4,
        xclean.remap_sex: 4,
        nhsx._remap_ethnicity: 5,
        xclean.remap_ethnicity: 5,
        nhsx._parse_binary_columns: 6,
        nhsx._parse_cat_columns: 7,
        xclean.parse_binary_and_cat: 7,
        xclean.binarise_lung_csv: 8,
        nhsx._remap_test_result_columns: 9,
        nhsx._parse_date_columns: 10,
        xclean.parse_date_columns: 10,
        nhsx._rescale_fio2: 11,
        xclean.rescale_fio2: 11,
        nhsx._coerce_numeric_columns: 12,
        xclean.clean_numeric: 13,
        nhsx._clip_numeric_values: 14,
        xclean.clip_numeric: 14,
        nhsx._fix_headers: 15,
        xclean.fix_headers: 15,
        xclean.sense_check: 16,
        xclean.inferences: 17,
    }

    if fio2_ltrs_to_percent:
        inference_pipeline = (
            'update_final_covid_status',
            'death_inferences',
            'last_known_alive_inferences',
            'itu_and_intubation_inferences',
            'ckd_inferences',
            'calculate_pf_ratio',
        )
    elif not fio2_ltrs_to_percent:
        # If not converting FiO2 values to percentages, then the P/F ratio cannot be calculated
        inference_pipeline = (
            'update_final_covid_status',
            'death_inferences',
            'last_known_alive_inferences',
            'itu_and_intubation_inferences',
            'ckd_inferences',
        )
    else:
        raise ValueError('fio2_ltrs_to_percent must be a boolean.')

    if xpipeline == 'xclean_default':
        if collapse_pmh:
            xpipeline = (
                xclean.check_new_centres,
                xclean.column_shift,
                xclean.remap_hospitals,
                xclean.remap_sex,
                xclean.remap_ethnicity,
                xclean.parse_date_columns,
                xclean.parse_binary_and_cat,
                xclean.binarise_lung_csv,
                (xclean.rescale_fio2, fio2_ltrs_to_percent),
                nhsx._coerce_numeric_columns,
                nhsx._remap_test_result_columns,
                xclean.clean_numeric,
                xclean.clip_numeric,
                xclean.fix_headers,
                xclean.sense_check,
                (xclean.inferences, inference_pipeline),
            )
        elif not collapse_pmh:
            xpipeline = (
                xclean.check_new_centres,
                xclean.column_shift,
                xclean.remap_hospitals,
                xclean.remap_sex,
                xclean.remap_ethnicity,
                xclean.parse_date_columns,
                xclean.parse_binary_and_cat,
                (xclean.rescale_fio2, fio2_ltrs_to_percent),
                nhsx._coerce_numeric_columns,
                nhsx._remap_test_result_columns,
                xclean.clean_numeric,
                xclean.clip_numeric,
                xclean.fix_headers,
                xclean.sense_check,
                (xclean.inferences, inference_pipeline)
            )
        else:
            raise ValueError('collapse_pmh must be boolean')
    else:
        _check_parsed_pipeline(xpipeline, preferred_function_order)

    # Run the program - clean_data_df (imported from nccid-cleaning) runs the specified pipeline
    console_msg.info('{:=^50}'.format(' BEGINNING CLEANING '))
    data_df = nhsx.clean_data_df(input_df, sorted(xpipeline, key=preferred_function_order.get))
    console_msg.info('{:=^50}'.format(' FINISHED CLEANING '))

    # Order columns for final outputs
    data_df_sorted = xclean.order_columns(data_df)
    # Create error report
    data_df_sorted = xclean.create_error_report(data_df_sorted, 'xclean.log')
    # Select columns for final output
    data_df_selected_cols = xclean.select_output_cols(data_df_sorted, return_cols)

    console_msg.info('{:=^50}'.format(' DONE! '))
    console_msg.info('Please now review the potential errors in `.for_review` and the cleaned data')

    return data_df_selected_cols


def main():
    """
    Handles command line execution of the cleaning pipeline.
    """

    def validate_basepath(string):
        value = str(string)
        if not Path(value).exists():
            raise argparse.ArgumentError(base_path_arg, f"path does not exist: {value}")
        return Path(value)

    def validate_subdir(base_dir, string, arg):
        if string is not None:
            value = str(string)
            if not (base_dir / value).exists():
                raise argparse.ArgumentError(arg, f"path does not exist: {value}")
        return

    parser = argparse.ArgumentParser(
        description='''Extracts NCCID clinical data from JSON's and cleans data with the NCCIDxClean Pipeline.''',
        epilog='''Thank you for using NCCIDxClean!'''
    )
    base_path_arg = parser.add_argument('base_path', type=validate_basepath,
                                        help="base directory - should have same structure as original S3 bucket")
    clinical_subdir_arg = parser.add_argument('clinical_subdir', type=str,
                                              help="subdirectory containing the clinical data json files")
    xray_subdir_arg = parser.add_argument("--xray_subdir", nargs=1, type=str,
                                          help="subdirectory containing the x-ray dicom files")
    ct_subdir_arg = parser.add_argument("--ct_subdir", nargs=1, type=str,
                                        help="subdirectory containing the ct dicom files")
    mri_subdir_arg = parser.add_argument("--mri_subdir", nargs=1, type=str,
                                         help="subdirectory containing the mri dicom files")
    return_cols_arg = parser.add_argument("--return_cols", nargs=1, type=str,
                                          help="oxygen columns in the dataframe returned by the pipeline")
    collapse_pmh_arg = parser.add_argument("--collapse_pmh", nargs=1, type=str,
                                           help="whether to collapse the pre-existing medical history values to binary "
                                                "+ unknown")
    fio2_ltrs_to_percent_arg = parser.add_argument("--fio2_ltrs_to_percent", nargs=1, type=str,
                                                   help="whether to convert fio2 values in litres to percentages")

    args = parser.parse_args()

    imaging_subdirs = {
        'xray': args.xray_subdir,
        'ct': args.ct_subdir,
        'mri': args.mri_subdir,
    }
    base_path = Path(args.base_path)

    # validate subdirectories provided
    for arg, val in vars(args).items():
        if arg != 'base_path' and val is not None:
            validate_subdir(base_path, val, arg)

    # read in dicom metadata if subdirectories provided
    dcm_meta = None
    if any(arg is not None for arg in imaging_subdirs):
        dcm_meta = xclean.extract_dcm_metadata(base_path, imaging_subdirs)

    # read in clinical data
    patients = xclean.read_in_clinical_data(base_path, args.clinical_subdir)

    # cleaning
    patients = xclean_nccid(
        patients,
        return_cols = args.return_cols_arg,
        collapse_pmh = args.collapse_pmh_arg,
        fio2_ltrs_to_percent = args.fio2_ltrs_to_percent_arg,
    )

    # update data with previous manual checks and dicom enrichment
    patients = xclean.update_with_prev_changes(patients)

    # enriching and sense checking using dicom metadata
    if dcm_meta is not None:
        patients = xclean.add_dicom_update(patients, list(dcm_meta.values()))
        for modality, data in dcm_meta.items():
            xclean.sense_check_dicom_dates(patients, data)
    xclean.save_dcm_updates(patients)

    # create output directory if does not exist
    outpath = Path("./data")
    outpath.mkdir(exist_ok=True)

    # save
    for modality, metadata in dcm_meta.items():
        metadata.to_csv(outpath / (modality + ".csv"), index=False)

    patients.to_csv(outpath / "patients.csv", index=False)


if __name__ == '__main__':
    main()
