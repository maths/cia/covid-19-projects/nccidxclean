from pathlib import Path
import os
import pandas as pd
import argparse
import nccid_cleaning.etl as etl
from nccid_cleaning import clean_data_df, patient_df_pipeline

from nccidxclean.clean.enrich_with_dcm import extract_dcm_metadata
from nccidxclean.clean.utils import read_in_clinical_data


def load_dcm_meta(arguments):

    meta = {}
    if arguments.xray_meta_path_arg is not None:
        meta['xrays'] = pd.read_csv(arguments.xray_meta_path_arg)
    if arguments.ct_meta_path_arg is not None:
        meta['ct'] = pd.read_csv(arguments.xray_meta_path_arg)
    if arguments.mri_meta_path_arg is not None:
        meta['mri'] = pd.read_csv(arguments.xray_meta_path_arg)

    if len(meta) == 0:
        imaging_subdirs = {
            'xray': arguments.xray_subdir,
            'ct': arguments.ct_subdir,
            'mri': arguments.mri_subdir,
        }
        meta = extract_dcm_metadata(base_path, imaging_subdirs)

    return meta


if __name__ == "__main__":

    def validate_path(argument, string):
        value = str(string)
        if not Path(value).exists():
            raise argparse.ArgumentError(argument, f"path does not exist: {value}")
        return Path(value)

    parser = argparse.ArgumentParser(
        description='''Cleans data with the original NHSx nccid-cleaning pipeline.''',
    )
    base_path_arg = parser.add_argument('base_path', type=str,
                                        help="base directory for the nccid data files - should have same "
                                             "structure as original S3 bucket")
    clinical_subdir_arg = parser.add_argument('clinical_subdir', type=str,
                                              help="subdirectory containing the clinical data json files")
    xray_subdir_arg = parser.add_argument("--xray_subdir", nargs=1, type=str, default=None,
                                          help="subdirectory containing the x-ray dicom files"
                                               " - only required if not already extracted")
    ct_subdir_arg = parser.add_argument("--ct_subdir", nargs=1, type=str, default=None,
                                        help="subdirectory containing the ct dicom files"
                                             " - only required if not already extracted")
    mri_subdir_arg = parser.add_argument("--mri_subdir", nargs=1, type=str, default=None,
                                         help="subdirectory containing the mri dicom files"
                                              " - only required if not already extracted")
    xray_meta_path_arg = parser.add_argument("--xray_meta_path", nargs=1, type=str, default=None,
                                             help="path to x-ray dicom metadata csv file if already extracted")
    ct_meta_path_arg = parser.add_argument("--ct_meta_path", nargs=1, type=str, default=None,
                                           help="path to ct dicom metadata csv file if already extracted")
    mri_meta_path_arg = parser.add_argument("--mri_meta_path", nargs=1, type=str, default=None,
                                            help="path to mri dicom metadata csv file if already extracted")
    args = parser.parse_args()

    for arg, val in vars(args).items():
        if val is not None:
            validate_path(arg, val)

    base_path = Path(args.base_path)

    # Get DICOM metadata for enrichment of clinical data
    dcm_meta = load_dcm_meta(args)

    # Read in clinical data
    patients_df = read_in_clinical_data(args.base_path, args.clinical_subdir)

    # Clean clinical data
    patients_df = clean_data_df(patients_df, patient_df_pipeline)

    # Enrich clinical data with DICOM metadata
    patients_df = etl.patient_data_dicom_update(patients_df, list(dcm_meta.values()))

    # Output directory
    out_dir = Path("./data")
    out_dir.mkdir(exist_ok=True)

    # Save cleaned data
    patients_df.to_csv(out_dir / "nhsx_patients.csv")
