"""
This module produces plots and numeric analysis used to compare the nccid-cleaning and nccidxclean pipelines.
"""


import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
import argparse
from typing import Dict, NoReturn, Collection, Union

from nccidxclean.figures.sns_settings import *
from nccidxclean.figures.prepare_dataframes import prepare_dataframes_for_comparison, allocate_hospital_codes
from nccidxclean.figures.overall_numbers import make_figure_1
from nccidxclean.figures.categorical import make_figure_2, make_figure_6
from nccidxclean.figures.dates import prepare_dataframe_dates, make_figure_3, make_figure_7
from nccidxclean.figures.numeric import make_figure_4, make_figure_5, make_figure_8
from nccidxclean.num_analysis.num_analysis import numeric_analysis
from nccidxclean.clean.dicts_and_maps import original_cols


def print_df_as_latex(
        df: pd.DataFrame,
        caption: str,
        label: str
) -> NoReturn:
    """
    Prints a dataframe as a latex table.

    :param df: dataframe to print
    :type df: pd.DataFrame
    :param caption: caption for the table
    :type caption: str
    :param label: label for the table
    :type label: str
    """
    print(df.to_latex(index=False))
    print(f"\\caption{{{caption}}}")
    print(f"\\label{{tab:{label}}}")
    print()


def make_figures_and_numeric_analysis(
        nhs_df: pd.DataFrame,
        xtd_df: pd.DataFrame,
        dcm_df: pd.DataFrame
) -> Dict[int, Collection[Union[plt.Figure, plt.Axes]]]:
    """
    Produces plots and numeric analysis used to compare the nccid-cleaning and nccidxclean pipelines.
    Numeric analysis is written to stdout, with plots saved to ./charts.

    :param nhs_df: dataframe containing clinical data cleaned by nccid-cleaning
    :type nhs_df: pd.DataFrame
    :param xtd_df: dataframe containing clinical data cleaned by nccidxclean
    :type xtd_df: pd.DataFrame
    :param dcm_df: dataframe containing dicom metadata
    :type dcm_df: pd.DataFrame
    :return: dictionary containing figures
    :rtype: Dict[int, plt.Figure]
    """
    # Set figure parameters
    plt.rcParams['figure.dpi'] = dpi / 2
    plt.rcParams['savefig.dpi'] = dpi

    sns.set_theme(
        style='ticks',
        palette='colorblind',
        font_scale=1,
        color_codes=True,
        rc=image_params
    )

    # Make clinical dataframes comparable
    nhs_df, xtd_df = prepare_dataframes_for_comparison(nhs_df, xtd_df)

    # Extract only patients in the positive cohort
    nhs_pos = nhs_df[
        nhs_df['filename_covid_status'] == True
        ].sort_values(['SubmittingCentre', 'hospital']).reset_index(drop=True)
    xtd_pos = xtd_df[
        xtd_df['filename_covid_status'] == True
        ].sort_values(['SubmittingCentre', 'hospital']).reset_index(drop=True)

    # Allocate hospital codes
    hosp_df = allocate_hospital_codes(xtd_pos)
    # Print out latex table
    print_df_as_latex(hosp_df, "Hospital codes", "hosp_codes")

    # as multiple centers have an 'Unknown' in hospital, from here on we will replace
    # 'Unknown' with the center name
    hosp_df.loc[hosp_df.hospital == "Unknown", 'hospital'] = hosp_df.SubmittingCentre
    nhs_df.loc[nhs_df.hospital == "Unknown", 'hospital'] = nhs_df.SubmittingCentre
    xtd_df.loc[xtd_df.hospital == "Unknown", 'hospital'] = xtd_df.SubmittingCentre
    nhs_pos.loc[nhs_pos.hospital == "Unknown", 'hospital'] = nhs_pos.SubmittingCentre
    xtd_pos.loc[xtd_pos.hospital == "Unknown", 'hospital'] = xtd_pos.SubmittingCentre

    # Create code dictionary and hospital sorter
    hosp_codes = {row['hospital']: row['Code'] for index, row in hosp_df.iterrows()}
    sorter = {hosp: i for i, hosp in enumerate(hosp_codes.keys())}

    # Create output folder
    Path("./charts").mkdir(exist_ok=True)

    # Prepare dataframes for date figure (3)...
    nhs_pos, xtd_pos, nhs_dates, xtd_dates = prepare_dataframe_dates(nhs_pos, xtd_pos, dcm_df)

    # CREATE FIGURES...
    # figs = {
    #     1: make_figure_1(nhs_pos, xtd_pos),
    #     2: make_figure_2(nhs_pos, xtd_pos),
    #     3: make_figure_3(nhs_pos, xtd_pos, nhs_dates, xtd_dates, hosp_codes, sorter),
    #     4: make_figure_4(nhs_pos, xtd_pos),
    #     5: make_figure_5(nhs_pos, xtd_pos, hosp_codes),
    #     6: make_figure_6(nhs_pos, xtd_pos, hosp_codes),
    #     7: make_figure_7(nhs_pos, hosp_codes),
    #     8: make_figure_8(nhs_pos, hosp_codes, sorter),
    # }

    # PERFORM NUMERIC ANALYSIS...
    if not all(x in xtd_df.columns.tolist() for x in original_cols):
        xtd_df = xtd_df.merge(nhs_df[["Pseudonym"] + original_cols], on='Pseudonym', how='left')
        xtd_pos = xtd_pos.merge(nhs_pos[["Pseudonym"] + original_cols], on='Pseudonym', how='left')

    numeric_analysis(xtd_df, xtd_pos, nhs_pos, xtd_dates, nhs_dates)

    # return figs


def main():
    """
    Handles command line execution of the analysis module on cleaned NCCID data.
    """

    def validate_path(arg, string):
        value = str(string)
        if not Path(value).exists():
            raise argparse.ArgumentError(arg, f"path does not exist: {value}")
        return Path(value)


    parser = argparse.ArgumentParser(
        description='''Creates plots used in our write up.''',
    )
    base_path_arg = parser.add_argument('nhsx_cleaned_path', type=str,
                                        help="path to the data cleaned with the nhsx pipeline")
    clinical_subdir_arg = parser.add_argument('extended_cleaned_path', type=str,
                                              help="path to the data cleaned with the extended nccidxclean pipeline")
    xray_subdir_arg = parser.add_argument("--xray_meta_path", nargs=1, type=str, default=None,
                                          help="path to the file containing the x-ray dicom metadata")
    ct_subdir_arg = parser.add_argument("--ct_meta_path", nargs=1, type=str, default=None,
                                        help="path to the file containing the ct dicom metadata")
    mri_subdir_arg = parser.add_argument("--mri_meta_path", nargs=1, type=str, default=None,
                                         help="path to the file containing the mri dicom metadata")
    args = parser.parse_args()

    # validate paths provided
    for arg, val in vars(args).items():
        if val is not None:
            validate_path(arg, val)

    # Read in cleaned clinical data
    nhsx = pd.read_csv(args.nhsx_cleaned_path, low_memory=False)
    xtd = pd.read_csv(args.extended_cleaned_path, low_memory=False)

    # Read in dicom metadata
    modality_paths = [args.xray_meta_path, args.ct_meta_path, args.mri_meta_path]
    metadata = [pd.read_csv(modality, dtype=str) for modality in modality_paths if modality is not None]
    imaging_df = pd.concat(metadata)

    make_figures_and_numeric_analysis(nhsx, xtd, imaging_df)


if __name__ == '__main__':
    main()
