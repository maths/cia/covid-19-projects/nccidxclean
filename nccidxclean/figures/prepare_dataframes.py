"""

The functions in this module prepare the dataframes from the
nccid-cleaning and nccidxclean pipelines for comparison.  Without it, the columns are not
equivalent. It is required before producing any of the figures.

"""

import string
import numpy as np
import pandas as pd
from typing import Tuple


def prepare_dataframes_for_comparison(
        nhsx_df: pd.DataFrame,
        xtd_df: pd.DataFrame
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Prepares the dataframes from the nccid-cleaning and nccidxclean pipelines for comparison.

    :param nhsx_df: dataframe containing clinical data cleaned by nccid-cleaning
    :type nhsx_df: pd.DataFrame
    :param xtd_df: dataframe containing clinical data cleaned by nccidxclean
    :type xtd_df: pd.DataFrame
    :return: nhsx_df, xtd_df
    :rtype: pd.DataFrame, pd.DataFrame
    """
    # make nhs dataframe comparable to new...

    def _make_pmh_lung_disease_string(df):
        df.pmh_lung_disease = df.pmh_lung_disease.fillna(-1).astype(int).astype(str)
        df.loc[df.pmh_lung_disease == '-1', 'pmh_lung_disease'] = np.nan
        return df

    # make pmh hypertension for the raw data include pmh h1pertension
    nhsx_df.loc[
        nhsx_df['PMH hypertension'].isna() | (nhsx_df['PMH hypertension'] == ""), 'PMH hypertension'
    ] = nhsx_df['PMH h1pertension']
    # rename age and sex columns
    nhsx_df = nhsx_df.rename({'age': 'age_b4dcm', 'sex': 'sex_b4dcm'}, axis=1)
    nhsx_df = nhsx_df.rename({'age_update': 'age', 'sex_update': 'sex'}, axis=1)
    # sort cleaned lung disease field
    nhsx_df = _make_pmh_lung_disease_string(nhsx_df)
    # merge the cleaned hospitals with the nhsx data
    nhsx_df = nhsx_df.merge(xtd_df[['Pseudonym', 'hospital']], how='left', on='Pseudonym')

    return nhsx_df, xtd_df


def allocate_hospital_codes(df: pd.DataFrame) -> pd.DataFrame:
    """
    Allocates a code to each hospital in the dataframe.

    :param df: dataframe containing hospital names and submitting centres
    :type df: pd.DataFrame
    :return: dataframe containing hospital names, submitting centres and allocated code
    :rtype: pd.DataFrame
    """
    # initialise variables
    hosp_df = df[['SubmittingCentre', 'hospital']].drop_duplicates().reset_index(drop=True)
    codes = list(string.ascii_uppercase) + ['α', 'β', 'γ', 'δ', 'ε'] + list(string.ascii_lowercase)
    hosp_df['Code'] = np.nan

    # allocate code
    for index, row in hosp_df.iterrows():
        hosp_df.loc[index, 'Code'] = codes[index]

    hosp_df = hosp_df[['Code', 'hospital', 'SubmittingCentre']]

    return hosp_df


cleaned_cols = [
    'age_b4dcm',
    'ethnicity',
    'sex_b4dcm',
    'smoking_status',
    'pack_year_history',
    'pmh_hypertension',
    'pmh_cvs_disease',
    'pmh_diabetes_mellitus_type_2',
    'pmh_lung_disease',
    'pmh_ckd',
    'if_ckd_stage',
    'current_acei_use',
    'current_angiotension_receptor_blocker_use',
    'current_nsaid_used',
    'date_of_admission',
    'duration_of_symptoms',
    'respiratory_rate_on_admission',
    'heart_rate_on_admission',
    'pao2',
    'fio2',
    'systolic_bp',
    'diastolic_bp',
    'temperature_on_admission',
    'news2_score_on_arrival',
    'wcc_on_admission',
    'lymphocyte_count_on_admission',
    'platelet_count_on_admission',
    'crp_on_admission',
    'd-dimer_on_admission',
    'fibrinogen__if_d-dimer_not_performed',
    'ferritin',
    'urea_on_admission',
    'creatinine_on_admission',
    'troponin_i',
    'troponin_t',
    'date_of_acquisition_of_1st_rt-pcr',
    'date_of_result_of_1st_rt-pcr',
    '1st_rt-pcr_result',
    'date_of_acquisition_of_2nd_rt-pcr',
    'date_of_result_of_2nd_rt-pcr',
    '2nd_rt-pcr_result',
    'negative_swab_date',
    'final_covid_status',
    'date_of_positive_covid_swab',
    'date_of_1st_cxr',
    'covid_code',
    'cxr_severity',
    'date_of_2nd_cxr',
    'covid_code_2',
    'cxr_severity_2',
    'itu_admission',
    'date_of_itu_admission',
    'apache_score_on_itu_arrival',
    'intubation',
    'date_of_intubation',
    'death',
    'date_of_death',
    'date_last_known_alive',
]


raw_cols = [
    'Age',
    'Ethnicity',
    'Sex',
    'Smoking status',
    'Pack year history',
    'PMH hypertension',
    'PMH CVS disease',
    'PMH diabetes mellitus type II',
    #     'PMH diabetes mellitus TYPE II',
    'PMH Lung disease',
    'PMH CKD',
    'If CKD, stage',
    'Current ACEi use',
    'Current Angiotension receptor blocker use',
    'Current NSAID used',
    'Date of admission',
    'Duration of symptoms',
    'Respiratory rate on admission',
    'Heart rate on admission',
    #     'O2 saturation',
    'PaO2',
    'FiO2',
    #     'Any supplemental oxygen: FiO2',
    'Systolic BP',
    'Diastolic BP',
    'Temperature on admission',
    'NEWS2 score on arrival',
    'WCC on admission',
    'Lymphocyte count on admission',
    'Platelet count on admission',
    'CRP on admission',
    'D-dimer on admission',
    'Fibrinogen  if d-dimer not performed',
    'Ferritin',
    'Urea on admission',
    'Creatinine on admission',
    'Troponin I',
    'Troponin T',
    'Date of acquisition of 1st RT-PCR',
    'Date of result of 1st RT-PCR',
    '1st RT-PCR result',
    'Date of acquisition of 2nd RT-PCR',
    'Date of result of 2nd RT-PCR',
    '2nd RT-PCR result',
    'Final COVID Status',
    'Date of Positive Covid Swab',
    'Date of 1st CXR',
    'COVID CODE',
    'CXR severity',
    'Date of 2nd CXR',
    'COVID CODE 2',
    'CXR severity 3',
    'ITU admission',
    'Date of ITU admission',
    'APACHE score on ITU arrival',
    'Intubation',
    'Date of intubation',
    'Death',
    'Date of death',
    'Date last known alive',
    'SwabDate'
]
