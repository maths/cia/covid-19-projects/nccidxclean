"""
This module contains functions used to create the figures for the numerical fields used in our write-up (Figures 4,5,8).

N.B. The prepare_dataframes_for_comparison and allocate_hospital_codes functions must be run prior to producing any of
the figures.
"""

import pandas as pd
import numpy as np
import matplotlib.ticker as mtick
from matplotlib import pyplot as plt
from typing import Tuple, Dict

from nccidxclean.figures.sns_settings import *

numeric_charts = {
    'creatinine_on_admission': 'Creatinine on admission',
    'troponin_i': 'Troponin I',
    'd-dimer_on_admission': 'D-dimer on admission',
    'fio2': 'FiO$\mathbf{_{2}}$',
}


def is_float(string: object) -> bool:
    """
    Checks if a string can be converted to a float.

    :param string: input variable
    :type string: str
    :return: boolean indicating whether the string can be converted to a float
    :rtype: bool
    """
    try:
        if pd.isna(string):
            return False
        else:
            float(string)
            return True
    except ValueError:
        return False


def create_numeric_results_fig(
        df: pd.DataFrame,
        field: str,
        axis: plt.Axes
) -> plt.Axes:
    """
    Creates a histogram for a numeric field.

    :param df: dataframe containing the data
    :type df: pd.DataFrame
    :param field: field to plot
    :type field: str
    :param axis: axis to plot on
    :type axis: plt.Axes
    :return: axis containing the histogram
    :rtype: plt.Axes
    """

    df = df[df[field].notna()].reset_index(drop=True)

    log_chart = False

    if field == 'troponin_i':
        df = df[df[field] < 32]
    elif field == 'creatinine_on_admission' or field == 'fio2':
        df = df[df[field] < 100]
    elif field == 'd-dimer_on_admission':
        log_chart = True

    sns.histplot(x=field,
                 hue="Dataset",
                 data=df,
                 multiple="dodge",
                 shrink=.9,
                 palette=dataset_palette,
                 ax=axis,
                 log_scale=log_chart,
                 bins=10,
                 alpha=colour_alpha,
                 )

    axis.set(xlabel=numeric_charts[field], ylabel='Patient Count')

    return axis


def clean_raw_data(df_raw: pd.DataFrame) -> pd.DataFrame:
    """
    Cleans the raw data ready for plotting.

    :param df_raw: dataframe containing the raw data to be cleaned
    :type df_raw: pd.DataFrame
    :return: cleaned dataframe
    :rtype: pd.DataFrame
    """

    df = df_raw[
        ['Pseudonym', 'hospital', 'FiO2', 'Creatinine on admission', 'Troponin I', 'D-dimer on admission']].copy()
    num_fields = df.columns.tolist()
    num_fields.remove('Pseudonym')
    for col in num_fields:
        df = df.rename({col: col.lower().replace(" ", "_")}, axis=1)

    # FiO2
    df.fio2 = df.fio2.str.replace('L', "").str.replace('l', "").astype(float)

    # D-Dimer
    df.loc[~(df['d-dimer_on_admission'].apply(is_float, ) | df
    ['d-dimer_on_admission'].isna()), 'd-dimer_on_admission'] = np.nan
    df['d-dimer_on_admission'] = df['d-dimer_on_admission'].astype(float)

    # Troponin
    df.loc[~(df['troponin_i'].apply(is_float, ) | df['troponin_i'].isna()), 'troponin_i'] = np.nan
    df.loc[~df.troponin_i.apply(is_float, ), 'troponin_i'] = np.nan
    df.troponin_i = df.troponin_i.astype(float)

    # Creatinine
    df.loc[~(df['creatinine_on_admission'].apply(is_float, ) | df
    ['creatinine_on_admission'].isna()), 'creatinine_on_admission'] = np.nan
    df.creatinine_on_admission = df.creatinine_on_admission.astype(float)

    return df


def make_figure_4(
        n_pos: pd.DataFrame, x_pos: pd.DataFrame
) -> Tuple[plt.Figure, plt.Axes]:
    """
    Generates figure 4 from the paper - histograms of FiO2, Creatinine, Troponin and D-Dimer for
    the raw data, nccid-cleaning pipeline, and nccidxclean pipeline.

    :param n_pos: dataframe containing clinical data cleaned by nccid-cleaning
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe containing clinical data cleaned by nccidxclean
    :type x_pos: pd.DataFrame
    :return: figure and axis containing figure 4
    :rtype: plt.Figure, plt.Axes
    """

    fig, ax = plt.subplots(
        nrows=2,
        ncols=2,
        figsize=(default_width * 2, default_height * 2),
    )

    plt.subplots_adjust(wspace=0.2, hspace=0.3)

    raw_pos = clean_raw_data(n_pos)

    raw_temp = raw_pos[['Pseudonym', 'hospital'] + list(numeric_charts.keys())].copy()
    nhs_temp = n_pos[['Pseudonym', 'hospital'] + list(numeric_charts.keys())].copy()
    cam_temp = x_pos[['Pseudonym', 'hospital'] + list(numeric_charts.keys())].copy()

    raw_temp['Dataset'] = 'Raw Data'
    nhs_temp['Dataset'] = 'NHSx Pipeline'
    cam_temp['Dataset'] = 'Extended Pipeline'

    temp_df = pd.concat([raw_temp, nhs_temp, cam_temp]).sort_values(by="Dataset", ascending=False)

    for i, chart in enumerate(numeric_charts.keys()):
        if i <= 1:
            create_numeric_results_fig(temp_df, chart, ax[0, i])
            if i == 1:
                sns.move_legend(ax[0, i], loc='upper right', bbox_to_anchor=(1, 1))
                ax[0, i].get_legend().set(title='')
            else:
                ax[0, i].get_legend().remove()
        else:
            create_numeric_results_fig(temp_df, chart, ax[1, 2 - i])
            ax[1, 2 - i].get_legend().remove()

    ax[1, 0].set(xlabel='$\mathbf{log_{10}}$ (D-dimer on admission)', )

    for axis in ax[0, 0], ax[0, 1]:
        d = .015  # how big to make the diagonal lines in axes coordinates
        gap = 0.01
        adjust = 0.001
        # arguments to pass to plot, just so we don't keep repeating them
        kwargs = dict(transform=axis.transAxes, color='k', clip_on=False, linewidth=1)
        #     axis.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
        axis.plot((1 - d + 0.01 + adjust, 1 + d - 0.01 + adjust), (-d - 0.01, +d + 0.01),
                  **kwargs)  # top-right diagonal
        axis.plot((1 - d + 0.01 + gap, 1 + d - 0.01 + gap), (-d - 0.01, +d + 0.01), **kwargs)  # top-right diagonal

    plt.savefig('./charts/figure4.png', bbox_inches='tight')

    return fig, ax


def make_figure_5(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame,
        hosp_code_dict: Dict
) -> Tuple[plt.Figure, plt.Axes]:
    """
    Produces figure 5 from the paper demonstrating the distribution of
    pao2 / spo2 in the data cleaned by the nccid-cleaning and nccidxclean pipelines.

    :param n_pos: dataframe containing clinical data cleaned by nccid-cleaning
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe containing clinical data cleaned by nccidxclean
    :type x_pos: pd.DataFrame
    :param hosp_code_dict: dictionary containing hospital codes and names
    :type hosp_code_dict: Dict
    :return: figure and axis containing figure 5
    :rtype: plt.Figure, plt.Axes
    """

    fig, ax = plt.subplots(
        nrows=1,
        ncols=2,
        figsize=(default_width * 2, default_height),
    )

    plt.subplots_adjust(wspace=0.15)

    sns.boxplot(data=n_pos, x='hospital', y='pao2', hue=None, order=None, hue_order=None, linewidth=0.9,
                color=dataset_palette['NHSx Pipeline'], ax=ax[0], boxprops=dict(alpha=colour_alpha),
                saturation=colour_saturation,
                flierprops={"marker": "x", "markersize": 4, 'markeredgewidth': 0.9}
                )

    sns.boxplot(data=x_pos, x='hospital', y='spo2_imputed', hue=None, order=None, hue_order=None,
                color=dataset_palette['Extended Pipeline'], ax=ax[1], boxprops=dict(alpha=colour_alpha),
                saturation=colour_saturation,
                linewidth=0.9, flierprops={"marker": "x", "markersize": 4, 'markeredgewidth': 0.9}
                )

    ax[0].set(ylabel='PaO$\mathbf{_{2}}$',
              xlabel="Hospital",
              ylim=(-2, 105))

    ax[1].set(ylabel="SpO$\mathbf{_{2}}$",
              xlabel="Hospital",
              ylim=(-2, 105),
              )

    ax[0].set_ylabel('PaO$\mathbf{_{2}}$', labelpad=0)
    ax[1].set_ylabel('SpO$\mathbf{_{2}}$', labelpad=-5)

    ax[1].yaxis.set_major_formatter(mtick.PercentFormatter(decimals=0))

    ax[0].set_title('Raw Data / NHSx Pipeline', x=1, y=1.05, horizontalalignment='right',
                    bbox=dict(facecolor='none', edgecolor=dataset_palette['NHSx Pipeline'], boxstyle='round', pad=0.4))
    ax[1].set_title('Extended Pipeline', x=1, y=1.05, horizontalalignment='right',
                    bbox=dict(facecolor='none', edgecolor=dataset_palette['Extended Pipeline'], boxstyle='round',
                              pad=0.4))

    for axis in 0, 1:
        old_labels = [item.get_text() for item in ax[axis].get_xticklabels()]
        labels = [hosp_code_dict[x] for x in old_labels]
        dummy = ax[axis].set_xticklabels(labels,
                                         fontdict={
                                             'ha': 'center',
                                         }
                                         )
        ax[axis].tick_params(axis='x', pad=8)

    plt.savefig('./charts/figure5.png', bbox_inches='tight')

    return fig, ax


def make_figure_8(
        n_pos: pd.DataFrame,
        hosp_code_dict: Dict,
        sorter: Dict
) -> Tuple[plt.Figure, plt.Axes]:
    """
    Produces figure 8, a box plot of the pao2 values for each hospital generated by the nccid-cleaning pipeline.

    :param n_pos: dataframe containing clinical data cleaned by nccid-cleaning
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe containing clinical data cleaned by nccidxclean
    :type x_pos: pd.DataFrame
    :param hosp_code_dict: dictionary containing hospital codes and names
    :type hosp_code_dict: Dict
    :param sorter: dictionary to sort the hospitals
    :type hosp_code_dict: Dict
    :return: figure and axis containing figure 4
    :rtype: plt.Figure, plt.Axes
    """
    fig, ax = plt.subplots(
        nrows=1,
        ncols=1,
        figsize=(default_width * 8 / 5, default_height * 8 / 5),
    )

    temp = n_pos.copy()
    temp['sort_value'] = temp.hospital.map(sorter)
    temp = temp.sort_values('sort_value').reset_index(drop=True)

    sns.boxplot(data=temp, x='hospital', y='d-dimer_on_admission', color=dataset_palette['NHSx Pipeline'], ax=ax,
                saturation=colour_saturation, boxprops=dict(alpha=colour_alpha),
                linewidth=0.9, flierprops={"marker": "x", "markersize": 4, 'markeredgewidth': 0.9, }
                )

    ax.set_yscale("log")
    ax.set(xlabel="Hospital", ylabel='$\mathbf{log_{10}}$ (D-dimer on admission)')

    old_labels = [item.get_text() for item in ax.get_xticklabels()]
    labels = [hosp_code_dict[x] for x in old_labels]
    ax.set_xticklabels(
        labels,
        fontdict={
            'ha': 'center',
        }
    )
    ax.tick_params(axis='x', pad=8)

    plt.savefig('./charts/figure8.png', bbox_inches='tight')

    return fig, ax
