"""

This file contains the code to generate Figure 1 of the paper providing the overall
number of missing values after using both pipelines.

N.B. The prepare_dataframes_for_comparison and allocate_hospital_codes
functions must be run prior to producing any of the figures.

"""


import matplotlib.ticker as mtick
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from typing import Tuple

from nccidxclean.figures.sns_settings import *
from nccidxclean.figures.prepare_dataframes import raw_cols, cleaned_cols
from nccidxclean.figures.dates import date_dict


remove_list = [
    'filename_earliest_date',
    'filename_covid_status',
    'filename_latest_date',
    'age_b4dcm',
    'sex_b4dcm',
    'hospital'
]


def get_total_missing(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Returns the total number of missing values and dates in each dataset.

    :param n_pos: dataframe cleaned by the nccid-cleaning pipeline for positive pcr patients
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe cleaned by the nccidxclean pipeline for positive pcr patients
    :type x_pos: pd.DataFrame
    :return: dataframes showing the total number of missing values and dates in each dataset
    :rtype: pd.DataFrame, pd.DataFrame
    """

    # Get the number of missing values
    raw_fig1 = n_pos[raw_cols].copy().replace(" ", np.nan).replace(".", np.nan).replace("  ", np.nan)
    nhs_fig1 = n_pos.rename(
        {'swabdate': 'negative_swab_date', 'age_update': 'age', 'sex_update': 'sex'}, axis=1
    )[cleaned_cols].copy()
    xtd_fig1 = x_pos.rename({'spo2_imputed': 'pao2'}, axis=1)[cleaned_cols].copy()

    total_missing = {
        'Raw Data': raw_fig1.isna().sum().sum(),
        'NHSx Pipeline': nhs_fig1.isna().sum().sum(),
        'Extended Pipeline': xtd_fig1.isna().sum().sum(),
    }
    total_missing = pd.DataFrame.from_records([total_missing], index=['No. of Missing Values']).transpose()
    total_missing = total_missing.reset_index().rename({'index': 'Dataset'}, axis=1)
    total_missing['Dataset'] = total_missing['Dataset'].replace(date_dict)

    # Get the number of missing dates
    dates_raw = raw_fig1[list(date_dict.values())]
    dates_nhs = nhs_fig1[list(date_dict.keys())]
    dates_cam = xtd_fig1[list(date_dict.keys())]
    total_dates_missing = {
        'Raw Data': dates_raw.isna().sum().sum(),
        'NHSx Pipeline': dates_nhs.isna().sum().sum(),
        'Extended Pipeline': dates_cam.isna().sum().sum(),
    }
    total_dates_missing = pd.DataFrame.from_records([total_dates_missing], index=['No. of Missing Dates']).transpose()
    total_dates_missing = total_dates_missing.reset_index().rename({'index': 'Dataset'}, axis=1)

    return total_missing, total_dates_missing


def make_figure_1(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame
) -> Tuple[plt.figure, plt.axes]:
    """
    Generate Figure 1 of the paper providing the overall
    number of missing values after using both pipelines.

    :param n_pos: dataframe containing clinical data cleaned by nccid-cleaning
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe containing clinical data cleaned by nccidxclean
    :type x_pos: pd.DataFrame
    :return: figure 1
    :rtype: Tuple[plt.figure, plt.axes]
    """

    fig, ax = plt.subplots(
        nrows=1,
        ncols=2,
        figsize=(default_width*2, default_height*1),
        sharex=True
    )

    total_missing, total_dates_missing = get_total_missing(n_pos, x_pos)

    sns.barplot(x="Dataset", y='No. of Missing Values', palette=dataset_palette, data=total_missing, width=0.5,
                alpha=colour_alpha, saturation=colour_saturation, ax=ax[0])
    ax[0].set(ylim=(251000, 263000))
    ax[0].get_yaxis().set_major_formatter(mtick.FuncFormatter(lambda x, p: format(int(x), ',')))

    sns.barplot(x="Dataset", y='No. of Missing Dates', palette=dataset_palette, data=total_dates_missing,
                width=0.5, alpha=colour_alpha, saturation=colour_saturation, ax=ax[1])
    ax[1].set(ylim=(51900, 53500))
    ax[1].get_yaxis().set_major_formatter(mtick.FuncFormatter(lambda x, p: format(int(x), ',')))

    ax[0].spines['left'].set_bounds((252000, 262000))
    ax[1].spines['left'].set_bounds((52000, 53400))

    plt.savefig('./charts/figure1.png', bbox_inches='tight')

    return fig, ax






