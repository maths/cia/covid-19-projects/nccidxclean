"""

This module produces the date figures used in our write-up to compare
the nccid-cleaning and nccidxclean pipelines (Figures 3,7).

N.B. The prepare_dataframes_for_comparison and allocate_hospital_codes
functions must be run prior to producing any of the figures.

"""


import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.dates import DateFormatter
import numpy as np
import pandas as pd
from typing import Dict, Tuple
import requests

from nccidxclean.figures.sns_settings import *


date_dict = {field.replace(" ", "_").lower(): field for field in list(date_field_palette_1.keys())}

dates_of_interest = [
    'date_of_acquisition_of_1st_rt-pcr',
    'date_of_acquisition_of_2nd_rt-pcr',
    'date_of_positive_covid_swab'
]

date_legend_map = {
    'Date of Positive Covid Swab': 'Date of Positive Covid Swab',
    'Date of acquisition of 1st RT-PCR': 'Date of Acquisition of 1st RT-PCR',
    'Date of acquisition of 2nd RT-PCR': 'Date of Acquisition of 2nd RT-PCR',
    'Date of result of 1st RT-PCR': 'Date of Result of 1st RT-PCR',
    'Date of result of 2nd RT-PCR': 'Date of Result of 2nd RT-PCR',
    'Date of admission': 'Date of Admission',
    'Date of ITU admission': 'Date of ITU Admission',
    'Date of intubation': 'Date of Intubation',
    'Date of 1st CXR': 'Date of 1st CXR',
    'Date of 2nd CXR': 'Date of 2nd CXR',
    'Date last known alive': 'Date Last Known Alive',
    'Date of death': 'Date of Death',
}

day_fields = [x.replace('date_', 'day_') for x in dates_of_interest]
month_fields = [x.replace('date_', 'month_') for x in dates_of_interest]


def get_gov_data():
    """
    Downloads the latest covid positivity data from the gov.uk api.
    """

    url = (
        'https://api.coronavirus.data.gov.uk/v1/data?'
        'filters=areaType=nation;areaName=england&'
        'structure={"date":"date","casePositivity":"uniqueCasePositivityBySpecimenDateRollingSum"}'
    )

    response = requests.get(url, timeout=10)

    if response.status_code >= 400:
        raise RuntimeError(f'Request to download national covid positivity data failed: {response.text}')

    return response.json()


def extract_day_and_month(df: pd.DataFrame) -> pd.DataFrame:
    """
    Extracts the day and month from each date field in the dataframe.

    :param df: dataframe containing dates
    :type df: pd.DataFrame
    :return: dataframe containing extracted day and month from each cleaned date field
    :rtype: pd.DataFrame
    """

    for field in date_dict.keys():
        df[field] = pd.to_datetime(df[field])
        try:
            df[field.replace('date_', 'day_')] = df[field].dt.day
            df[field.replace('date_', 'month_')] = df[field].dt.month
        except:
            print(df[field])
    return df


def remap_melted_date_names(df: pd.DataFrame) -> pd.DataFrame:
    """
    Remaps the melted date names ready for plotting.

    :param df: melted dataframe
    :type df: pd.DataFrame
    :return: melted dataframe with remapped names
    :rtype: pd.DataFrame
    """

    for field in df.variable.unique():
        df['variable'] = df['variable'].str.replace(
            field,
            date_dict[field.replace('day_', 'date_').replace('month_', 'date_')]
        )
    return df


def get_min_gap(x: pd.Series) -> pd.Series:
    """
    Calculates the minimum gap between imaging and a PCR for each patient.

    :param x: row of dataframe corresponding to a patient
    :type x: pd.Series
    :return: row of dataframe with minimum gap between imaging and a PCR
    :rtype: pd.Series
    """

    dates = x[["date_of_positive_covid_swab", "date_of_acquisition_of_1st_rt-pcr", "date_of_acquisition_of_2nd_rt-pcr"]]
    dates = dates[dates.notna()]
    dates_study = abs((x["StudyDate"] - dates).dt.days)
    if pd.notna(x["date_of_1st_cxr"]):
        dates_1st_cxr = abs((x["date_of_1st_cxr"] - dates).dt.days)
        x.time_gap = pd.concat([dates_study, dates_1st_cxr]).min()
    else:
        x.time_gap = dates_study.min()
    return x


def merge_pcr_and_study_dates(
        df: pd.DataFrame,
        img_df: pd.DataFrame
) -> pd.DataFrame:
    """
    Merges the pcr and imaging dates and calculates the minimum gap between.

    :param df: dataframe containing pcr dates
    :type df: pd.DataFrame
    :param img_df: dataframe containing imaging dates from dicom headers
    :type img_df: pd.DataFrame
    :return: dataframe containing merged pcr and imaging dates
    :rtype: pd.DataFrame
    """

    merged = img_df[['Pseudonym', 'StudyDate']].merge(df, how='left', on='Pseudonym')
    merged["StudyDate"] = pd.to_datetime(merged["StudyDate"], format='%Y%m%d')

    merged = merged[
        (merged["date_of_positive_covid_swab"].notna() |
         merged['date_of_acquisition_of_1st_rt-pcr'].notna() |
         merged['date_of_acquisition_of_2nd_rt-pcr'].notna())
        & merged["StudyDate"].notna()]

    merged['time_gap'] = np.nan
    merged = merged.apply(get_min_gap, axis=1)
    merged = merged.sort_values('time_gap', ascending=True).drop_duplicates(subset='Pseudonym', keep='first')
    merged = merged.rename({'time_gap': 'Minimum Time between a PCR and Imaging (Days)'}, axis=1)

    return merged


def prepare_dataframe_dates(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame,
        img_df: pd.DataFrame
) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    Prepares the dataframes containing the dates for plotting.

    :param n_pos: dataframe containing data cleaned by the nccid-cleaning pipeline.
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe containing data cleaned by the nccidxclean pipeline.
    :type x_pos: pd.DataFrame
    :param img_df: dataframe containing imaging dates from dicom headers
    :type img_df: pd.DataFrame
    :return: tuple of dataframes containing the dates for now ready for plotting
    :rtype: Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]
    """

    # extract date day and month...
    n_pos = extract_day_and_month(n_pos)
    x_pos = extract_day_and_month(x_pos)
    # merge dicom dates with clinical data...
    n_dates = merge_pcr_and_study_dates(n_pos, img_df)
    x_dates = merge_pcr_and_study_dates(x_pos, img_df)

    return n_pos, x_pos, n_dates, x_dates


def make_figure_3(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame,
        n_dates: pd.DataFrame,
        x_dates: pd.DataFrame,
        hosp_code_dict: Dict,
        code_sorter: Dict
) -> Tuple[plt.Figure, plt.Axes]:
    """
    Produces figure 3 from the paper demonstrating the mean minimum time between PCR and imaging at each hospital
     and the distribution of days of the months in the data cleaned by the nccid-cleaning and nccidxclean pipelines.

    :param n_pos: dataframe containing clinical data cleaned by nccid-cleaning
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe containing clinical data cleaned by nccidxclean
    :type x_pos: pd.DataFrame
    :param n_dates: dataframe containing clinical data cleaned by nccid-cleaning with day and month extracted for
     each date and after merging with dates from dicom metadata
    :type n_dates: pd.DataFrame
    :param x_dates: dataframe containing clinical data cleaned by nccidxclean with day and month extracted for
     each date and after merging with dates from dicom metadata
    :type x_dates: pd.DataFrame
    :param hosp_code_dict: dictionary containing hospital codes and names
    :type hosp_code_dict: Dict
    :param code_sorter: dictionary containing hospital codes and sorting order
    :type code_sorter: Dict
    :return: figure and axis containing figure 3
    :rtype: plt.Figure, plt.Axes
    """

    # Make Plot
    fig, ax = plt.subplots(
        nrows=2,
        ncols=2,
        sharey='row',
        sharex='row',
        figsize=(default_width * 2, default_height * 2),
    )

    plt.subplots_adjust(wspace=0.05, hspace=0.3)

    for i in range(0, 2):
        for j in range(0, 2):
            if i == 0:
                if j == 0:
                    df_dates = n_dates
                else:
                    df_dates = x_dates

                with_max_gap = df_dates[df_dates["Minimum Time between a PCR and Imaging (Days)"] < 352]

                #             with_max_gap = with_max_gap.sort_values(['hospital']).copy()

                means_by_hosp = with_max_gap[['hospital', 'Minimum Time between a PCR and Imaging (Days)']].groupby(
                    'hospital').mean().reset_index()
                means_by_hosp['sort_value'] = means_by_hosp.hospital.map(code_sorter)
                means_by_hosp = means_by_hosp.sort_values('sort_value').reset_index(drop=True)

                sns.barplot(
                    x="hospital",
                    y="Minimum Time between a PCR and Imaging (Days)",
                    data=means_by_hosp,
                    ax=ax[i, j],
                    width=0.8,
                    color=list(dataset_palette.values())[j + 1],
                    errorbar=None,
                    alpha=colour_alpha, saturation=colour_saturation,
                )

                ax[i, j].set(ylabel="Mean Min. Time between\nPCR and Imaging (days)")
                ax[i, j].set(xlabel="Hospital")

            else:
                time_fields = day_fields
                val = 'Day of the Month'

                if j == 0:
                    part_df = remap_melted_date_names(n_pos[time_fields].melt())
                else:
                    part_df = remap_melted_date_names(x_pos[time_fields].melt())

                part_df = part_df.rename({'value': val, 'variable': 'Date Field'}, axis=1)
                part_df = part_df.sort_values('Date Field', ascending=False)

                sns.histplot(x=val, hue='Date Field', data=part_df,
                             multiple="stack",
                             shrink=.8, discrete=True, palette=date_field_palette_1, alpha=colour_alpha,
                             ax=ax[i, j],
                             )

                ax[i, j].set(ylabel="Date Count")

                if i == 1 and j == 1:
                    old_labels = ax[i, j].get_legend().get_texts()
                    old_labels.reverse()
                    old_labels = [date_legend_map[x.get_text()] for x in old_labels]
                    ax[i, j].legend(title='', loc='upper right', bbox_to_anchor=(1, 1.05), labels=old_labels)
                else:
                    ax[i, j].get_legend().remove()

    ax[0, 0].set_title('NHSx Pipeline', x=1, y=1.05, horizontalalignment='right',
                       bbox=dict(facecolor='none', edgecolor=dataset_palette['NHSx Pipeline'], boxstyle='round',
                                 pad=0.5))
    ax[0, 1].set_title('Extended Pipeline', x=1, y=1.05, horizontalalignment='right',
                       bbox=dict(facecolor='none', edgecolor=dataset_palette['Extended Pipeline'], boxstyle='round',
                                 pad=0.5))

    ax[0, 1].set(ylabel="")

    old_labels = [item.get_text() for item in ax[0, 0].get_xticklabels()]
    labels = [hosp_code_dict[x] for x in old_labels]
    for axis in 0, 1:
        try:
            ax[0, axis].set_xticklabels(labels, fontdict={'ha': 'center'})
            ax[0, axis].tick_params(axis='x', pad=8)
        except KeyError:
            pass

    plt.savefig('charts/figure3.png',
                bbox_inches='tight')

    return fig, ax


def make_figure_7(
        n_pos: pd.DataFrame,
        hosp_code_dict: Dict
) -> Tuple[plt.Figure, plt.Axes]:
    """
    Generates figure 7 - the distribution of dates corresponding to positive RT-PCR results at 3 hospitals where the
    distribution was suspicious of a change in the date format, alongside the distribution of dates for the rest of
    the NCCID hospitals and the proportion of positive PCR tests in England.

    :param n_pos: dataframe containing clinical data cleaned by nccid-cleaning
    :type n_pos: pd.DataFrame
    :param hosp_code_dict: dictionary containing hospital codes and names
    :type hosp_code_dict: Dict
    :return: figure and axis containing figure 7
    :rtype: plt.Figure, plt.Axes
    """

    fig, ax = plt.subplots(
        nrows=2,
        ncols=2,
        figsize=(default_width * 2, default_height * 2),
        sharey=True,
        sharex=True
    )

    plt.subplots_adjust(wspace=0.05, hspace=0.15)

    switched_date_hosps = [
        'University Hospitals of Leicester NHS Trust',
        "St George's Hospital",
        'The Walton Centre'
    ]

    switched_dates_base_palette = sns.color_palette(["#CC0060", "#F5AC77", "#FFD406"], 3)

    switched_dates_palette = {
        'University Hospitals of Leicester NHS Trust': switched_dates_base_palette[0],
        "St George's Hospital": switched_dates_base_palette[1],
        'The Walton Centre': switched_dates_base_palette[2],
        'Other NCCID Hospitals': dataset_palette['NHSx Pipeline'],
    }

    date_form = DateFormatter("%b'%y")

    start_date = pd.to_datetime('2020-01-01')
    end_date = pd.to_datetime('2022-03-30')

    bins = abs(end_date - start_date).days // 7
    print('Bins', bins)

    for i, center in enumerate(['Other NCCID Hospitals'] + switched_date_hosps):

        if center == 'Other NCCID Hospitals':
            df = n_pos[~n_pos.hospital.isin(switched_date_hosps)].copy()
        else:
            df = n_pos[n_pos.hospital == center].copy()

        df = df[['date_of_acquisition_of_1st_rt-pcr', 'date_of_acquisition_of_2nd_rt-pcr',
                 'date_of_positive_covid_swab']].melt()

        df = df.rename({'value': 'Date', 'variable': 'Date Field'}, axis=1)

        df = df[df.Date.notna()]

        df['Date Field'] = df['Date Field'].map(date_dict)

        df['Hospital'] = center

        if i <= 1:
            axis = (0, i)
        else:
            axis = (1, i - 2)

        sns.histplot(x="Date", hue='Hospital', data=df,
                     bins=bins,
                     palette=switched_dates_palette,
                     stat="percent",
                     ax=ax[axis],
                     legend=False,
                     alpha=0.75,
                     binrange=(
                         mpl.dates.date2num(start_date),
                         mpl.dates.date2num(end_date)
                     )
                     )

        ax[axis].tick_params(axis='x', labelsize=19)
        ax[axis].yaxis.set_major_formatter(mtick.PercentFormatter(decimals=0))
        ax[axis].xaxis.set_major_formatter(date_form)
        ax[axis].set_ylabel("Positive PCRs at Hospital/s", horizontalalignment='center')
        if axis != (0, 0):
            ax[axis].set_title("Hospital " + hosp_code_dict[center], x=1, y=1, pad=-14, horizontalalignment='right',
                               bbox=dict(facecolor='none', edgecolor=switched_dates_palette[center], boxstyle='round',
                                         pad=0.5, alpha=1, ))
        else:
            ax[axis].set_title(center, x=1, y=1, pad=-14, horizontalalignment='right',
                               bbox=dict(facecolor='none', edgecolor=switched_dates_palette[center], boxstyle='round',
                                         pad=0.5, alpha=1, ))

        ax2 = ax[axis].twinx()

        national_pcrs = pd.DataFrame(get_gov_data()['data'])
        national_pcrs.date = pd.to_datetime(national_pcrs.date, format='%Y-%m-%d')
        national_pcrs = national_pcrs[national_pcrs.date < end_date].reset_index(drop=True)
        national_pcrs = national_pcrs.rename(
            {'date': 'Date', 'casePositivity': 'Percent'}, axis=1)
        national_pcrs.Percent = national_pcrs.Percent.astype(float)

        sns.lineplot(data=national_pcrs, x="Date", y="Percent", color='#005C45', ax=ax2, linestyle="dashed", alpha=1, )

        ax2.yaxis.set_major_formatter(mtick.PercentFormatter(decimals=0))

        ax2.tick_params(axis='y', colors='#005C45')

        if axis[1] == 1:
            ax2.set_ylabel("National PCR Positivity", color="#005C45", y=0.42, horizontalalignment='center', alpha=1)
            if axis[0] == 0:
                ax2.legend(['PCR Test Positivity\nin England', ], loc='upper right', bbox_to_anchor=(1, 0.9))
        else:
            ax2.set_ylabel("")
            ax2.yaxis.set_ticklabels([])
        ax2.set_ylim(0, 59)
        ax2.spines['right'].set_visible(True)
        ax2.spines['right'].set_color('darkgreen')
        ax2.spines['right'].set_linestyle(':')
        ax2.spines['right'].set_linewidth(1.5)
        ax2.spines['right'].set_bounds((0, 50))

    plt.savefig('./charts/figure7.png', bbox_inches='tight')

    return fig, ax
