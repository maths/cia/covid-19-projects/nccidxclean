"""

This module contains the CategoricalField class and the functions to
produce the date figures used in our write-up, comparing
the nccid-cleaning and nccidxclean pipelines (Figures 2,6).

N.B. The prepare_dataframes_for_comparison and allocate_hospital_codes
functions must be run prior to producing any of the figures.

"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from typing import Tuple, Dict

from nccidxclean.figures.sns_settings import *

cat_legend_map = {
    'PMH hypertension': 'PMH Hypertension',
    'PMH CVS disease': 'PMH CVS Disease',
    'Sex': 'Sex',
}

sex_map = {
    'M': '1 (M)',
    'F': '0 (F)',
    'Unknown': '2 (Unknown)',
    '1': '1 (M)',
    '2': '2 (Unknown)',
    '0': '0 (F)'
}

sex_hue_order = [
    '0 (F)',
    '1 (M)',
    '2 (Unknown)',
    'NaN'
]

htn_map = {
    '0': "0 (No)",
    '1': "1 (Yes)",
    '2': "2 (Unknown)",
    '3': "2 (Unknown)",
    'True': '1 (Yes)',
    'False': '0 (No)',
    'nan': np.nan,
}

htn_hue_order = [
    "0 (No)",
    "1 (Yes)",
    "2 (Unknown)",
    'NaN',
]

cvs_map = {
    '0': '0',
    '1': '1',
    '2': '2',
    '3': '3',
    '4': '4',
    'nan': np.nan,
    '1es': '1',
    '1,2': '1',
}

cvs_map_split_cam = {
    '0': '0 (No)',
    '1': '1 (Yes)',
    '2': '2 (Unknown)',
    'nan': np.nan,
}

cvs_hue_order = [
    '0',
    '1',
    '2',
    '3',
    '4',
    '0 (No)',
    '1 (Yes)',
    '2 (Unknown)',
    'NaN',
]

cvs_nhs_map = {
    '0': '0 (None)',
    '1': '1 (MI)',
    '2': '2 (Angina)',
    '3': '3 (Stroke)',
    '4': '4 (Multiple)',
    'NaN': 'NaN',
}

cvs_cam_map = {
    '0': '0 (No)',
    '1': '1 (Yes)',
    '2': '2 (Unknown)',
    'NaN': 'NaN',
}


class CategoricalField:
    """
    A class to hold the data for a single categorical field, and to produce plots for the figures in the write-up.
    """
    def __init__(self, nhs_data, xtd_data, original_field, cleaned_field=None):
        self.raw = original_field
        if cleaned_field is not None:
            self.clean = cleaned_field
        else:
            self.clean = original_field.lower().replace(" ", "_")
        self.field_label = original_field
        self.raw_df = nhs_data.astype(str)[['Pseudonym', 'hospital', self.raw]].copy()
        self.nhs_df = nhs_data.astype(str)[['Pseudonym', 'hospital', self.clean]].copy()
        self.cam_df = xtd_data.astype(str)[['Pseudonym', 'hospital', self.clean]].copy()
        self.raw_df['Dataset'] = 'Raw Data'
        self.nhs_df['Dataset'] = 'NHSx Pipeline'
        self.cam_df['Dataset'] = 'Extended Pipeline'

        self.remap_column_names(self.field_label, first_map=True)

        self.df = pd.concat([
            self.raw_df,
            self.nhs_df,
            self.cam_df
        ]).sort_values(by="Dataset", ascending=False).reset_index(drop=True)

    def remap_column_names(self, new_label, first_map=False):
        if first_map:
            cleaned_label = self.clean
        else:
            cleaned_label = self.field_label
            self.df = self.df.rename({self.field_label: new_label, }, axis=1)
        self.raw_df = self.raw_df.rename({self.field_label: new_label, 'hospital': 'Hospital', }, axis=1)
        self.nhs_df = self.nhs_df.rename({cleaned_label: new_label, 'hospital': 'Hospital', }, axis=1)
        self.cam_df = self.cam_df.rename({cleaned_label: new_label, 'hospital': 'Hospital', }, axis=1)

    def map_values(self, field_map):
        self.raw_df[self.field_label] = self.raw_df[self.field_label].str.replace(".0", "").map(field_map,
                                                                                                na_action='ignore')
        self.nhs_df[self.field_label] = self.nhs_df[self.field_label].str.replace(".0", "").map(field_map,
                                                                                                na_action='ignore')
        self.cam_df[self.field_label] = self.cam_df[self.field_label].str.replace(".0", "").map(field_map,
                                                                                                na_action='ignore')
        self.df[self.field_label] = self.df[self.field_label].str.replace(".0", "").map(field_map, na_action='ignore')

    def cvs_map_values(self, nhs_field_map, cam_field_map):
        self.raw_df['PMH CVS disease'] = self.raw_df['PMH CVS disease'].str.replace(".0", "").map(nhs_field_map,
                                                                                                  na_action='ignore')
        self.nhs_df['PMH CVS disease'] = self.nhs_df['PMH CVS disease'].str.replace(".0", "").map(nhs_field_map,
                                                                                                  na_action='ignore')
        self.cam_df['PMH CVS disease'] = self.cam_df['PMH CVS disease'].str.replace(".0", "").map(cam_field_map,
                                                                                                  na_action='ignore')
        self.df.loc[self.df["Dataset"] != 'Extended Pipeline', 'PMH CVS disease'] = self.df[
            'PMH CVS disease'].str.replace(".0", "").map(nhs_field_map, na_action='ignore')
        self.df.loc[self.df["Dataset"] == 'Extended Pipeline', 'PMH CVS disease'] = self.df[
            'PMH CVS disease'].str.replace(".0", "").map(cam_field_map, na_action='ignore')

    def rename_field(self, label):
        self.remap_column_names(label)
        self.field_label = label

    def results_hist_by_dataset(self, axis, field_hue_order, field_palette, multi="stack", shrink_bars=0.5):

        sns.histplot(
            ax=axis,
            data=self.df.rename(cat_legend_map, axis=1).fillna('NaN'),
            x="Dataset",
            hue=cat_legend_map[self.field_label],
            palette=field_palette,
            multiple=multi,
            shrink=shrink_bars,
            hue_order=field_hue_order,
            alpha=colour_alpha,
        )

        axis.set(ylabel="Patient Count")
        axis.get_legend().set(title='')

    def methods_hist_by_hosp(self, axis, field_hue_order, field_palette, dataset, multi="stack"):
        if dataset == 'raw':
            plot_df = self.raw_df.copy()
        elif dataset == 'nhs':
            plot_df = self.nhs_df.copy()
        else:
            plot_df = self.cam_df.copy()

        plot_df = plot_df.rename(cat_legend_map, axis=1)

        sns.histplot(
            ax=axis,
            data=plot_df.fillna('NaN'),
            x="Hospital",
            hue=cat_legend_map[self.field_label],
            palette=field_palette,
            multiple=multi,
            hue_order=field_hue_order,
            alpha=colour_alpha,
        )

        axis.set(ylabel="Patient Count")
        axis.get_legend().set(title='')


def make_figure_2(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame
) -> Tuple[plt.Figure, plt.Axes, plt.Axes]:
    """
    Generate Figure 2 - histograms of sex (with and without dicom enrichment), sex, pmh hypertension, pmh cvs disease
    for each of the three datasets (raw, nhsx pipeline, nccidxclean pipeline).

    :param n_pos: dataframe containing clinical data cleaned by nccid-cleaning
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe containing clinical data cleaned by nccidxclean
    :type x_pos: pd.DataFrame
    :return: figure and axis containing figure 2
    :rtype: plt.Figure, plt.Axes
    """
    fig, ax = plt.subplots(
        nrows=2,
        ncols=2,
        # sharey=True, sharex=True,
        figsize=(default_width * 2, default_height * 2),
    )

    plt.subplots_adjust(wspace=0.5, hspace=0.2)


    # SEX PLOTS
    # Without DICOM enrichment
    sex = CategoricalField(n_pos, x_pos, 'Sex', 'sex_b4dcm')
    sex.map_values(sex_map)
    sex.results_hist_by_dataset(ax[0, 0], sex_hue_order, sex_palette)
    # With DICOM enrichment
    sex_enriched = CategoricalField(n_pos, x_pos, 'Sex', 'sex')
    sex_enriched.map_values(sex_map)
    sex_enriched.results_hist_by_dataset(ax[0, 1], sex_hue_order, sex_palette)

    sns.move_legend(ax[0, 0], loc='upper left', bbox_to_anchor=(1, 1))
    sns.move_legend(ax[0, 1], loc='upper left', bbox_to_anchor=(1, 1))
    ax[0, 0].get_legend().set_title('Patient Sex\n\n\n\nWithout DICOM\n\n\nEnrichment', prop=legend_title_font_props)
    ax[0, 1].get_legend().set_title('Patient Sex\n\n\n\nWith DICOM\n\n\nEnrichment', prop=legend_title_font_props)
    ax[0, 0].get_legend().get_title().set_ha("left")
    ax[0, 1].get_legend().get_title().set_ha("left")
    ax[0, 0].get_legend().get_title().set(linespacing=0.25)
    ax[0, 1].get_legend().get_title().set(linespacing=0.25)
    ax[0, 0].get_legend()._legend_box.align = "left"
    ax[0, 1].get_legend()._legend_box.align = "left"

    # PMH HYPERTENSION PLOT
    htn = CategoricalField(n_pos, x_pos, 'PMH hypertension')
    htn.map_values(htn_map)
    htn.results_hist_by_dataset(ax[1, 0], htn_hue_order, htn_palette)

    sns.move_legend(ax[1, 0], loc='upper left', bbox_to_anchor=(1, 1))

    ax[1, 0].get_legend().set_title('PMH Hypertension', prop=legend_title_font_props)
    ax[1, 0].get_legend()._legend_box.align = "left"

    # PMH CVS DISEASE PLOT
    cvs = CategoricalField(n_pos, x_pos, 'PMH CVS disease')
    cvs.cvs_map_values(cvs_map, cvs_map_split_cam)
    split_cvs_palatte = {**cvs_palette, **htn_palette}
    cvs.results_hist_by_dataset(ax[1, 1], cvs_hue_order, split_cvs_palatte)

    # Create second legend for CVS Disease
    # Top CVS legend...
    old_patches = ax[1, 1].get_legend().get_patches()
    ax[1, 1].legend(handles=old_patches[0:5] + [old_patches[-1]],
                    labels=list(cvs_nhs_map.values()),
                    title='PMH CVS Disease\n\n\n\nRaw / NCCID',
                    loc='upper left',
                    bbox_to_anchor=(1, 1),
                    labelspacing=0.35,
                    title_fontproperties=legend_title_font_props
                    )
    ax[1, 1].get_legend().get_title().set_ha("left")
    ax[1, 1].get_legend().get_title().set(linespacing=0.25)
    ax[1, 1].get_legend()._legend_box.align = "left"

    new_labels = list(cvs_nhs_map.values())
    for t, l in zip(ax[1, 1].get_legend().get_texts()[0:5] + [ax[1, 1].get_legend().get_texts()[-1]], new_labels):
        t.set_text(l)

    # Bottom CVS legend...
    new_patches = old_patches[5:]
    ax2 = ax[1, 1].twinx()
    ax2.legend(handles=new_patches,
               labels=list(cvs_cam_map.values()),
               title='Extended Pipeline',
               loc='lower left',
               bbox_to_anchor=(1.0, -0.15),
               labelspacing=0.35,
               title_fontproperties=legend_title_font_props
               )
    ax2.axis('off')
    ax2.get_legend()._legend_box.align = "left"

    # Save final
    plt.savefig('./charts/figure2.png', bbox_inches='tight')

    return fig, ax, ax2


def make_figure_6(
        n_pos: pd.DataFrame,
        x_pos: pd.DataFrame,
        hosp_code_dict: Dict
) -> Tuple[plt.Figure, plt.Axes]:
    """
    Generate Figure 6 - histograms of age and sex at each hospital in the dataset cleaned by nccid-cleaning.

    :param n_pos: dataframe containing clinical data cleaned by nccid-cleaning
    :type n_pos: pd.DataFrame
    :param x_pos: dataframe containing clinical data cleaned by nccidxclean
    :type x_pos: pd.DataFrame
    :param hosp_code_dict: dictionary containing hospital codes and names
    :type hosp_code_dict: Dict
    :return: figure and axis containing figure 6
    :rtype: plt.Figure, plt.Axes
    """

    fig, ax = plt.subplots(
        nrows=1,
        ncols=2,
        figsize=(default_width * 2, default_height),
        sharey=True,
    )

    plt.subplots_adjust(wspace=0.05)

    sex = CategoricalField(n_pos, x_pos, 'Sex', 'sex_b4dcm')

    # sex.nhs_df = sex.nhs_df[sex.nhs_df['Sex'].notna()]

    sex.map_values(sex_map)

    sex.methods_hist_by_hosp(ax[0], sex_hue_order[0:-1], sex_palette, 'nhs')

    ax[0].get_legend().set(title='')
    sns.move_legend(ax[0], loc='upper left', bbox_to_anchor=(0, 1.0))

    cvs = CategoricalField(n_pos, x_pos, 'PMH CVS disease')

    cvs.map_values(cvs_map)

    cvs.methods_hist_by_hosp(ax[1], cvs_hue_order[0:5] + [cvs_hue_order[-1]], cvs_palette, 'nhs')

    cvs_nhs_cats = {
        '0': '0 (None)',
        '1': '1 (MI)',
        '2': '2 (Angina)',
        '3': '3 (Stroke)',
        '4': '4 (Multiple)',
        'NaN': 'NaN',
    }

    sns.move_legend(ax[1], loc='upper left', bbox_to_anchor=(0, 1.0))

    new_labels = list(cvs_nhs_cats.values())
    for t, l in zip(ax[1].get_legend().get_texts(), new_labels):
        t.set_text(l)

    ax[1].set(ylabel="")

    fig.canvas.draw()

    for axis in 0, 1:
        old_labels = [item.get_text() for item in ax[axis].get_xticklabels()]
        labels = [hosp_code_dict[x] for x in old_labels]
        ax[axis].set_xticklabels(
            labels,
            fontdict={
                'ha': 'center',
            }
        )
        ax[axis].tick_params(axis='x', pad=8)

    ax[0].get_legend().set_title('Patient Sex', prop=legend_title_font_props)
    ax[1].get_legend().set_title('PMH CVS Disease', prop=legend_title_font_props)
    ax[0].get_legend()._legend_box.align = "left"
    ax[1].get_legend()._legend_box.align = "left"

    plt.savefig('./charts/figure6.png', bbox_inches='tight')

    return fig, ax
