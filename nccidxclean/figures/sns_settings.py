"""
Used to set the seaborn settings, including palettes, for all figures
"""


import seaborn as sns
import matplotlib as mpl

dpi = 200
default_width = 11
default_height = 7
font_size_main = 'xx-large'
legend_font_size = 'xx-large'
font_size_titles = 25
figure_padding = 0.2
colour_alpha = .8
colour_saturation = 1

image_params = {
    'savefig.dpi': dpi,
    "figure.dpi": dpi/2,
    'figure.figsize': [default_width, default_height],
#     "figure.autolayout":True,
    'xtick.major.width': 1,
    'ytick.major.width': 1,
    "boxplot.flierprops.marker": "x",
    "boxplot.flierprops.markersize": 4,
    'boxplot.flierprops.markeredgewidth': 0.9,
    'font.family': 'sans-serif',
    'font.sans-serif': 'FreeSans', # Neue
    'xtick.labelsize': font_size_main,
    'ytick.labelsize': font_size_main,
    'legend.title_fontsize': font_size_main,
    'legend.fontsize': legend_font_size,
#     'legend.shadow': True,
    'legend.frameon': False,
    'axes.linewidth': 1,
    'axes.titlepad': 6.0,
    'axes.titlesize': font_size_titles,
    'axes.titleweight': 'bold',
    'axes.spines.bottom': True,
    'axes.spines.left': True,
    'axes.spines.right': False,
    'axes.spines.top': False,
    'axes.grid': False,
    'axes.labelsize': font_size_titles,
    'axes.labelweight': 'bold',
    'axes.labelpad': 8.0,
    'xtick.major.pad': 5,
    'mathtext.fontset': 'dejavusans',
    'figure.subplot.hspace': figure_padding,
    'figure.subplot.wspace': figure_padding,
    "savefig.pad_inches": figure_padding,
}

legend_title_font_props = mpl.font_manager.FontProperties(weight='bold', size='xx-large')

# All palattes should be colourblind safe

base_dataset_palette = sns.color_palette('Blues', 3)

dataset_palette = {
    'Raw Data': base_dataset_palette[0],
    'NHSx Pipeline': base_dataset_palette[1],
    'Extended Pipeline': base_dataset_palette[2],
}

base_pmh_palette = sns.color_palette('PuRd', 3)

sex_palette = {
    '1 (M)': base_pmh_palette[1],
    '0 (F)': base_pmh_palette[2],
    '2 (Unknown)': base_pmh_palette[0],
    'NaN': '#810f7c'
}

base_cvs_palette = sns.color_palette('Oranges', 6)

cvs_palette = {
    '0': base_cvs_palette[5],
    '1': base_cvs_palette[4],
    '2': base_cvs_palette[3],
    '3': base_cvs_palette[2],
    '4': base_cvs_palette[1],
    '1,2': base_cvs_palette[0],
    'NaN': '#810f7c',
}


htn_palette = {
    "0 (No)": base_pmh_palette[2],
    "1 (Yes)": base_pmh_palette[1],
    "2 (Unknown)": base_pmh_palette[0],
    'NaN': '#810f7c',
}

base_date_palette = sns.color_palette('YlGn', 12)

date_field_palette_1 = {
    'Date of Positive Covid Swab': base_date_palette[3],
    'Date of acquisition of 1st RT-PCR': base_date_palette[6],
    'Date of acquisition of 2nd RT-PCR': base_date_palette[10],
    'Date of result of 1st RT-PCR': base_date_palette[7],
    'Date of result of 2nd RT-PCR': base_date_palette[11],
    'Date of admission': base_date_palette[0],
    'Date of ITU admission': base_date_palette[4],
    'Date of intubation': base_date_palette[5],
    'Date of 1st CXR': base_date_palette[8],
    'Date of 2nd CXR': base_date_palette[9],
    'Date last known alive': base_date_palette[1],
    'Date of death': base_date_palette[2],
}
