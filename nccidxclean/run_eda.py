"""
Runs the execution of exploratory data analysis on cleaned NCCID data from the command line.
"""

import pandas as pd
from pathlib import Path
import argparse

from nccidxclean.eda import create_all_reports


def main():
    """
    Handles command line execution of exploratory data analysis on cleaned NCCID data.
    """

    def validate_path(string):
        value = str(string)
        if not Path(value).exists():
            raise argparse.ArgumentError(data_path_arg, f"path does not exist: {value}")
        return Path(value)

    parser = argparse.ArgumentParser(
        description='''Creates EDA Reports for Cleaned NCCID Data.''',
    )
    data_path_arg = parser.add_argument('cleaned_data_path', type=validate_path, help="path to cleaned data csv")
    features_arg = parser.add_argument('--features', type=str, help="features to report on (default: all)")
    args = parser.parse_args()

    features = args.features.split(',') if args.features else None
    features = [f.strip().replace("(", "").replace(")", "") for f in features] if features else None

    patients = pd.read_csv(args.cleaned_data_path, index_col='Pseudonym')
    all_pats_rep, cov_pos_rep, cov_neg_rep, reps_by_center, rep_by_feature = create_all_reports(patients, features)

    eda_path = Path('./eda/')
    eda_path.mkdir(exist_ok=True)
    reports_by_center_path = eda_path / 'reports_by_center'
    reports_by_center_path.mkdir(exist_ok=True)

    all_pats_rep.save("all_patients_report.pdf")
    cov_pos_rep.save("covid_pos_report.pdf")
    cov_neg_rep.save("covid_neg_report.pdf")
    for center, report in reps_by_center.items():
        report.save(reports_by_center_path / f"{center}.pdf")
    rep_by_feature.output("features_by_center.pdf")


if __name__ == '__main__':
    main()
