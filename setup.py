from setuptools import setup, find_packages

setup(
    author='Ian Selby, Anna Breger, and the AIX-COVNET Collaboration',
    author_email='ias49@cam.ac.uk',
    description='Extended pipeline to clean the NCCID clinical data',
    name='nccidxclean',
    url='https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/nccidxclean',
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    packages=find_packages(include=['nccidxclean', 'nccidxclean.*']),
    entry_points={
        'console_scripts': [
            'nccidxclean=nccidxclean.nccidxclean:main',
            'xclean_analysis=nccidxclean.analysis:main',
            'xclean_eda=nccidxclean.run_eda:main',
            'xclean_run_nhsx=nccidxclean.run_nhsx_pipeline:main',
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
    install_requires=[
        'nccid_cleaning @ git+https://git@github.com/nhsx/nccid-cleaning#egg=nccidxclean',
        'pandas',
        'numpy>=1.20.3',
        'cryptography',
        "cffi>=1.12",
        "pycparser",
        'jsondiff',
        'regex',
        'simple-icd-10',
        'tqdm',
        "pydicom>=2.2.0",
        "python-dateutil>=2.8.1",
        "six>=1.5",
        "pytz>=2020.1",
    ],
    extras_require={
        "notebooks": ['jupyter'],
        "eda": ['dataprep', 'fpdf', 'markdown', 'jinja2<3.1,>=3.0'],
        "analysis": ['matplotlib', 'seaborn', 'requests'],
    }
)
