# <img style="float: right;" src="https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/nccidxclean/-/raw/main/docs/source/_static/nccidxclean_logo_150.png" alt="NCCIDxClean" width="60"> NCCIDxClean 

A pipeline to further enhance quality, integrity and reusability of the UK's
National COVID Chest Imaging Database (NCCID) clinical data. This is
an extension to the [original NHSx cleaning pipeline](https://github.com/nhsx/nccid-cleaning) 
for the clinical data of the database[^1]. It has been expanded and adjusted to correct additional 
systematic inconsistencies in the raw data such as patient sex, oxygen
levels and date values.


> [Further information on the rationale for the additions and changes made is provided 
> in the documentation](https://maths.uniofcam.dev/cia/covid-19-projects/nccidxclean).

A pre-print of our paper (under review) is available [on request](https://rrw71r81hp8.typeform.com/to/VADVHaTq):  

>>>
***A pipeline to further enhance quality, integrity and reusability of the NCCID clinical data*** - A. Breger\*, I. Selby\*, M. Roberts, J. Preller, J.H.F. Rudd, J.A.D. Aston, J.R. Weir-McCall, C.B. Schönlieb on behalf of the [AIX-COVNET Collaboration](https://covid19ai.maths.cam.ac.uk).

\* Joint first authors.
>>>

## Contents

[[_TOC_]]

## Installation

The package and dependencies, including the original NHSx pipeline, may be installed using pip:
```console
pip install git+https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/nccidxclean
```

Alternatively, the git repository may be cloned and installed locally:
```console
git clone https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/nccidxclean
cd nccidxclean
pip install .
```

It is advised that the package be installed in a virtual environment (conda or venv).

## Usage

The package may be run from the command line or within python.

To run the package on the command line:
```console
nccidxclean <base_path> <clinical_subdir> --xray_subdir --ct_subdir --eda
```

The output, clean data is stored in `./data/` generated in the working directory.
For additional information on command line usage, please see the 
[docs](https://maths.uniofcam.dev/cia/covid-19-projects/nccidxclean).

An [example notebook](https://gitlab.developers.cam.ac.uk/maths/cia/covid-19-projects/nccidxclean/-/blob/main/notebooks/ingest_and_run.ipynb) is provided which demonstrates how the cleaning process may be
performed step-by-step, from reading in the clinical data to enriching missing values using
the DICOM metadata.

To run the default module pipeline on a pandas dataframe in python, use:
```python
import nccidxclean as xclean
xclean_df = xclean.xclean_nccid(df)
```

The pipeline may be modified to remove steps, use original NHSx modules,
and select which data features will be returned. Further information is provided in the package
[docs](https://maths.uniofcam.dev/cia/covid-19-projects/nccidxclean).

>>>
## Data Warnings and Errors

A log file collates data warnings / errors, which are processed into three .csv files.
These files contain patient data for which manual review is advised and they are stored
in the "for_review" folder generated in the working directory.

This data should be reviewed and amended (e.g. in a spreadsheet application) and the changes
then merged with the cleaned data. This is then stored in an encrypted format allowing
these updates to be applied during future deployments.
>>>

## Pipeline Comparison and Analysis

The charts and calculations made for our write-up may be replicated using the
`figures` and `analysis` modules.

### 1. Generating Cleaned Data using the NHSx NCCID-Cleaning Pipeline
Prior to running the module, the data must also be cleaned using the NHSx nccid-cleaning
pipeline. The analysis module needs this to allow comparison with the data cleaned by
to the extended pipeline. 

Full documentation of how to run the NHSx pipeline is provided on their GitHub page; however, 
you may now run their default pipeline from the command line using:
```console
xclean_run_nhsx_pipeline <base_path> <clinical_subdir> --xray_subdir --ct_subdir --mri_subdir --xray_meta_path --ct_meta_path --mri_meta_path
```

The arguments allow you to specify the location of either the imaging DICOM/json files or
the csv's if the metadata has previously been extracted. Outputs are again saved in `./data/`.

The NHSx team have provided [Jupyter notebooks](https://github.com/nhsx/nccid-cleaning/blob/master/notebooks/) 
should you prefer to run the pipeline step-by-step or encounter any issues.

### 2. Running the Analysis to Compare the Pipelines

To generate both the analysis and figures using the command line:
```console
xclean_analysis <nhsx_cleaned_path> <extended_cleaned_path> --xray_meta_path --ct_meta_path --mri_meta_path
```

To run from python:
```python
import nccidxclean as xclean
xclean.analysis(<nhsx_cleaned_path>, <extended_cleaned_path>, xray_meta_path=None, ct_meta_path=None, mri_meta_path=None)
```

## Exploratory Data Analysis (EDA)

The package includes an EDA module to facilitate review of the cleaning data. The code utilises 
the [DataPrep](https://docs.dataprep.ai/index.html) package[^2].

Usage:
```console
xclean_eda <cleaned_data_path> --features
```

The output files are then stored in `./eda/`.

## References

[^1]: Cushnan, D., Bennett, O., Berka, R., Bertolli, O., Chopra, A., Dorgham, S., Favaro, A., 
Ganepola, T., Halling-Brown, M., Imreh, G., Jacob, J., Jefferson, E., Lemarchand, F., Schofield, D., &#38; Wyatt, J. C. 
(2021). An overview of the National COVID-19 Chest Imaging Database: data quality and cohort analysis. <i>GigaScience</i>, 
<i>10</i>(11), 2021.03.02.21252444. https://doi.org/10.1093/GIGASCIENCE/GIAB076  

[^2]: Jinglin Peng, Weiyuan Wu, Brandon Lockhart, Song Bian, Jing Nathan Yan, Linghao Xu, Zhixuan Chi, Jeffrey M. 
Rzeszotarski, and; Jiannan Wang. (2021, June). DataPrep.EDA: Task-Centric Exploratory Data Analysis for Statistical 
Modeling in Python. <i>Proceedings of the 2021 International Conference on Management of Data (SIGMOD ’21)</i>.</div>