.. raw:: html

   <style>
   .sidebar_secondary {
       remove: true;
   }
   </style>

.. image:: _static/nccidxclean_logo_dark_300.png
   :class: only-dark
   :align: right
   :width: 300


.. image:: _static/nccidxclean_logo_300.png
   :class: only-light
   :align: right
   :width: 300

Welcome to NCCIDxClean's Documentation
======================================

Introduction
------------

Welcome to NCCIDxClean (NCCID eXtended Clean), a pipeline to further enhance quality, integrity and reusability of the UK's
National COVID Chest Imaging Database (NCCID) clinical data. This is
an extension to the `original NHSx cleaning pipeline <https://github.com/nhsx/nccid-cleaning>`_
for the clinical data of the database. It has been expanded and adjusted to correct additional
systematic inconsistencies in the raw data, such as patient sex, oxygen
levels and date values.

.. admonition:: Our paper is now available:

   Breger, A.\*, Selby, I.\*, Roberts, M. et al. A pipeline to further enhance quality, integrity and reusability of the NCCID clinical data. Sci Data 10, 493 (2023). https://doi.org/10.1038/s41597-023-02340-7

   \* Joint first authors


.. figure:: _static/nccidxclean_pipes.png
    :width: 75%
    :alt: Comparison of the original NCCID cleaning pipeline and NCCIDxClean.

    A visual comparison of the original NCCID cleaning pipeline and NCCIDxClean [#myfootnote]_. This figure is a modified version of
    a figure from our paper, which may be requested above.


User Guide
----------

Information about this package, how to customise it, and how if differs from the
`original NHSx cleaning package <https://github.com/nhsx/nccid-cleaning>`_.

.. toctree::
   :maxdepth: 3

   user_guide/index

API Reference
-------------

A reference for the modules and functions in this package.

.. toctree::
   :maxdepth: 2

   api/index


Indices and Tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
