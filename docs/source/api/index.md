---
myst:
  html_meta:
    "description lang=en": |
      API Reference for NCCIDxClean.
---

# API Reference

You can customise the behaviour of the pipeline to your own specification. The following pages provide a developer 
reference to the modules and functions used in the pipeline to allow for customisation.

```{toctree}
nccidxclean
nccidxclean.clean
nccidxclean.figures
nccidxclean.num_analysis
nccidxclean.eda
```
