Fixing Header Names
===================

Original NCCID Cleaning Pipeline
--------------------------------

NCCID Function
: `_fix_headers`

- Fixed known mistakes in column headers and was always run last as it acts on the cleaned columns.

---

NCCIDxClean
-----------

New Function
: `fix_headers`

- 'swabdate' is renamed to 'negative_swab_date'. This is to make it clear that this is only relevant for the COVID-19 'negative' patients.
