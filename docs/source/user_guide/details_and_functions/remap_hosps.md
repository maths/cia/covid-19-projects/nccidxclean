Remapping Hospital Names
========================

New Function
:  ```remap_hospitals```

- Abbreviated hospital names are remapped to their full name and made consistent, e.g., 'HH' becomes 'Hammersmith Hospital'.  

- 'Musgrove Park Hospital' had entries in the form 'MPH*n*', such as 'MPH4'. These were remapped to 'Musgrove Park Hospital'.

This was not originally handled in the NCCID Cleaning package, but is useful as some systematic errors occur by hospital rather than by 'Submitting Centre'.
