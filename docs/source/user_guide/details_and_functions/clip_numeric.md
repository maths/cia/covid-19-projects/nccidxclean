Clip Numeric Fields
===================

Original NCCID Cleaning Pipeline
--------------------------------

NCCID Function
: `_clip_numeric_values`

Removed values outside of expected limits:

| Field                                  | Minimum | Maximum |
|----------------------------------------|---------|---------|
| temperature_on_admission               | 25      | 45      |
| fibrinogen\_\_if_d-dimer_not_performed | 0       | 100     |
| urea_on_admission                      | 0       | 100     |
| o2_saturation                          | 0       | 100     |

---

NCCIDxClean
-----------

New Function
: `clip_numeric`

-   Additional clipping of numerical fields.
-   Removal of non-integer numbers from integer fields.

| Field                                  | Minimum   | Maximum   | Integer |
|----------------------------------------|-----------|-----------|---------|
| temperature_on_admission               | 25        | 45        | No      |
| heart_rate_on_admission                | 0         | 250       | Yes     |
| respiratory_rate_on_admission          | 0         | 100       | Yes     |
| systolic_bp                            | 0         | 300       | Yes     |
| diastolic_bp                           | 10        | 250       | Yes     |
| wcc_on_admission                       | 0         | 500       | No      |
| lymphocyte_count_on_admission          | 0         | 500       | No      |
| platelet_count_on_admission            | 0         | 1000      | No      |
| crp_on_admission                       | Truncated | 1000      | No      |
| d-dimer_on_admission                   | 1         | Truncated | Yes     |
| fibrinogen\_\_if_d-dimer_not_performed | 0         | 100       | No      |
| creatinine_on_admission                | 30        | 6000      | Yes     |
| urea_on_admission                      | 0         | 100       | No      |
| news2_score_on_arrival                 | 0         | 20        | Yes     |
| apache_score_on_itu_arrival            | 0         | 71        | Yes     |
