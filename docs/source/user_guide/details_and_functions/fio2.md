FiO<sub>2</sub>
===============

Original NCCID Cleaning Pipeline
--------------------------------

NCCID Function
: `_rescale_fio2`

- NCCID Cleaning remaps FiO<sub>2</sub> entries to the '%' scale, as a combination of units (Litres and %) was provided.
- '0.5' is remapped to '50%' oxygen.

---

NCCIDxClean
-----------

New Function
: `rescale_fio2`

Default functionality:
- '0.5' is remapped to '23%' rather than '50%'. The NCCID function assumes '0.5' is 50% oxygen rather than 0.5 Litres as it is a decimal. However, 0.5L is more common than 50% oxygen and is equivalent to 23%.
- Minimum oxygen is set to 21% (room air), not 0%, which suggests the patient is breathing no oxygen at all. The value is clipped at 21% and 100%.
- The 'Any supplemental oxygen: FiO2' column is merged into the 'fio2' column.

Given that the conversion from litres to percentage is an approximation, the user can choose to 
retain the original values by setting `ltrs_to_percent` to `False` if running the function directly. If running the 
pipeline, you can change the `fio2_ltrs_to_percent` parameter / argument.
