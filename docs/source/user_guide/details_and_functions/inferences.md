Inferences
==========

New Function
: `inferences`

`inferences` consists of a number of nested functions:

| Nested Function              | Purpose                                                                                                                                                                                        |
|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `_update_final_covid_status` | There are patients with two positive swabs with a Negative final covid status. This function changes the final covid status to 'Positive' if they have had a positive test.                    |
| `_death_inferences`          | If a date of death is provided, but 'death' is missing, 'death' is set to '1'. If there is a 'date last known alive' but death and death of death are missing, death is set to '0'.            |
| `_itu_inferences`            | If a date of itu admission is provided, but 'itu_admission' is missing, 'itu_admission' is set to '1'. A warning is produced if they have a date of itu admission, but 'itu_admission' is '0'. |
| `_ckd_inferences`            | If they have a stage of CKD and 'pmh_ckd' is missing, 'pmh_ckd' is set to '1'.                                                                                                                 |
| `_intubation_inferences`     | If a date of intubation is provided, but 'intubation' is missing, 'intubation' is set to '1'. A warning is produced if they have a date of intubation, but 'intubation' is '0'.                |
| `_calculate_pf_ratio`        | P/F ratio is calculated in a new column to encourage use of PaO<sub>2</sub> with FiO<sub>2</sub>.                                                                                                                    |
