Sex
===

Original NCCID Cleaning Pipeline
--------------------------------

NCCID Function
: `_remap_sex`

- Remapped to F/M/Unknown.
- Any missing values are converted to 'Unknown'.

---

NCCIDxClean
-----------

New Function
: `remap_sex`

- Leaves missing values as missing (`np.nan`) to be consistent with the other categorical fields. The developer should choose whether to treat unknown and missing values as equivalent.
- Converts the codes used by Sandwell and West Birmingham to match the schema. Originally, this centre used '1' and '2' rather than the requested '0' (Female) and '1' (Male). By analysing the DICOM data and reviewing imaging, it was confirmed that 2 = Female and 1 = Male for this centre.

Possible Values
: 
|    Entry    |    Meaning    |
|:-----------:|:-------------:|
|  'Unknown'  |    Unknown    |
|     'M'     |     Male      |
|     'F'     |    Female     |
| `np.nan` | Missing Value |
