Sense Checking
==============

New Function
: `sense_check`

- Checks that the dates flow in a logical order and produces a warning message if a possible error is found. Affected patients and their date fields are output in a separate .csv file for review and correction/removal.

```{important}
Make sure to check the .csv files in `./for_review` prior to using the data. You can read-in any manual changes and save for future updates using `merge_checks_with_cleaned_df`.

- Dates are logged if they are after 2022, before 2019, or after the date of the latest included data set.
- Provides a warning if lymphocyte count is greater than the white cell count (WCC).
- Checks whether the NEWS2 is larger than the score you can calculate from the data. The calculated score is saved in 'calculated_minimum_news2' and is a minimum approximation as not all the necessary fields are provided. You should not use both in any development.
```
