Order Columns
=============

New Function
: `order_columns`

- Orders columns in same order as the Excel template used by the data providers.
- This order is more meaningful, with fields grouped together, e.g. vital signs.
