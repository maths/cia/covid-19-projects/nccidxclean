Lateral Column Shift
====================

New Function
: `column_shift`

- For one hospital there is a systematic error for many patients, in which the majority of columns had their values entered in the adjacent cell on the right in the submission Excel file.

- The affected patients are identified as they all have a date entered in the 'Current NSAID used', 'Troponin T' and 'Final COVID Status' columns.
