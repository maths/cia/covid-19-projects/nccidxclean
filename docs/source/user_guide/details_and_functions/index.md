---
myst:
  html_meta:
    "description lang=en": |
      Documentation for users who wish to build sphinx sites with
      pydata-sphinx-theme.
---

# NCCIDxClean Details and Functions

This section provides information about the modules and functions in NCCIDxClean and more detail
of the rationale underpinning it.

```{toctree}
:caption: Details and Functions
:maxdepth: 1

bin_and_cat_columns
check_new_centres
clip_numeric
column_shift
dates
fio2
fix_headers
inferences
numeric_results
order_columns
remap_hosps
sense_check
sex
```
