Checking for New Centres
========================

New Function
:  `check_new_centres`

- Checks for 'Submitting Centres' which were not present in the development data and therefore may have unresolved issues after cleaning.
