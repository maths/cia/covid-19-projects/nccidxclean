---
myst:
  html_meta:
    "description lang=en": |
      An introductory guide to the NCCIDxClean package including installation, usage, the NCCID, and development.
---

# Getting Started

This section provides an introductory guide to installation, usage, the NCCID, and development of NCCIDxClean.

```{toctree}
:caption: Get Started

installation
ingestion
dev_data
key_changes
data_analysis
eda
clinical_data
```
