---
myst:
  html_meta:
    "description lang=en": |
      A guide to running the built-in EDA module within NCCIDxClean.
---

Exploratory Data Analysis (EDA)
===============================

The package includes an EDA module to facilitate review of the cleaning data. The code utilises 
the [DataPrep](https://docs.dataprep.ai/index.html) package.

Usage:
```console
xclean_eda <cleaned_data_path> --features
```

The output files are then stored in the `./eda` folder.

```{warning}
This may fail if a modified pipeline is used due to missing columns. You may need to modify the code accordingly.
```
