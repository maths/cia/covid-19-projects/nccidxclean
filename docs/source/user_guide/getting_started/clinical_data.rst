NCCID Quick Reference
=====================

.. admonition:: NCCID Webpages

   `NCCID Web Portal <https://medphys.royalsurrey.nhs.uk/nccid/guidance.php>`_

   `NCCID Site <https://nhsx.github.io/covid-chest-imaging-database>`_

********************************************************************************************************************

Inclusion Criteria
------------------

1. All patients admitted to hospital who had a positive COVID-19 PCR test; and
2. 50 inpatients twice-a-week with a negative PCR that have never had a positive COVID PCR test.

********************************************************************************************************************

Data Included
-------------

.. topic:: COVID-Positive Patients

    :Imaging Data:
        1. All chest X-rays and CTs (and some MRIs) for the admission.
        2. Any previous thoracic imaging studies performed within the last three years.
    :Clinical Data:
        Full clinical data.

.. topic:: COVID-Negative Patients

    :Imaging Data:
        Any thoracic imaging studies performed within the 4 weeks prior to the COVID PCR test.
    :Clinical Data:
        Limited additional data: SubmittingCentre, Ethnicity, and SwabDate

.. warning::
    - Vital signs and laboratory data are the first on admission, even if they contracted COVID some weeks or even months after.
    - If the patient has multiple imaging studies, the one row of data for that patient is used for all.
    - Completeness varies between centres.
