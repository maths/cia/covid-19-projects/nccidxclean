---
myst:
  html_meta:
    "description lang=en": |
       Information on the development data used to produce NCCIDxClean.
---

Development Data
================

|                | Patients      | Submitting Centres | Hospitals | Raw Data Fields | Cleaned Data Fields | Earliest<br /> COVID Swab | Latest<br /> COVID Swab |
|----------------|---------------|--------------------|-----------|-----------------|---------------------|---------------------------|-------------------------|
| COVID-Positive | 6,931 (33%)   | 23                 | 31        | 68              | 66                  | 02/01/2020                | 11/09/2021              |
| COVID-Negative | 14,322 (67%)  | 22                 | nan       | 7               | 7                   | 14/02/2020                | 09/02/2022              |
| Full Dataset   | 21,253 (100%) | 25                 | 33        | 70              | 67                  | 02/01/2020                | 09/02/2022              |

---

No. of Patients, Missing Values and Swab Dates by Submitting Centre
-------------------------------------------------------------------

|                                                               | Total Patients | COVID-Positive /<br /> Negative Patients | Missing Values<br /> (COVID-Positives) | Earliest<br /> COVID Swab | Latest<br /> COVID Swab |
|---------------------------------------------------------------|----------------|------------------------------------------|----------------------------------------|---------------------------|-------------------------|
| Royal Cornwall Hospitals NHS Trust                            | 405 (1.9%)     | 217 (54%) /<br />188 (46%)               | 4,012 (32%)                            | 06/03/2020                | 17/05/2020              |
| University Hospitals of Leicester NHS Trust                   | 9,345 (44%)    | 1,033 (11%) /<br />8,312 (89%)           | 54,978 (92%)                           | 05/03/2020                | 31/12/2020              |
| Hampshire Hospitals NHS Foundation Trust                      | 246 (1.2%)     | 67 (27%) /<br />179 (73%)                | 1,052 (27%)                            | 22/03/2020                | 10/07/2020              |
| Cambridge University Hospitals NHS Foundation Trust           | 2,233 (11%)    | 0 (0%) /<br />2,233 (100%)               | 0.0 (nan%)                             | 14/02/2020                | 08/06/2020              |
| West Suffolk NHS Foundation Trust                             | 118 (0.56%)    | 118 (100%) /<br />0 (0%)                 | 2,024 (30%)                            | 16/03/2020                | 15/06/2020              |
| Imperial College Healthcare NHS Trust                         | 637 (3%)       | 404 (63%) /<br />233 (37%)               | 6,261 (27%)                            | 04/03/2020                | 12/07/2020              |
| Leeds Teaching Hospitals NHS Trust                            | 181 (0.85%)    | 177 (98%) /<br />4 (2.2%)                | 5,202 (51%)                            | 17/03/2020                | 17/01/2021              |
| St Georges University Hospitals NHS Foundation Trust          | 1,600 (7.5%)   | 1,165 (73%) /<br />435 (27%)             | 62,390 (92%)                           | 12/03/2020                | 31/01/2021              |
| Royal United Hospitals Bath NHS Foundation Trust              | 761 (3.6%)     | 681 (89%) /<br />80 (11%)                | 12,703 (32%)                           | 02/01/2020                | 09/03/2021              |
| Sandwell and West Birmingham Hospitals NHS Trust              | 1,951 (9.2%)   | 1,872 (96%) /<br />79 (4%)               | 78,576 (72%)                           | 24/02/2020                | 17/06/2020              |
| George Eliot Hospital NHS Trust                               | 175 (0.82%)    | 127 (73%) /<br />48 (27%)                | 2,111 (29%)                            | 13/03/2020                | 04/06/2020              |
| Liverpool Heart and Chest NHS Foundation Trust                | 1,861 (8.8%)   | 26 (1.4%) /<br />1,835 (99%)             | 360 (24%)                              | 20/03/2020                | 30/10/2021              |
| Norfolk and Norwich University Hospitals NHS Foundation Trust | 154 (0.72%)    | 154 (100%) /<br />0 (0%)                 | 3,920 (44%)                            | 10/03/2020                | 30/04/2020              |
| Ashford and St Peters Hospitals NHS Foundation Trust          | 362 (1.7%)     | 140 (39%) /<br />222 (61%)               | 3,353 (41%)                            | 24/02/2020                | 30/04/2020              |
| Sheffield Teaching Hospitals NHS Foundation Trust             | 75 (0.35%)     | 75 (100%) /<br />0 (0%)                  | 3,987 (92%)                            | nan                       | nan                     |
| Betsi Cadwaladr University Health Board                       | 88 (0.41%)     | 42 (48%) /<br />46 (52%)                 | 790 (32%)                              | 13/03/2020                | 27/12/2020              |
| London North West University Healthcare NHS Trust             | 201 (0.95%)    | 172 (86%) /<br />29 (14%)                | 3,189 (32%)                            | 11/03/2020                | 19/04/2020              |
| Brighton and Sussex University Hospitals NHS Trust            | 324 (1.5%)     | 128 (40%) /<br />196 (60%)               | 5,285 (71%)                            | 20/03/2020                | 24/04/2020              |
| Cwm Taf Morgannwg University Health Board                     | 75 (0.35%)     | 45 (60%) /<br />30 (40%)                 | 1,011 (39%)                            | 06/04/2020                | 29/05/2020              |
| Oxford University Hospitals NHS Foundation Trust              | 280 (1.3%)     | 188 (67%) /<br />92 (33%)                | 3,814 (35%)                            | 15/03/2020                | 09/02/2022              |
| The Walton Centre NHS Foundation Trust                        | 63 (0.3%)      | 57 (90%) /<br />6 (9.5%)                 | 3,008 (91%)                            | 23/03/2020                | 18/02/2021              |
| Taunton and Somerset NHS Foundation Trust                     | 52 (0.24%)     | 41 (79%) /<br />11 (21%)                 | 663 (28%)                              | 11/03/2020                | 30/05/2020              |
| Sheffield Childrens NHS Foundation Trust                      | 46 (0.22%)     | 1 (2.2%) /<br />45 (98%)                 | 12 (21%)                               | 03/04/2020                | 14/10/2020              |
| Royal Surrey NHS Foundation Trust                             | 19 (0.089%)    | 1 (5.3%) /<br />18 (95%)                 | 20 (34%)                               | 04/03/2020                | 15/03/2020              |
| University Hospitals Bristol NHS Foundation Trust             | 1 (0.0047%)    | 0 (0%) /<br />1 (100%)                   | 0.0 (nan%)                             | 03/10/2020                | 03/10/2020              |