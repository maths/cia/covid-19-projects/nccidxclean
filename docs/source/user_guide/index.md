---
myst:
  html_meta:
    "description lang=en": |
      User guide for NCCIDxClean including an explanation of the modules / functions and the rationale behind them.
---

# User Guide

This guide provides a user guide to the NCCIDxClean package and the rationale behind the functionality included. Basic
information about the NCCID dataset is also included.

```{toctree}
:caption: Get started...
:maxdepth: 3

getting_started/index
```

```{toctree}
:caption: Details and Functions...
:maxdepth: 2

details_and_functions/index
```